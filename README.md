# kemono
_a game engine for making pixels do things_

Hi! This is the Kemono game engine main project.

If you're looking to use Kemono, try forking off
[this template](https://gitlab.com/daedulas-stuff/templates/kemono).
You can [unlink the fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork) in the settings
to make it less forkly.

## What is kemono?

kemono (sic.) is a game engine created to make a specific kind of game.

It's written in Kotlin using Kotlin/Multiplatform to compile to native code and allow the game (and itself) to adapt code to different platforms.

The engine supports **Linux**, **Windows**, and _might_ build for **macOS**, but I haven't been able to test on macOS so take that with a grain of salt.

#### Obligitory αlpha warning

kemono is not complete yet.

It is currently in an αlpha state, which means that it's still very much in-development, and probably not suitable just yet for any real projects.

Use it if you will, but prepare yourself for a ton of bug-swatting and lacking of features.


## Cloning the repository

```shell
git clone --recursive git@gitlab.com:daedulas-stuff/kemono.git

# or, with http
git clone --recursive https://gitlab.com/daedulas-stuff/kemono.git

# then, pull lfs files
git lfs pull
```

### Pulling new changes

You probably want to update everything at once, like so:
```shell
git pull
git submodule update --recursive
git lfs pull
```

## Building

Kemono is only equipped to build its packages on the target platform itself.
For example, you can't build `linuxX64` on Windows, even though Kotlin/Native itself supports it.

### Building the engine

To build the `kemono` library, you need to first run vcpkg for your platform.
See below the code block for dependency requirements on Linux.

```shell
# linux
./gradlew bootstrapVcpkg vcpkgLinuxX64

# windows
./gradlew bootstrapVcpkg vcpkgMingwX64

# macos
./gradlew bootstrapVcpkg vcpkgMacosX64 vcpkgMacosArm64
```

On Windows and macOS, this does not require any additional dependencies.

On Linux, the vcpkg task requires X11 and its development libraries to be installed, among other packages for vcpkg to function.
Install them with something like:

#### Ubuntu
```shell
sudo apt install curl zip unzip tar libxcursor-dev libxi-dev libxinerama-dev libxrandr-dev libx11-dev libxrender-dev make autoconf
```

#### Red Hat / Fedora Distros

This is untested, so if it doesn't work for you, let me know.

```shell
sudo dnf install curl zip unzip tar libXcursor-devel libXi-devel libXinerama-devel libXrandr-devel make autoconf
```

#### Arch-based Distros
```shell
sudo pacman -S curl zip unzip tar cmake ninja libxcursor libxi libxinerama libxrandr libxrender libx11 make autoconf
```

### Building
#### For Windows

```shell
./gradlew mingwX64MainKlibrary
./gradlew linkDebugExecutableMingwX64 # for the test game (see below)
```

#### For Linux

```shell
./gradlew linuxX64MainKlibrary
./gradlew linkDebugExecutableLinuxX64 # for the test game (see below)
```

#### For macOS

```shell
./gradlew macosX64MainKlibrary macosArm64MainKlibrary

# for the test game (see below)
./gradlew linkDebugExecutableMacosX64 linkDebugExecutableMacosArm64
```

#### Compiling resources

Kemono uses a custom Gradle plugin called `kebuild` to compile resources.

```shell
./gradlew compileKmResources
./gradlew packageKmResources # to .kmrs
```

Resources are compiled into `build/compiledResources`. The default resources are placed in a module directory, e.g. `_modules_km/_kemono` and must be referenced as `kemono:<resource path>`. Module resource paths do not begin with slash, unlike regular resource paths, which do.

Resources are packaged into `build/distributions` in a file with a `.kmrs` extension. It's just a zip file with a funky hat.

### Running the test game

Staging and running the staged game (the way games are intended to be run in a dev environment):
```shell
./gradlew stageDebug
./gradlew runStagedDebug

# or the release variant
./gradlew stageRelease
./gradlew runStagedRelease
```
