# Changelog

### Planned versions
- [2023.1α2](https://gitlab.com/daedulas-stuff/kemono/-/milestones/2)
- [2023.1α3](https://gitlab.com/daedulas-stuff/kemono/-/milestones/3)
- [2023.1α4](https://gitlab.com/daedulas-stuff/kemono/-/milestones/4)
- [2023.1α5](https://gitlab.com/daedulas-stuff/kemono/-/milestones/5)

## Version 2023.1α2 (latest)
**Revision** 1  
**Version** 2023.1α1 (2023.1 alpha 1)  
**Version (maven)** `2023.1a2`  
**State:** Work in progress.  
[View `master` on GitLab.](https://gitlab.com/daedulas-stuff/kemono/-/tree/master)  
—

Change notes for this release, compared to 2023.1α1:

- Added service system and moved most game components to use it. **(breaking!)**
- Various resources will now ignore exceptions when disposed.
- Renamed `Platform` to `OperatingSystem` and the old `OperatingSystem` to `CurrentPlatform`. This was done to make their purposes more clear, and to not conflict with `kotlin.native.Platform` **(breaking!)**
- `Game.dispose()` will now be called by `runGame()` while the game is still in scope. This allows functions in dispose to access the scoped game.
- `ThereIsNoSpoonException` is now `open`.
- Added the `SaveDataService`. It's for storing save data.

  You can use `SaveDataService.registerSaveDataCodec<T>()` to register a codec for a class to saved as a save file. The codec can be `SaveDataCodec.fromSerializer()` to encode it in CBOR format with a `kotlinx-serialization` serializer.
- `PlayerEntity` can now be overridden and replaced by overriding `MapService`. There is currently no system for using the overriden service, but it's there!
- Removed `TestEntity`.
- Added functions to `Map` to manage `Entity` instances contained within.
- `SpriteEntity` now allows selecting the area of the texture to render.
- Spritesheets can now be offset by pixels and can ignore a margin on the right and bottom of the image. This allows strangely sized sprites to behave nicely.
- Added some UI rendering stuff. `Screen` allows fullscreen activities, `Overlay` is overlaid on top of regular gameplay.
- Added text. You can now render text inside a `UIContext` by creating a `Text` object using `text()`.
    - Text is aligned to the bottom left corner of the screen and pretends that it's rendering inside a window that is 300 pixels tall and 300 * (window's width / height) wide. This will help scaling stuff later.
- Locale resources can now be created and used with the `LocaleService`.

## Version 2023.1α1
**Revision** 1  
**Version** 2023.1α1 (2023.1 alpha 1)  
**Maven version & tag name:** `2023.1a1`  
**State:** Released.  
[View this tag on GitLab.](https://gitlab.com/daedulas-stuff/kemono/-/tree/2023.1a1)  
—

Initial release.
[See here for all commits that went into this tag.](
https://gitlab.com/daedulas-stuff/kemono/-/network/2023.1a1)
