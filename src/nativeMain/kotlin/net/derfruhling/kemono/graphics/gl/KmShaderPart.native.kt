/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.invoke
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.interfaces.ShaderPart
import net.derfruhling.kemono.platform.native.graphics.glDeleteShader
import me.derfruhling.quantum.core.log.LogPrettyPrint
import me.derfruhling.quantum.core.log.logger

@OptIn(ExperimentalForeignApi::class)
internal actual class KmShaderPart(
    private val graphics: Graphics,
    internal val location: Int,
    actual override val stage: ShaderPartStage,
    actual override val sources: Array<String>
) : ShaderPart, LogPrettyPrint {
    private val log by logger

    actual override fun dispose() {
        try {
            graphics.catchGL { glDeleteShader!!(location.toUInt()) }
        } catch (e: Exception) {
            log.error("Error occurred disposing {}", e, this)
        }
    }

    override fun toPrettyString(): String {
        return "#shader_part[$stage $location]"
    }
}