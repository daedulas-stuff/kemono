/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.*
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.graphics.enums.*
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.interfaces.TextureConfigurator
import net.derfruhling.kemono.math.Vector4
import net.derfruhling.kemono.platform.asFloatArray
import net.derfruhling.kemono.platform.native.graphics.*
import me.derfruhling.quantum.core.log.LogPrettyPrint
import me.derfruhling.quantum.core.log.logger

@OptIn(ExperimentalForeignApi::class)
internal actual sealed class KmTexture(
    private val graphics: Graphics,
    private val target: UInt,
    private val binding: UInt,
    actual val gl: UInt
) : KmGraphicsResource(), Texture, LogPrettyPrint {
    private val log by logger
    private var _internalTextureFormat: InternalTextureFormat? = null
    private var _textureFormat: TextureFormat? = null
    private var _valueType: ValueType? = null
    private var _minFilter: MinifyFilter? = null
    private var _magFilter: MagnifyFilter? = null
    private var _sEdgeMode: EdgeMode? = null
    private var _tEdgeMode: EdgeMode? = null
    private var _rEdgeMode: EdgeMode? = null
    private var _borderColor: Vector4<Float>? = null

    actual override val internalTextureFormat: InternalTextureFormat
        get() = _internalTextureFormat ?: throw RuntimeException("texture not configured")
    actual override val textureFormat: TextureFormat
        get() = _textureFormat ?: throw RuntimeException("texture not configured")
    actual override val valueType: ValueType
        get() = _valueType ?: throw RuntimeException("texture not configured")
    actual override val minFilter: MinifyFilter
        get() = _minFilter ?: throw RuntimeException("texture not configured")
    actual override val magFilter: MagnifyFilter
        get() = _magFilter ?: throw RuntimeException("texture not configured")
    actual override val sEdgeMode: EdgeMode
        get() = _sEdgeMode ?: throw RuntimeException("texture not configured")
    actual override val tEdgeMode: EdgeMode
        get() = _tEdgeMode ?: throw RuntimeException("texture not configured")
    actual override val rEdgeMode: EdgeMode
        get() = _rEdgeMode ?: throw RuntimeException("texture not configured")
    actual override val borderColor: Vector4<Float>?
        get() = _borderColor

    protected actual fun setPostWriteVariables(
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType
    ) {
        _internalTextureFormat = internalTextureFormat
        _textureFormat = textureFormat
        _valueType = valueType
    }

    actual override fun checkBound(): Boolean = memScoped {
        val value = alloc<IntVar>()
        graphics.catchGL { glGetIntegerv!!(binding, value.ptr) }
        value.value.toUInt() == gl
    }

    actual override fun bind() {
        graphics.catchGL { glBindTexture!!(target, gl) }
    }

    actual override fun configure(fn: TextureConfigurator.() -> Unit) {
        val configurator = object : TextureConfigurator {
            override var minFilter: MinifyFilter = _minFilter ?: MinifyFilter.NEAREST
                set(value) {
                    field = value
                    graphics.catchGL { glTexParameteri!!(target, GL_TEXTURE_MIN_FILTER.toUInt(), value.gl) }
                }

            override var magFilter: MagnifyFilter = _magFilter ?: MagnifyFilter.NEAREST
                set(value) {
                    field = value
                    graphics.catchGL { glTexParameteri!!(target, GL_TEXTURE_MAG_FILTER.toUInt(), value.gl) }
                }

            override var sEdgeMode: EdgeMode = _sEdgeMode ?: EdgeMode.REPEAT
                set(value) {
                    field = value
                    graphics.catchGL { glTexParameteri!!(target, GL_TEXTURE_WRAP_S.toUInt(), value.gl) }
                }

            override var tEdgeMode: EdgeMode = _tEdgeMode ?: EdgeMode.REPEAT
                set(value) {
                    field = value
                    graphics.catchGL { glTexParameteri!!(target, GL_TEXTURE_WRAP_T.toUInt(), value.gl) }
                }

            override var rEdgeMode: EdgeMode = _rEdgeMode ?: EdgeMode.REPEAT
                set(value) {
                    field = value
                    graphics.catchGL { glTexParameteri!!(target, GL_TEXTURE_WRAP_R.toUInt(), value.gl) }
                }

            override var borderColor: Vector4<Float>? = _borderColor
                set(value) {
                    field = value
                    val values = value?.asFloatArray()

                    if (values != null) values.usePinned { pin ->
                        graphics.catchGL {
                            glTexParameterfv!!(
                                target,
                                GL_TEXTURE_BORDER_COLOR.toUInt(),
                                pin.addressOf(0)
                            )
                        }
                    } else memScoped {
                        graphics.catchGL {
                            glTexParameterfv!!(
                                target,
                                GL_TEXTURE_BORDER_COLOR.toUInt(),
                                allocArrayOf(0f, 0f, 0f, 1f)
                            )
                        }
                    }
                }
        }

        configurator.fn()

        _minFilter = configurator.minFilter
        _magFilter = configurator.magFilter
        _sEdgeMode = configurator.sEdgeMode
        _tEdgeMode = configurator.tEdgeMode
        _rEdgeMode = configurator.rEdgeMode
        _borderColor = configurator.borderColor
    }

    actual override fun dispose() = memScoped {
        try {
            graphics.catchGL { glDeleteTextures!!(1, alloc(gl).ptr) }
        } catch (e: Exception) {
            log.error("Error occurred disposing {}", e, this)
        }
    }
}
