/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.*
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.graphics.structures.UniformValue
import net.derfruhling.kemono.platform.native.graphics.*
import me.derfruhling.quantum.core.dsl.DslIndexSet
import me.derfruhling.quantum.core.log.LogPrettyPrint
import me.derfruhling.quantum.core.log.logger

@OptIn(ExperimentalForeignApi::class)
internal actual class KmRenderObject private constructor(
    private val graphics: Graphics,
    private val location: Int,
    shader: Shader,
    vertexBuffer: Buffer,
    vertexCount: Int,
    indexBuffer: Buffer?,
    indexCount: Int?,
    uniformValues: Map<ShaderUniform, UniformValue>
) : KmGraphicsResource(), RenderObject, LogPrettyPrint {
    actual class Creator : RenderObjectConfigurator {
        private var _shader: Shader? = null
        private var _vertexBuffer: Buffer? = null
        private var _vertexCount: Int? = null
        private val _uniformValues = mutableMapOf<ShaderUniform, UniformValue>()

        actual override var shader: Shader
            get() = _shader ?: throw IllegalStateException("shader not set")
            set(value) {
                _shader = value
            }

        actual override var vertexBuffer: Buffer
            get() = _vertexBuffer ?: throw IllegalStateException("vertexBuffer not set")
            set(value) {
                _vertexBuffer = value
            }
        actual override var vertexCount: Int
            get() = _vertexCount ?: throw IllegalStateException("vertexCount not set")
            set(value) {
                _vertexCount = value
            }
        actual override var indexBuffer: Buffer? = null
        actual override var indexCount: Int? = null

        actual override val uniformValues = DslIndexSet<ShaderUniform, UniformValue> { key, value ->
            _uniformValues[key] = value
        }

        actual fun build(graphics: Graphics, vao: Int): KmRenderObject {
            return KmRenderObject(
                graphics,
                vao,
                shader,
                vertexBuffer,
                vertexCount,
                indexBuffer,
                indexCount,
                _uniformValues.toMap()
            )
        }
    }

    actual class Editor(private val renderObject: KmRenderObject) : RenderObjectConfigurator {
        private var _shader: Shader? = null
        private var _vertexBuffer: Buffer? = null
        private var _vertexCount: Int? = null
        private var _indexBuffer: Buffer? = null
        private var _indexCount: Int? = null
        private val _uniformValues: MutableList<Pair<ShaderUniform, UniformValue>> = mutableListOf()

        actual override var shader: Shader
            get() = _shader ?: renderObject.shader
            set(value) {
                _shader = value
            }

        actual override val uniformValues = DslIndexSet<ShaderUniform, UniformValue> { key, value ->
            _uniformValues.add(key to value)
        }

        actual override var vertexBuffer: Buffer
            get() = _vertexBuffer ?: renderObject.vertexBuffer
            set(value) {
                _vertexBuffer = value
            }
        actual override var vertexCount: Int
            get() = _vertexCount ?: renderObject.vertexCount
            set(value) {
                _vertexCount = value
            }
        actual override var indexBuffer: Buffer?
            get() = _indexBuffer ?: renderObject.indexBuffer
            set(value) {
                _indexBuffer = value
            }
        actual override var indexCount: Int?
            get() = _indexCount ?: renderObject.indexCount
            set(value) {
                _indexCount = value
            }

        actual fun apply() {
            renderObject.ensureBound()
            (_shader as KmShader?)?.bind()
            (vertexBuffer as KmBuffer).bind()

            _shader?.inputOrder?.let { inputOrder ->
                val stride = inputOrder.sumOf { input -> input.type.byteSize * input.size }

                inputOrder.forEach { input ->
                    val ptr = (input.type.byteSize * input.size).toLong()

                    glVertexAttribPointer!!(
                        input.location.toUInt(),
                        input.size,
                        input.type.gl.toUInt(),
                        GL_FALSE.toUByte(),
                        stride,
                        ptr.toCPointer()
                    )

                    renderObject.graphics.catchGL { glEnableVertexAttribArray!!(input.location.toUInt()) }
                }
            }

            val kmBuffer = _indexBuffer as KmBuffer?
            if (kmBuffer != null) kmBuffer.bind()
            else KmBuffer.bindNull(BufferType.INDEX_ARRAY)

            _shader?.let { renderObject._shader = it }
            _vertexBuffer?.let { renderObject._vertexBuffer = it }
            _vertexCount?.let { renderObject._vertexCount = it }
            _indexBuffer?.let { renderObject._indexBuffer = it }
            _indexCount?.let { renderObject._indexCount = it }

            renderObject._uniformValues += _uniformValues.toMap()
        }
    }

    private var _shader: Shader = when (shader) {
        is ShaderResource -> shader.shader
        else -> shader
    }

    private val log by logger

    private var _vertexBuffer: Buffer = vertexBuffer
    private var _vertexCount: Int = vertexCount
    private var _indexBuffer: Buffer? = indexBuffer
    private var _indexCount: Int? = indexCount
    private var _uniformValues: Map<ShaderUniform, UniformValue> = uniformValues

    actual override val shader: Shader
        get() = _shader
    actual override val vertexBuffer: Buffer
        get() = _vertexBuffer
    actual override val vertexCount: Int
        get() = _vertexCount
    actual override val indexBuffer: Buffer?
        get() = _indexBuffer
    actual override val indexCount: Int?
        get() = _indexCount
    actual override val uniformValues: Map<ShaderUniform, UniformValue>
        get() = _uniformValues

    init {
        ensureBound()
        (_shader as KmShader?)?.bind()
        (vertexBuffer as KmBuffer).bind()

        _shader.inputOrder?.let { inputOrder ->
            val stride = inputOrder.sumOf { input -> input.type.byteSize * input.size }

            var ptr = 0L
            inputOrder.forEach { input ->
                glVertexAttribPointer!!(
                    input.location.toUInt(),
                    input.size,
                    input.type.gl.toUInt(),
                    GL_FALSE.toUByte(),
                    stride,
                    ptr.toCPointer()
                )

                graphics.catchGL { glEnableVertexAttribArray!!(input.location.toUInt()) }
                ptr += input.type.byteSize * input.size
            }
        }

        (_indexBuffer as KmBuffer?)?.bind()
        log.debug("Created {}", this)
    }

    actual override fun checkBound(): Boolean = memScoped {
        val value = alloc<IntVar>()
        graphics.catchGL { glGetIntegerv!!(GL_VERTEX_ARRAY_BINDING.toUInt(), value.ptr) }
        value.value == location
    }

    actual override fun bind() {
        graphics.catchGL { glBindVertexArray!!(location.toUInt()) }
    }

    actual override fun dispose() = memScoped {
        log.debug("Deleting {}", this@KmRenderObject)
        try {
            graphics.catchGL { glDeleteVertexArrays!!(1, alloc(location.toUInt()).ptr) }
        } catch (e: Exception) {
            log.error("Error occurred disposing {}", e, this)
        }
    }

    actual override fun edit(fn: RenderObjectConfigurator.() -> Unit) {
        Editor(this).apply(fn).apply()
        log.debug("Edited {}", this)
    }

    override fun toPrettyString(): String {
        return "#render_object[#vao $location " +
                "[#shader ${(_shader as LogPrettyPrint).toPrettyString()}] " +
                "[#vertex-buffer ${(_vertexBuffer as LogPrettyPrint).toPrettyString()} $_vertexCount vertices] " +
                "[#index-buffer ${(_indexBuffer as LogPrettyPrint).toPrettyString()} $_indexCount indices] " +
                "[#uniforms ${uniformValues.toList().joinToString { (k, v) ->
                    "${k.name}=${
                        when (v) {
                            is UniformValue.Custom -> when {
                                v.isNull -> "<null>"
                                v is LogPrettyPrint -> v.toPrettyString()
                                else -> v.toString()
                            }

                            is LogPrettyPrint -> v.toPrettyString()
                            else -> v.toString()
                        }
                    }"
                }}] " +
                "fin]"
    }
}