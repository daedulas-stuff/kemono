/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.*
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.enums.BufferUpdatePurpose
import net.derfruhling.kemono.graphics.enums.BufferUpdateRate
import net.derfruhling.kemono.graphics.enums.convertGL
import net.derfruhling.kemono.graphics.interfaces.Buffer
import net.derfruhling.kemono.platform.native.graphics.glBindBuffer
import net.derfruhling.kemono.platform.native.graphics.glBufferData
import net.derfruhling.kemono.platform.native.graphics.glDeleteBuffers
import net.derfruhling.kemono.platform.native.graphics.glGetIntegerv
import net.derfruhling.kemono.scopedGraphics
import me.derfruhling.quantum.core.log.LogPrettyPrint
import me.derfruhling.quantum.core.log.logger
import okio.Buffer as OkioBuffer

@OptIn(ExperimentalForeignApi::class)
internal actual class KmBuffer(
    private val graphics: Graphics,
    type: BufferType,
    val gl: UInt
) : KmGraphicsResource(), Buffer, LogPrettyPrint {
    actual override var type = type
        private set

    private val log by logger
    private var _size = 0

    actual override val size: Int by this::_size

    actual override fun checkBound() = memScoped {
        val bound = alloc<IntVar>()
        graphics.catchGL { glGetIntegerv!!(type.binding.toUInt(), bound.ptr) }
        bound.value.toUInt() == gl
    }

    actual override fun bind() {
        graphics.catchGL { glBindBuffer!!(type.gl.toUInt(), gl) }
    }

    actual override fun write(buffer: OkioBuffer, updateRate: BufferUpdateRate, updatePurpose: BufferUpdatePurpose) {
        log.debug("Writing {} data [{} {}] {} bytes", this, updateRate, updatePurpose, buffer.size)
        _size = buffer.size.toInt()

        ensureBound()
        buffer.copy().readByteArray().usePinned { pin ->
            graphics.catchGL {
                glBufferData!!(
                    type.gl.toUInt(),
                    buffer.size,
                    pin.addressOf(0),
                    convertGL(updatePurpose, updateRate)
                )
            }
        }
    }

    actual override fun transitionBuffer(type: BufferType) {
        if(this.type == type) return
        log.debug("Transitioning buffer {} into {}", this, type)
        bindNull(this.type)
        this.type = type
    }

    actual override fun dispose() = memScoped {
        try {
            graphics.catchGL { glDeleteBuffers!!(1, alloc(gl).ptr) }
        } catch (e: Exception) {
            log.error("Error occurred disposing {}", e, this)
        }
    }

    companion object {
        internal fun bindNull(type: BufferType) {
            scopedGraphics.catchGL { glBindBuffer!!(type.gl.toUInt(), 0u) }
        }
    }

    override fun toPrettyString(): String {
        return "#buffer[$type $gl]"
    }
}