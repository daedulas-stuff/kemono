/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.invoke
import kotlinx.cinterop.usePinned
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.enums.InternalTextureFormat
import net.derfruhling.kemono.graphics.enums.TextureFormat
import net.derfruhling.kemono.graphics.enums.ValueType
import net.derfruhling.kemono.graphics.interfaces.Texture3D
import net.derfruhling.kemono.platform.native.graphics.GL_TEXTURE_3D
import net.derfruhling.kemono.platform.native.graphics.GL_TEXTURE_BINDING_3D
import net.derfruhling.kemono.platform.native.graphics.glTexImage3D
import net.derfruhling.kemono.platform.native.graphics.glTexSubImage3D
import me.derfruhling.quantum.core.log.logger
import okio.Buffer as OkioBuffer

@OptIn(ExperimentalForeignApi::class)
internal actual class KmTexture3D(graphics: Graphics, gl: UInt) : KmTexture(
    graphics,
    TARGET.toUInt(),
    BINDING.toUInt(),
    gl,
), Texture3D {
    private val log by logger

    private var _width: Int = 0
    private var _height: Int = 0
    private var _depth: Int = 0

    actual override val width: Int by this::_width
    actual override val height: Int by this::_height
    actual override val depth: Int by this::_depth

    actual override fun write(
        width: Int,
        height: Int,
        depth: Int,
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: OkioBuffer
    ) {
        log.debug(
            "Writing {} data [#size {},{},{}] [#format {} of {} as {} internally] {} bytes",
            this,
            width, height, depth,
            textureFormat, valueType, internalTextureFormat,
            buffer.size
        )

        _width = width
        _height = height
        _depth = depth

        setPostWriteVariables(internalTextureFormat, textureFormat, valueType)
        ensureBound()

        buffer.readByteArray().usePinned { pin ->
            glTexImage3D!!(
                TARGET.toUInt(),
                0,
                internalTextureFormat.gl,
                width,
                height,
                depth,
                0,
                textureFormat.gl.toUInt(),
                valueType.gl.toUInt(),
                pin.addressOf(0)
            )
        }
    }

    actual override fun writePartial(
        x: Int,
        y: Int,
        z: Int,
        width: Int,
        height: Int,
        depth: Int,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: OkioBuffer
    ) {
        log.debug(
            "Writing partial {} data [#at {},{},{}] [#size {},{},{}] [#format {} of {}] {} bytes",
            this,
            x, y, z,
            width, height, depth,
            textureFormat, valueType,
            buffer.size
        )

        ensureBound()

        buffer.readByteArray().usePinned { pin ->
            glTexSubImage3D!!(
                TARGET.toUInt(),
                0,
                x,
                y,
                z,
                width,
                height,
                depth,
                textureFormat.gl.toUInt(),
                valueType.gl.toUInt(),
                pin.addressOf(0)
            )
        }
    }

    override fun toPrettyString(): String {
        return "#texture<3d>[$gl]"
    }

    companion object {
        private const val TARGET = GL_TEXTURE_3D
        private const val BINDING = GL_TEXTURE_BINDING_3D
    }
}