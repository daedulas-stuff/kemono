/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.*
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.enums.ValueType
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4
import net.derfruhling.kemono.platform.asFloatArray
import net.derfruhling.kemono.platform.asIntArray
import net.derfruhling.kemono.platform.native.graphics.*
import me.derfruhling.quantum.core.log.LogPrettyPrint
import me.derfruhling.quantum.core.log.logger

@OptIn(ExperimentalForeignApi::class)
internal actual class KmShader(
    private val graphics: Graphics,
    val location: Int,
    actual override val parts: Map<ShaderPartStage, ShaderPart>,
    inputOrder: Iterable<String>? = null,
    private val disposeParts: Boolean = false
) : KmGraphicsResource(), Shader, LogPrettyPrint {
    private val log by logger

    companion object {
        private fun convertGlValueType(type: Int): Pair<ValueType, Int> = when (type) {
            GL_FLOAT -> ValueType.FLOAT to 1
            GL_FLOAT_VEC2 -> ValueType.FLOAT to 2
            GL_FLOAT_VEC3 -> ValueType.FLOAT to 3
            GL_FLOAT_VEC4 -> ValueType.FLOAT to 4
            GL_INT -> ValueType.U_INT to 1
            GL_INT_VEC2 -> ValueType.U_INT to 2
            GL_INT_VEC3 -> ValueType.U_INT to 3
            GL_INT_VEC4 -> ValueType.U_INT to 4
            else -> throw IllegalArgumentException("illegal value $type")
        }
    }

    actual override val inputs: Map<String, ShaderInput> = memScoped {
        val count = alloc<IntVar>()
        val name = allocArray<ByteVar>(256)
        val type = alloc<GLenumVar>()
        graphics.catchGL { glGetProgramiv!!(location.toUInt(), GL_ACTIVE_ATTRIBUTES.toUInt(), count.ptr) }

        buildMap {
            for (i in 0..<count.value) {
                graphics.catchGL { glGetActiveAttrib!!(location.toUInt(), i.toUInt(), 256, null, null, type.ptr, name) }
                val nameStr = name.toKString()
                val (t, s) = try {
                    convertGlValueType(type.value.toInt())
                } catch (e: IllegalArgumentException) {
                    throw IllegalArgumentException("shader input $nameStr has illegal value ${type.value}", e)
                }

                val location = graphics.catchGL { glGetAttribLocation!!(location.toUInt(), name) }

                val input = object : ShaderInput, LogPrettyPrint {
                    override val name: String = nameStr
                    override val type: ValueType = t
                    override val size: Int = s
                    override val location: Int = location

                    override fun toPrettyString(): String {
                        return "[#input $name: $type$size @$location]"
                    }
                }

                put(nameStr, input)
            }
        }
    }

    actual override val uniforms: Map<String, ShaderUniform> = memScoped {
        val count = alloc<IntVar>()
        val name = allocArray<ByteVar>(256)
        graphics.catchGL { glGetProgramiv!!(location.toUInt(), GL_ACTIVE_UNIFORMS.toUInt(), count.ptr) }

        buildMap {
            for (i in 0..<count.value) {
                graphics.catchGL { glGetActiveUniformName!!(location.toUInt(), i.toUInt(), 256, null, name) }
                val nameStr = name.toKString()

                val location = graphics.catchGL { glGetUniformLocation!!(location.toUInt(), name) }

                val uniform = object : ShaderUniform, LogPrettyPrint {
                    override val name: String = nameStr
                    override val location: Int = location

                    override fun toPrettyString(): String {
                        return "[#uniform $name: @$location]"
                    }
                }

                put(nameStr, uniform)
            }
        }
    }

    actual override val inputOrder: Iterable<ShaderInput>? = inputOrder?.mapNotNull { inputs[it] }

    actual override fun getOutput(name: String): ShaderOutput = memScoped {
        object : ShaderOutput, LogPrettyPrint {
            override val name: String = name
            override val location: Int = glGetUniformLocation!!(
                this@KmShader.location.toUInt(),
                name.cstr.getPointer(this@memScoped)
            )

            override fun toPrettyString(): String {
                return "[#output $name @$location]"
            }
        }
    }

    actual override fun setUniform(uniform: ShaderUniform, x: Int) =
        graphics.catchGL { glUniform1i!!(uniform.location, x) }

    actual override fun setUniform(uniform: ShaderUniform, x: Int, y: Int) =
        graphics.catchGL { glUniform2i!!(uniform.location, x, y) }

    actual override fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int) =
        graphics.catchGL { glUniform3i!!(uniform.location, x, y, z) }

    actual override fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int, w: Int) =
        graphics.catchGL { glUniform4i!!(uniform.location, x, y, z, w) }

    actual override fun setUniform(uniform: ShaderUniform, x: Float) =
        graphics.catchGL { glUniform1f!!(uniform.location, x) }

    actual override fun setUniform(uniform: ShaderUniform, x: Float, y: Float) =
        graphics.catchGL { glUniform2f!!(uniform.location, x, y) }

    actual override fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float) =
        graphics.catchGL { glUniform3f!!(uniform.location, x, y, z) }

    actual override fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float, w: Float) =
        graphics.catchGL { glUniform4f!!(uniform.location, x, y, z, w) }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector2<Int>) =
        x.asIntArray().usePinned { pin -> graphics.catchGL { glUniform2iv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector3<Int>) =
        x.asIntArray().usePinned { pin -> graphics.catchGL { glUniform3iv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector4<Int>) =
        x.asIntArray().usePinned { pin -> graphics.catchGL { glUniform4iv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector2<Float>) =
        x.asFloatArray().usePinned { pin -> graphics.catchGL { glUniform2fv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector3<Float>) =
        x.asFloatArray().usePinned { pin -> graphics.catchGL { glUniform3fv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun setUniform(uniform: ShaderUniform, x: Vector4<Float>) =
        x.asFloatArray().usePinned { pin -> graphics.catchGL { glUniform4fv!!(uniform.location, 1, pin.addressOf(0)) } }

    actual override fun bind() {
        graphics.catchGL { glUseProgram!!(location.toUInt()) }
    }

    actual override fun checkBound(): Boolean = memScoped {
        val bound = alloc<IntVar>()
        graphics.catchGL { glGetIntegerv!!(GL_CURRENT_PROGRAM.toUInt(), bound.ptr) }
        bound.value == location
    }

    actual override fun dispose() {
        try {
            graphics.catchGL { glDeleteProgram!!(location.toUInt()) }
        } catch (e: Exception) {
            log.error("Error occurred disposing {}", e, this)
        }

        if (disposeParts) {
            parts.values.forEach { it.dispose() }
        }
    }

    override fun toPrettyString(): String {
        return "#shader[$location]"
    }
}