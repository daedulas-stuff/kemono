/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.invoke
import kotlinx.cinterop.usePinned
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.enums.InternalTextureFormat
import net.derfruhling.kemono.graphics.enums.TextureFormat
import net.derfruhling.kemono.graphics.enums.ValueType
import net.derfruhling.kemono.graphics.interfaces.Texture1D
import net.derfruhling.kemono.platform.native.graphics.GL_TEXTURE_1D
import net.derfruhling.kemono.platform.native.graphics.GL_TEXTURE_BINDING_1D
import net.derfruhling.kemono.platform.native.graphics.glTexImage1D
import net.derfruhling.kemono.platform.native.graphics.glTexSubImage1D
import me.derfruhling.quantum.core.log.logger
import okio.Buffer

@OptIn(ExperimentalForeignApi::class)
internal actual class KmTexture1D(graphics: Graphics, gl: UInt) : KmTexture(
    graphics,
    TARGET.toUInt(),
    BINDING.toUInt(),
    gl,
), Texture1D {
    private val log by logger
    private var _width: Int = 0

    actual override val width: Int by this::_width

    actual override fun write(
        width: Int,
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    ) {
        log.debug(
            "Writing {} data [#size {}] [#format {} of {} as {} internally] {} bytes",
            this,
            width,
            textureFormat, valueType, internalTextureFormat,
            buffer.size
        )

        _width = width
        setPostWriteVariables(internalTextureFormat, textureFormat, valueType)
        ensureBound()

        buffer.readByteArray().usePinned { pin ->
            glTexImage1D!!(
                TARGET.toUInt(),
                0,
                internalTextureFormat.gl,
                width,
                0,
                textureFormat.gl.toUInt(),
                valueType.gl.toUInt(),
                pin.addressOf(0)
            )
        }
    }

    actual override fun writePartial(
        x: Int,
        width: Int,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    ) {
        log.debug(
            "Writing partial {} data [#at {}] [#size {}] [#format {} of {}] {} bytes",
            this,
            x,
            width,
            textureFormat, valueType,
            buffer.size
        )

        ensureBound()

        buffer.readByteArray().usePinned { pin ->
            glTexSubImage1D!!(
                TARGET.toUInt(),
                0,
                x,
                width,
                textureFormat.gl.toUInt(),
                valueType.gl.toUInt(),
                pin.addressOf(0)
            )
        }
    }

    override fun toPrettyString(): String {
        return "#texture<1d>[$gl]"
    }

    companion object {
        private const val TARGET = GL_TEXTURE_1D
        private const val BINDING = GL_TEXTURE_BINDING_1D
    }
}


