/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.platform.native.graphics.*

/**
 * A representation of a texture's minification filter.
 *
 * @property gl The OpenGL representation.
 */
@Serializable
public actual enum class MinifyFilter(public val gl: Int) {
    /**
     * Nearest-neighbor filtering.
     */
    @SerialName("n")
    NEAREST(GL_NEAREST),

    /**
     * Linear filtering.
     */
    @SerialName("l")
    LINEAR(GL_LINEAR),

    /**
     * [NEAREST] filtering, both for the texture itself and it's mipmap.
     */
    @SerialName("nn")
    NEAREST_MIPMAP_NEAREST(GL_NEAREST_MIPMAP_NEAREST),

    /**
     * [NEAREST] filtering, but [LINEAR] for the mipmap.
     */
    @SerialName("nl")
    NEAREST_MIPMAP_LINEAR(GL_NEAREST_MIPMAP_LINEAR),

    /**
     * [LINEAR] filtering, but [NEAREST] for the mipmap.
     */
    @SerialName("ln")
    LINEAR_MIPMAP_NEAREST(GL_LINEAR_MIPMAP_NEAREST),

    /**
     * [LINEAR] filtering, both for the texture itself and it's mipmap.
     */
    @SerialName("ll")
    LINEAR_MIPMAP_LINEAR(GL_LINEAR_MIPMAP_LINEAR)
}