/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.platform.native.graphics.GL_FLOAT
import net.derfruhling.kemono.platform.native.graphics.GL_UNSIGNED_BYTE
import net.derfruhling.kemono.platform.native.graphics.GL_UNSIGNED_INT
import net.derfruhling.kemono.platform.native.graphics.GL_UNSIGNED_SHORT

/**
 * Represents a primitive value type.
 *
 * @property gl The OpenGL representation of this enum class.
 * @property byteSize The size in bytes of this value type.
 */
@Serializable
@Suppress("KDocMissingDocumentation")
public actual enum class ValueType(public val gl: Int, public val byteSize: Int) {
    @SerialName("float")
    FLOAT(GL_FLOAT, 4),

    @SerialName("u_int")
    U_INT(GL_UNSIGNED_INT, 4),

    @SerialName("u_short")
    U_SHORT(GL_UNSIGNED_SHORT, 2),

    @SerialName("u_byte")
    U_BYTE(GL_UNSIGNED_BYTE, 1)
}
