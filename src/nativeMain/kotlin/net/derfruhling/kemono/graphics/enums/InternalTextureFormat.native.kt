/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

import net.derfruhling.kemono.platform.native.graphics.GL_RED
import net.derfruhling.kemono.platform.native.graphics.GL_RG
import net.derfruhling.kemono.platform.native.graphics.GL_RGB
import net.derfruhling.kemono.platform.native.graphics.GL_RGBA

/**
 * The internal texture format of a texture.
 * As of now, this is just a representation of the color components that the
 * texture requires.
 *
 * @property gl The OpenGL representation of the internal texture format.
 */
@Suppress("KDocMissingDocumentation")
public actual enum class InternalTextureFormat(public val gl: Int) {
    RGBA(GL_RGBA),
    RGB(GL_RGB),
    RG(GL_RG),
    R(GL_RED),
}