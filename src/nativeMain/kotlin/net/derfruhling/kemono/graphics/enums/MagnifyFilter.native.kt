/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.platform.native.graphics.GL_LINEAR
import net.derfruhling.kemono.platform.native.graphics.GL_NEAREST

/**
 * A texture magnification filter.
 *
 * @property gl The OpenGL representation of the filter.
 */
@Serializable
public actual enum class MagnifyFilter(public val gl: Int) {
    /**
     * Nearest neighbor filtering.
     * The pixel chosen will be the nearest pixel to the requested position.
     */
    @SerialName("n")
    NEAREST(GL_NEAREST),

    /**
     * Linear interpolation filtering.
     * The pixel chosen will be interpolated between the pixels that surround
     * the texture coordinates.
     */
    @SerialName("l")
    LINEAR(GL_LINEAR)
}