/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import kotlinx.cinterop.*
import net.derfruhling.kemono.KemonoEngine
import net.derfruhling.kemono.ThereIsNoSpoonException
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.gl.*
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.graphics.structures.UniformValue
import net.derfruhling.kemono.graphics.structures.setUniform
import net.derfruhling.kemono.math.Vector4
import net.derfruhling.kemono.platform.native.graphics.*
import net.derfruhling.kemono.scopedGame
import me.derfruhling.quantum.core.log.Logger
import me.derfruhling.quantum.core.log.logger
import okio.FileSystem
import okio.Path.Companion.toPath
import platform.posix.getenv
import kotlin.random.Random
import kotlin.reflect.KClass

private val log = Logger.of(Graphics::class)

@Suppress("UNUSED_PARAMETER")
@OptIn(ExperimentalForeignApi::class)
private fun onDebugMessage(
    source: GLenum,
    type: GLenum,
    id: GLuint,
    severity: GLenum,
    length: GLsizei,
    message: CPointer<ByteVar>?,
    userParam: COpaquePointer?
) {
    val sourceStr = when (source.toInt()) {
        GL_DEBUG_SOURCE_API -> "API"
        GL_DEBUG_SOURCE_WINDOW_SYSTEM -> "WS"
        GL_DEBUG_SOURCE_SHADER_COMPILER -> "SDC"
        GL_DEBUG_SOURCE_THIRD_PARTY -> "3P"
        GL_DEBUG_SOURCE_APPLICATION -> "APP"
        GL_DEBUG_SOURCE_OTHER -> "OTH"
        else -> "<unknown source>"
    }

    val typeStr = when (type.toInt()) {
        GL_DEBUG_TYPE_ERROR -> "error"
        GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR -> "deprecated behavior"
        GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR -> "undefined behavior"
        GL_DEBUG_TYPE_PORTABILITY -> "portability"
        GL_DEBUG_TYPE_PERFORMANCE -> "performance"
        GL_DEBUG_TYPE_MARKER -> "marker"
        GL_DEBUG_TYPE_PUSH_GROUP -> "push group"
        GL_DEBUG_TYPE_POP_GROUP -> "pop group"
        GL_DEBUG_TYPE_OTHER -> "other"
        else -> "<unknown type>"
    }

    when (severity.toInt()) {
        GL_DEBUG_SEVERITY_HIGH -> log.error(
            "debug: {} {}: {}",
            ThereIsNoSpoonException(),
            sourceStr,
            typeStr,
            message!!.toKString()
        )

        GL_DEBUG_SEVERITY_MEDIUM -> log.warn(
            "debug: {} {}: {}",
            ThereIsNoSpoonException(),
            sourceStr,
            typeStr,
            message!!.toKString()
        )

        GL_DEBUG_SEVERITY_LOW, GL_DEBUG_SEVERITY_NOTIFICATION ->
            log.debug("debug: {} {}: {}", sourceStr, typeStr, message!!.toKString())
    }
}

/**
 * A graphics context that can be used to interact with OpenGL.
 * This instance should only be interacted with in the context of the render
 * thread, which can be retrieved the game instance if it is available.
 *
 * On macOS, the render thread **must** be the same as the main thread, else
 * macOS gets all pissy and throws an exception that may or may not be caught
 * by a try-catch block.
 */
@OptIn(ExperimentalForeignApi::class)
public actual open class Graphics actual constructor() {
    private val log by logger

    init {
        gladLoadGL()

        if (getenv("KM_DEBUG")?.toKString() == "YES") {
            catchGL { glEnable!!(GL_DEBUG_OUTPUT.toUInt()) }
            catchGL { glDebugMessageCallback!!(staticCFunction(::onDebugMessage), null) }
        }
    }

    /**
     * The screen clear color.
     *
     * Safe to get asynchronously.
     * However, setting this calls an OpenGL function, and must be done on the
     * render thread.
     *
     * @see clear
     */
    @set:RenderFun
    public actual var clearColor: Vector4<Float> = Vector4(0f, 0f, 0f, 1f)
        set(value) {
            catchGL { glClearColor!!(value.x, value.y, value.z, value.w) }
            field = value
        }

    init {
        clearColor = Vector4(0f, 0f, 0f, 1f)
    }

    /**
     * Throws if there is an OpenGL error present.
     *
     * @see catchGL
     *
     * @throws OpenGLException There is an error.
     * A subclass of [OpenGLException] is thrown rather than the class itself.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun throwIfError() {
        val error = glGetError!!().toInt()
        if (error == GL_NO_ERROR) return

        throw when (error) {
            GL_INVALID_ENUM -> InvalidEnumGLException()
            GL_INVALID_VALUE -> InvalidValueGLException()
            GL_INVALID_OPERATION -> InvalidOperationGLException()
            GL_STACK_OVERFLOW -> StackOverflowGLException()
            GL_STACK_UNDERFLOW -> StackUnderflowGLException()
            GL_OUT_OF_MEMORY -> GPUOutOfMemoryError()
            GL_INVALID_FRAMEBUFFER_OPERATION -> InvalidFramebufferGLException()
            GL_CONTEXT_LOST -> ContextLostGLException()
            else -> OpenGLException("Unknown error $error")
        }
    }

    /**
     * Clears the current framebuffer, or just the window contents if there
     * is no framebuffer bound.
     *
     * @see clearColor
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun clear() {
        catchGL { glClear!!(GL_COLOR_BUFFER_BIT.convert()) }
    }

    /**
     * Creates a new buffer.
     *
     * @param type The type of the buffer to create.
     * Cannot be changed later.
     *
     * @return A brand new [Buffer].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun createBuffer(type: BufferType): Buffer = memScoped {
        val buffer = alloc<UIntVar>()
        catchGL { glGenBuffers!!(1, buffer.ptr) }
        return KmBuffer(this@Graphics, type, buffer.value)
    }

    /**
     * Creates a new 1-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture1D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun createTexture1D(): Texture1D = memScoped {
        val texture = alloc<UIntVar>()
        catchGL { glGenTextures!!(1, texture.ptr) }
        return KmTexture1D(this@Graphics, texture.value)
    }

    /**
     * Creates a new 2-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture2D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun createTexture2D(): Texture2D = memScoped {
        val texture = alloc<UIntVar>()
        catchGL { glGenTextures!!(1, texture.ptr) }
        return KmTexture2D(this@Graphics, texture.value)
    }

    /**
     * Creates a new 3-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture3D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun createTexture3D(): Texture3D = memScoped {
        val texture = alloc<UIntVar>()
        catchGL { glGenTextures!!(1, texture.ptr) }
        return KmTexture3D(this@Graphics, texture.value)
    }

    private val shaderCommonPreprocessorBindings = mutableMapOf<String, String?>()
    private val shaderVertexStagePreprocessorBindings = mutableMapOf<String, String?>()
    private val shaderFragmentStagePreprocessorBindings = mutableMapOf<String, String?>()
    private val shaderGeometryStagePreprocessorBindings = mutableMapOf<String, String?>()

    private fun Map<String, String?>.asSource() = buildString {
        for ((key, value) in this@asSource) {
            if (value != null) {
                appendLine("#define $key $value")
            } else {
                appendLine("#define $key 1")
            }
        }
    }

    /**
     * Sets a shader preprocessor define for _all_ shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun setShaderCommonPreprocessorBindingCommon(name: String, value: String?) {
        shaderCommonPreprocessorBindings[name] = value
    }

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.VERTEX] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun setShaderVertexStagePreprocessorBindingCommon(name: String, value: String?) {
        shaderVertexStagePreprocessorBindings[name] = value
    }

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.FRAGMENT] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun setShaderFragmentStagePreprocessorBindingCommon(name: String, value: String?) {
        shaderFragmentStagePreprocessorBindings[name] = value
    }

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.GEOMETRY] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun setShaderGeometryStagePreprocessorBindingCommon(name: String, value: String?) {
        shaderGeometryStagePreprocessorBindings[name] = value
    }

    /**
     * Removes a shader preprocessor define for _all_ shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun unsetShaderCommonPreprocessorBindingCommon(name: String) {
        shaderCommonPreprocessorBindings.remove(name)
    }

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.VERTEX]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun unsetShaderVertexStagePreprocessorBindingCommon(name: String) {
        shaderVertexStagePreprocessorBindings.remove(name)
    }

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.FRAGMENT]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun unsetShaderFragmentStagePreprocessorBindingCommon(name: String) {
        shaderFragmentStagePreprocessorBindings.remove(name)
    }

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.GEOMETRY]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public actual open fun unsetShaderGeometryStagePreprocessorBindingCommon(name: String) {
        shaderGeometryStagePreprocessorBindings.remove(name)
    }

    /**
     * Creates a new shader part from the provided GLSL source.
     *
     * @param stage The stage of the part to create.
     * @param source The source code of the shader, in GLSL.
     * @param definitions An optional map of preprocessor definitions to pass
     * to this shader specifically.
     *
     * @return A brand new [ShaderPart]
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderCompileException The shader part failed to compile.
     * [ShaderCompileException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderCompileException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderCompileException::class)
    @RenderFun
    public actual open fun createShaderPart(
        stage: ShaderPartStage,
        source: String,
        definitions: Map<String, String>
    ): ShaderPart = memScoped {
        val concatenatedSource = buildShaderSourceCode(stage, source, definitions)
        return createShaderPartFromRawSource(stage, concatenatedSource)
    }

    private fun buildShaderSourceCode(
        stage: ShaderPartStage,
        source: String,
        definitions: Map<String, String>
    ) = /* language=glsl */ arrayOf(
        """
            #version 410 core
            
            /* Shader info:
             * - Stage: $stage (ord: ${stage.ordinal})
             * - Source length: ${source.length}
             * - Source hash: ${source.hashCode()}
             * - State hash: ${listOf(
                stage,
                COMMON_SHADER_PREFIX,
                shaderCommonPreprocessorBindings.toList().joinToString { (k, v) -> "$k=$v" },
                when (stage) {
                    ShaderPartStage.VERTEX -> VERTEX_SHADER_PREFIX
                    ShaderPartStage.FRAGMENT -> FRAGMENT_SHADER_PREFIX
                    ShaderPartStage.GEOMETRY -> GEOMETRY_SHADER_PREFIX
                },
                when (stage) {
                    ShaderPartStage.VERTEX -> shaderVertexStagePreprocessorBindings
                    ShaderPartStage.FRAGMENT -> shaderFragmentStagePreprocessorBindings
                    ShaderPartStage.GEOMETRY -> shaderGeometryStagePreprocessorBindings
                }.toList().joinToString { (k, v) -> "$k=$v" },
                source
            ).joinToString().hashCode()} (approx. for verify shader sameness)
            */
            
            /* common shader prefix */
            #line 1 0
            
        """.trimIndent() + COMMON_SHADER_PREFIX,
        """
            /* common shader defines */
            #line 1 1
            
        """.trimIndent() + shaderCommonPreprocessorBindings.asSource(),
        """
            /* ${stage.name.lowercase()}-specific shader prefix */
            #line 1 2
                    
        """.trimIndent() + when (stage) {
            ShaderPartStage.VERTEX -> VERTEX_SHADER_PREFIX
            ShaderPartStage.FRAGMENT -> FRAGMENT_SHADER_PREFIX
            ShaderPartStage.GEOMETRY -> GEOMETRY_SHADER_PREFIX
        },
        """
            /* ${stage.name.lowercase()}-specific shader defines */
            #line 1 3
            
        """.trimIndent() + when (stage) {
            ShaderPartStage.VERTEX -> shaderVertexStagePreprocessorBindings
            ShaderPartStage.FRAGMENT -> shaderFragmentStagePreprocessorBindings
            ShaderPartStage.GEOMETRY -> shaderGeometryStagePreprocessorBindings
        }.asSource(),
        """
            /* application-provided shader defines */
            #line 1 90
            
        """.trimIndent() + definitions.asSource(),
        """
            /* === BEGIN SHADER SOURCE === */
            #line 1 99
            
        """.trimIndent() + source
    ).joinToString("\n")

    /**
     * Creates a new shader part from the raw source code.
     * The source code will be passed directly to the compiler without being
     * modified in any way, which means that preprocessor definitions added to
     * this graphics instance will not be defined for these shaders.
     *
     * @param stage The stage of the part to create.
     * @param source The source code of the shader, in GLSL.
     *
     * @return A brand new [ShaderPart]
     *
     * You probably want [createShaderPart].
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderCompileException The shader part failed to compile.
     * [ShaderCompileException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderCompileException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderCompileException::class)
    @RenderFun
    public actual open fun createShaderPartFromRawSource(stage: ShaderPartStage, source: String): ShaderPart =
        memScoped {
            val shaderPart = catchGL {
                glCreateShader!!(
                    when (stage) {
                        ShaderPartStage.VERTEX -> GL_VERTEX_SHADER
                        ShaderPartStage.FRAGMENT -> GL_FRAGMENT_SHADER
                        ShaderPartStage.GEOMETRY -> GL_GEOMETRY_SHADER
                    }.toUInt()
                )
            }

            if (KemonoEngine.dumpShaderContents) {
                val path = FileSystem.SYSTEM_TEMPORARY_DIRECTORY.resolve(
                    (source.hashCode().toString(36) + ".${stage.name.lowercase()}.glsl").toPath()
                )

                FileSystem.SYSTEM.write(path) { writeUtf8(source) }
                log.debug("DEBUG: Dumped shader contents to {}", FileSystem.SYSTEM.canonicalize(path))
            }

            catchGL { glShaderSource!!(shaderPart, 1, allocArrayOf(source.cstr.getPointer(this)), null) }
            catchGL { glCompileShader!!(shaderPart) }

            val compileStatus = alloc<IntVar>()
            val infoLogLength = alloc<IntVar>()

            catchGL { glGetShaderiv!!(shaderPart, GL_COMPILE_STATUS.toUInt(), compileStatus.ptr) }
            catchGL { glGetShaderiv!!(shaderPart, GL_INFO_LOG_LENGTH.toUInt(), infoLogLength.ptr) }

            val log = if (infoLogLength.value > 0) {
                val infoLog = allocArray<ByteVar>(infoLogLength.value + 1)
                catchGL { glGetShaderInfoLog!!(shaderPart, infoLogLength.value + 1, null, infoLog) }

                infoLog.toKString().also { infoLogStr ->
                    log.warn(
                        "shader part {} emit info log whilst compiling:\n{}",
                        shaderPart, infoLogStr
                    )
                }
            } else String()

            if (compileStatus.value == GL_FALSE) {
                @OptIn(ExperimentalStdlibApi::class)
                val path = FileSystem.SYSTEM_TEMPORARY_DIRECTORY
                    .resolve(Random.Default.nextBytes(16).toHexString() + ".glsl")
                FileSystem.SYSTEM.write(path) { writeUtf8(source) }
                throw ShaderCompileException(shaderPart, path, log)
            }

            KmShaderPart(this@Graphics, shaderPart.toInt(), stage, arrayOf(source))
        }

    /**
     * Creates a new shader by linking multiple shader parts together.
     *
     * @param shaderParts The parts to link.
     * @param inputOrder An optional sequence of shader input names that will be
     * the order that inputs are placed into buffers when the mesh builder
     * checks for inputs.
     * If not provided, the input order is not guaranteed to be the same across
     * runs of the game.
     *
     * May be useful for debugging!
     *
     * @param disposeParts If `true`, the resulting [Shader] will dispose its
     * parts as well as itself when it is [dispose][Shader.dispose]d.
     * Otherwise, the parts are left dangling.
     * `false` may be useful if you are using the shader part in multiple
     * shaders, and/or plan to reuse shader parts.
     *
     * Kemono does not make use of this feature itself.
     *
     * @return A brand new [Shader]
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderLinkException The shader failed to link.
     * [ShaderLinkException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderLinkException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderLinkException::class)
    @RenderFun
    public actual open fun createShader(
        vararg shaderParts: ShaderPart,
        inputOrder: Iterable<String>?,
        disposeParts: Boolean
    ): Shader = memScoped {
        val program = glCreateProgram!!()

        shaderParts.forEach { shaderPart ->
            if (shaderPart !is KmShaderPart) {
                throw IllegalArgumentException("shader part $shaderPart is not KmShaderPart")
            }

            catchGL { glAttachShader!!(program, shaderPart.location.toUInt()) }
        }

        catchGL { glLinkProgram!!(program) }

        val linkStatus = alloc<IntVar>()
        val infoLogLength = alloc<IntVar>()

        catchGL { glGetProgramiv!!(program, GL_LINK_STATUS.toUInt(), linkStatus.ptr) }
        catchGL { glGetProgramiv!!(program, GL_INFO_LOG_LENGTH.toUInt(), infoLogLength.ptr) }

        val log = if (infoLogLength.value > 0) {
            val infoLog = allocArray<ByteVar>(infoLogLength.value + 1)
            catchGL { glGetProgramInfoLog!!(program, infoLogLength.value + 1, null, infoLog) }

            infoLog.toKString().also { infoLogStr ->
                log.warn(
                    "program {} of [{}] emit info log whilst linking:\n{}",
                    program, shaderParts.joinToString(), infoLogStr
                )
            }
        } else String()

        if (linkStatus.value == GL_FALSE) {
            throw ShaderLinkException(program, log)
        }

        KmShader(this@Graphics, program.toInt(), shaderParts.associateBy { it.stage }, inputOrder, disposeParts)
    }

    /**
     * Creates a new [RenderObject] from a configuration provided by the block.
     *
     * @param fn The configuration block. Should produce a valid [RenderObject].
     *
     * @return A brand new [RenderObject]
     *
     * @see RenderObjectConfigurator
     *
     * @throws OpenGLException An OpenGL error occurred.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun createRenderObject(fn: RenderObjectConfigurator.() -> Unit): RenderObject = memScoped {
        val vao = alloc<UIntVar>()
        catchGL { glGenVertexArrays!!(1, vao.ptr) }
        KmRenderObject.Creator().apply(fn).build(this@Graphics, vao.value.toInt())
    }

    /**
     * Copies the contents of one buffer to another.
     * Both buffers must have been initialized by a [Buffer.write] before this
     * function is called, otherwise an error may be thrown.
     *
     * @param sourceOffset The offset (in bytes) into [source] from which to
     * copy bytes.
     * @param source The buffer to copy bytes from.
     * @param targetOffset The offset (in bytes) into [target] into which to
     * copy bytes.
     * @param target The buffer to copy bytes into.
     * @param size The number of bytes to copy from [source] to [target].
     *
     * @throws OpenGLException An OpenGL error occurred.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public actual open fun copyBuffer(
        sourceOffset: Int,
        source: Buffer,
        targetOffset: Int,
        target: Buffer,
        size: Int
    ) {
        source.ensureBound()
        target.ensureBound()

        log.debug("Copy buffer {} -> {}", source, target)
        glCopyBufferSubData!!(
            source.type.gl.toUInt(),
            target.type.gl.toUInt(),
            sourceOffset.toLong(),
            targetOffset.toLong(),
            size.toLong()
        )
    }

    /**
     * Draws the [renderObject] to the current framebuffer.
     *
     * @param renderObject The [RenderObject] to draw.
     */
    public actual inline fun draw(renderObject: RenderObject, crossinline fn: RenderConfigContext.() -> Unit) {
        val configContext = object : RenderConfigContext {
            private val tokenHandlers =
                mutableMapOf<KClass<*>, RenderConfigContext.(ShaderUniform, UniformValue.Token) -> UniformValue>()

            override val renderObject: RenderObject = renderObject

            override fun <T : UniformValue.Token> onUniformToken(
                kClass: KClass<T>,
                handler: RenderConfigContext.(ShaderUniform, UniformValue.Token) -> UniformValue
            ) {
                tokenHandlers[kClass] = handler
            }

            fun getTokenByClass(
                kClass: KClass<*>
            ): RenderConfigContext.(ShaderUniform, UniformValue.Token) -> UniformValue {
                return tokenHandlers[kClass]
                    ?: { _, _ -> throw IllegalStateException("Unregistered token class $kClass") }
            }
        }

        renderObject.ensureBound()
        renderObject.shader.ensureBound()

        configContext.fn()

        renderObject.uniformValues.forEach { (uniform, value) ->
            renderObject.shader.setUniform(uniform, when(value) {
                is UniformValue.Special -> when(value) {
                    UniformValue.Special.CAMERA_POSITION ->
                        UniformValue.Vector2F(scopedGame.map.currentMap!!.playerEntity.position)
                }

                is UniformValue.Token -> {
                    configContext.getTokenByClass(value::class).let { fn ->
                        configContext.fn(uniform, value)
                    }
                }

                else -> value
            })
        }

        if (renderObject.indexBuffer != null) {
            catchGL {
                glDrawElements!!(
                    GL_TRIANGLES.toUInt(),
                    renderObject.indexCount!!,
                    GL_UNSIGNED_SHORT.toUInt(),
                    null
                )
            }
        } else {
            catchGL { glDrawArrays!!(GL_TRIANGLES.toUInt(), 0, renderObject.vertexCount) }
        }
    }

    public actual companion object {
        /**
         * The common shader prefix prepended to all shaders passed to
         * [createShaderPart].
         */
        //language=glsl
        public actual val COMMON_SHADER_PREFIX: String = """
            #define KEMONO 1
        """.trimIndent()

        /**
         * The [ShaderPartStage.VERTEX] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        //language=glsl
        public actual val VERTEX_SHADER_PREFIX: String = """
            #define KM_VERTEX_SHADER 1
        """.trimIndent()

        /**
         * The [ShaderPartStage.FRAGMENT] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        //language=glsl
        public actual val FRAGMENT_SHADER_PREFIX: String = """
            #define KM_FRAGMENT_SHADER 1
        """.trimIndent()

        /**
         * The [ShaderPartStage.GEOMETRY] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        //language=glsl
        public actual val GEOMETRY_SHADER_PREFIX: String = """
            #define KM_GEOMETRY_SHADER 1
        """.trimIndent()
    }
}
