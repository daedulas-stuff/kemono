/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import kotlinx.cinterop.*
import net.derfruhling.kemono.error.GLFWError
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.platform.native.graphics.*

/**
 * Represents a GLFW monitor, which can be used to make a window fullscreen.
 */
@OptIn(ExperimentalForeignApi::class)
public actual class Monitor private constructor(private val monitor: COpaquePointer) {
    /**
     * The current video mode the monitor is using.
     */
    public actual val currentVideoMode: VideoMode
        get() = glfwGetVideoMode(monitor.reinterpret())?.let {
            videoModeFrom(it.pointed)
        } ?: throw GLFWError()

    /**
     * Every video mode the monitor supports.
     */
    public actual val allVideoModes: List<VideoMode>
        get() = memScoped {
            val count = alloc<IntVar>()
            val videoModes = glfwGetVideoModes(monitor.reinterpret(), count.ptr)
                ?: throw GLFWError()

            List(count.value) { videoModeFrom(videoModes[it]) }
        }

    /**
     * The physical size of the monitor, in millimeters.
     */
    public actual val physicalSize: Vector2<Int>?
        get() = vector2iFromPtrs { x, y ->
            glfwGetMonitorPhysicalSize(monitor.reinterpret(), x, y)
        }.takeUnless { it.x == 0 || it.y == 0 }

    /**
     * The virtual position of the monitor in the arrangement of monitors.
     */
    public actual val virtualPosition: Vector2<Int>
        get() = vector2iFromPtrs { x, y ->
            glfwGetMonitorPos(monitor.reinterpret(), x, y)
        }.also { if(it.x == 0 || it.y == 0) throw GLFWError() }

    /**
     * The display name of the monitor.
     */
    public actual val name: String
        get() = glfwGetMonitorName(monitor.reinterpret())?.toKString()
            ?: throw GLFWError()

    public actual companion object {
        private fun videoModeFrom(value: GLFWvidmode): VideoMode {
            return VideoMode(
                value.width,
                value.height,
                value.refreshRate,
                value.redBits,
                value.greenBits,
                value.blueBits
            )
        }

        /**
         * The primary monitor of the system.
         */
        public actual val primary: Monitor
            get() = Monitor(glfwGetPrimaryMonitor() ?: throw GLFWError())

        /**
         * All monitors of the system.
         */
        public actual val all: List<Monitor>
            get() = memScoped {
                val count = alloc<IntVar>()
                val monitors = glfwGetMonitors(count.ptr)
                    ?: throw GLFWError()

                List(count.value) {
                    Monitor(monitors[it]!!)
                }
            }
    }
}
