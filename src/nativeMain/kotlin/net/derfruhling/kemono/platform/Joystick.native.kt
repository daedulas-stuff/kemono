/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import kotlinx.cinterop.*
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.error.GLFWError
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.native.graphics.glfwGetJoystickAxes
import net.derfruhling.kemono.platform.native.graphics.glfwGetJoystickButtons
import me.derfruhling.quantum.core.dsl.PairOf

/**
 * Represents a GLFW joystick.
 *
 * @property jid The id of the joystick.
 * @property guid The GUID of the joystick.
 * Can be used to identify a specific joystick.
 * @property name The user-facing name of the joystick.
 */
@OptIn(ExperimentalForeignApi::class)
public actual class Joystick internal constructor(
    public actual val jid: Int,
    public actual val guid: PairOf<Long>, // TODO
    public actual val name: String
) {
    /**
     * Gets the button values of the joystick.
     *
     * @return An array of buttons.
     *
     * @see InputTrigger.GamepadButton
     */
    @RenderFun
    public actual fun getButtons(): BooleanArray = memScoped {
        val count = alloc<IntVar>()
        val buttons = glfwGetJoystickButtons(jid, count.ptr)
            ?: throw GLFWError()

        BooleanArray(count.value) { buttons[it] > 0u }
    }

    /**
     * Gets the axis values of the joystick.
     *
     * @return An array of buttons.
     *
     * @see InputTrigger.GamepadAxis
     */
    @RenderFun
    public actual fun getAxes(): FloatArray = memScoped {
        val count = alloc<IntVar>()
        val axes = glfwGetJoystickAxes(jid, count.ptr)
            ?: throw GLFWError()

        FloatArray(count.value) { axes[it] }
    }
}
