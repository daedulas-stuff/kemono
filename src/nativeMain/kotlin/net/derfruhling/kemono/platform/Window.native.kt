/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import kotlinx.cinterop.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.*
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.KemonoEngine
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.error.GLFWError
import net.derfruhling.kemono.graphics.catchGL
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.platform.native.graphics.*
import net.derfruhling.kemono.scopedGraphics
import me.derfruhling.quantum.core.dsl.PairOf
import me.derfruhling.quantum.core.interfaces.Closeable
import me.derfruhling.quantum.core.log.logger
import platform.posix.getenv

@OptIn(ExperimentalForeignApi::class)
private inline fun windowCallback(window: COpaquePointer, fn: (Window) -> Unit) {
    val ptr = glfwGetWindowUserPointer(window.reinterpret()) ?: return
    fn(ptr.asStableRef<Window>().get())
}

/**
 * Represents a GLFW window.
 */
@OptIn(ExperimentalForeignApi::class)
public actual class Window(
    private val glfw: COpaquePointer,
    title: String
) : Closeable {
    private val log by logger

    /**
     * The title of the window.
     */
    public actual var title: String = title
        set(value) {
            field = value
            glfwSetWindowTitle(glfw.reinterpret(), value)
        }

    /**
     * The size, in pixels, of the window.
     *
     * @see framebufferSize
     * @see sizeFlow
     */
    public actual var size: Vector2<Int>
        get() = sizeFlow.value
        set(value) {
            glfwSetWindowSize(glfw.reinterpret(), value.x, value.y)
            _sizeFlow.update { value }

            memScoped {
                val fx = alloc<IntVar>()
                val fy = alloc<IntVar>()
                glfwGetFramebufferSize(glfw.reinterpret(), fx.ptr, fy.ptr)
                _framebufferSizeFlow.update { Vector2(fx.value, fy.value) }

                scopedGraphics.catchGL { glViewport!!(0, 0, fx.value, fy.value) }
            }
        }

    /**
     * The size of the window's framebuffer, in pixels.
     *
     * @see framebufferSizeFlow
     */
    public actual val framebufferSize: Vector2<Int>
        get() = framebufferSizeFlow.value

    /**
     * The position of the window on the screen, in pixels.
     *
     * @see positionFlow
     */
    public actual var position: Vector2<Int>
        get() = _positionFlow.value
        set(value) {
            glfwSetWindowPos(glfw.reinterpret(), value.x, value.y)
            _positionFlow.update { value }
        }

    /**
     * Whether this window wants to close.
     * If this is `true`, the user probably clicked the X to close the window,
     * and it should be closed / hidden asap.
     */
    public actual var shouldClose: Boolean
        get() = glfwWindowShouldClose(glfw.reinterpret()) == GLFW_TRUE
        set(value) = glfwSetWindowShouldClose(glfw.reinterpret(), if (value) GLFW_TRUE else GLFW_FALSE)

    /**
     * Whether the window is currently visible.
     * Set this to change the window's visibility.
     * This is not the same as iconifying the window, which does not make it
     * invisible according to this property.
     */
    public actual var isVisible: Boolean = false
        set(value) {
            field = value
            if (value) glfwShowWindow(glfw.reinterpret())
            else glfwHideWindow(glfw.reinterpret())
        }

    /**
     * Whether this window will wait for the next v-blank of the monitor before
     * returning from [swapBuffers].
     *
     * This is `true` by default.
     * Use [Game.lastFrameTimeAvg]!
     */
    public actual var isVsyncEnabled: Boolean = false
        set(value) {
            field = value
            makeActive()
            glfwSwapInterval(if (value) 1 else 0)
        }

    /**
     * Whether the window is currently focused.
     */
    public actual val isActive: Boolean
        get() = glfwGetCurrentContext() == glfw

    /**
     * Checks whether mouse delta input hooks are even supported at all.
     * Some systems may return `false`, so ensure that this is `true` before
     * committing to using mouse delta.
     *
     * It may be worth throwing an error if this value must be `true`.
     */
    public actual val isMouseDeltaSupported: Boolean
        get() = glfwRawMouseMotionSupported() != GLFW_FALSE

    /**
     * If this is `true`, mouse delta mode is enabled and input events will be
     * output for mouse delta, replacing mouse position.
     *
     * For this to be meaningful, [isMouseDeltaSupported] must also be `true`.
     */
    public actual var isMouseDeltaEnabled: Boolean = false
        set(value) {
            glfwSetInputMode(
                glfw.reinterpret(),
                GLFW_RAW_MOUSE_MOTION,
                when(value) {
                    true -> GLFW_TRUE
                    false -> GLFW_FALSE
                }
            )

            field = value
        }

    private val _stableRef = StableRef.create(this)
    private val _sizeFlow = MutableStateFlow(vector2iFromPtrs { x, y -> glfwGetWindowSize(glfw.reinterpret(), x, y) })
    private val _framebufferSizeFlow =
        MutableStateFlow(vector2iFromPtrs { x, y -> glfwGetFramebufferSize(glfw.reinterpret(), x, y) })
    private val _positionFlow =
        MutableStateFlow(vector2iFromPtrs { x, y -> glfwGetWindowPos(glfw.reinterpret(), x, y) })

    /**
     * A flow that is emitted to whenever the window's size changes.
     *
     * @see size
     */
    public actual val sizeFlow: StateFlow<Vector2<Int>> = _sizeFlow.asStateFlow()

    /**
     * A flow that is emitted to whenever the window's framebuffer size changes.
     * This is always fired alongside [sizeFlow].
     *
     * @see framebufferSize
     */
    public actual val framebufferSizeFlow: StateFlow<Vector2<Int>> = _framebufferSizeFlow.asStateFlow()

    /**
     * A flow that is emitted to whenever the window's position changes.
     *
     * @see position
     */
    public actual val positionFlow: StateFlow<Vector2<Int>> = _positionFlow.asStateFlow()

    private val _keyFlow: MutableSharedFlow<Pair<InputTrigger.Key, Boolean>> = MutableSharedFlow(extraBufferCapacity = 32, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    private val _mouseButtonFlow: MutableSharedFlow<Pair<InputTrigger.MouseButton, Boolean>> = MutableSharedFlow(extraBufferCapacity = 32, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    private val _mousePositionFlow: MutableSharedFlow<Pair<InputTrigger.MousePosition, Float>> = MutableSharedFlow(extraBufferCapacity = 32, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    private val _mouseDeltaFlow: MutableSharedFlow<Pair<InputTrigger.MouseDelta, Float>> = MutableSharedFlow(extraBufferCapacity = 32, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    private val _gamepadButtonsFlow: MutableStateFlow<BooleanArray> =
        MutableStateFlow(BooleanArray(InputTrigger.GamepadButton.entries.size))
    private val _gamepadAxesFlow: MutableStateFlow<FloatArray> =
        MutableStateFlow(FloatArray(InputTrigger.GamepadAxis.entries.size))

    /**
     * A shared flow that is emitted to whenever a key's state changes and
     * [pollEvents] is called.
     * The [Boolean] is the new state of the key.
     * `true` for "pressed", `false` for "released"
     */
    public actual val keyFlow: SharedFlow<Pair<InputTrigger.Key, Boolean>> = _keyFlow.asSharedFlow()

    /**
     * A shared flow that is emitted to whenever a mouse button's state changes
     * and [pollEvents] is called.
     * The [Boolean] is the new state of the mouse button.
     * `true` for "pressed", `false` for "released"
     */
    public actual val mouseButtonFlow: SharedFlow<Pair<InputTrigger.MouseButton, Boolean>> = _mouseButtonFlow.asSharedFlow()

    /**
     * A shared flow that is emitted to whenever the mouse cursor's position
     * changes and [pollEvents] is called.
     * The position is in pixels relative to the top-left corner of the window.
     *
     * There are no values emitted to this flow if [isMouseDeltaEnabled] is
     * `true` and [isMouseDeltaSupported] is also `true`.
     * If [isMouseDeltaEnabled] is `true` but [isMouseDeltaSupported] is
     * `false`, this flow will emit values as a fallback as the values are
     * readily available.
     */
    public actual val mousePositionFlow: SharedFlow<Pair<InputTrigger.MousePosition, Float>> = _mousePositionFlow.asSharedFlow()

    /**
     * A shared flow that is emitted to whenever the mouse is moved while
     * [isMouseDeltaEnabled] and [pollEvents] is called.
     * The delta is in pixels.
     *
     * There are no values emitted to this flow by default.
     * Only if [isMouseDeltaEnabled] is `true` and [isMouseDeltaSupported] is
     * `true` will values be emitted to this flow.
     * Otherwise, [mousePositionFlow] receives events instead.
     */
    public actual val mouseDeltaFlow: SharedFlow<Pair<InputTrigger.MouseDelta, Float>> = _mouseDeltaFlow.asSharedFlow()

    internal actual val gamepadButtonsFlow: StateFlow<BooleanArray> = _gamepadButtonsFlow.asStateFlow()
    internal actual val gamepadAxesFlow: StateFlow<FloatArray> = _gamepadAxesFlow.asStateFlow()

    private fun onSizeCallback(width: Int, height: Int) {
        _sizeFlow.value = Vector2(width, height)
        _framebufferSizeFlow.value = vector2iFromPtrs { x, y ->
            glfwGetFramebufferSize(glfw.reinterpret(), x, y)
        }
    }

    private fun onPositionCallback(x: Int, y: Int) {
        _positionFlow.value = Vector2(x, y)
    }

    private fun onCursorPositionCallback(x: Double, y: Double) {
        if(isMouseDeltaSupported && isMouseDeltaEnabled) {
            log.debug("Emit mouse delta -> {}, {}", x.toFloat(), y.toFloat())
            _mouseDeltaFlow.tryEmit(InputTrigger.MouseDelta.X to x.toFloat())
            _mouseDeltaFlow.tryEmit(InputTrigger.MouseDelta.Y to y.toFloat())
        } else {
            if(!isMouseDeltaSupported && isMouseDeltaEnabled) {
                log.warn("Ignoring unsupported mouse delta feature, " +
                        "which was enabled by the program")
            }

            if(KemonoEngine.debugInput) {
                log.debug("Emit mouse position -> {}, {}", x.toFloat(), y.toFloat())
            }

            _mousePositionFlow.tryEmit(InputTrigger.MousePosition.X to x.toFloat())
            _mousePositionFlow.tryEmit(InputTrigger.MousePosition.Y to y.toFloat())
        }
    }

    private fun onKeyCallback(key: InputTrigger.Key, action: Int) {
        val actionBool = when(action) {
            GLFW_PRESS -> true
            GLFW_RELEASE -> false
            else -> false
        }

        if(!_keyFlow.tryEmit(key to actionBool)) {
            log.error("Failed to set key state {} -> {}", key, actionBool)
        } else if(KemonoEngine.debugInput) {
            log.debug("Emit key state {} -> {}", key, actionBool)
        }
    }

    private fun onMouseButtonCallback(button: Int, action: Int) {
        val btn = InputTrigger.MouseButton.entries[button]
        val actionBool = when(action) {
            GLFW_PRESS -> true
            GLFW_RELEASE -> false
            else -> false
        }

        if(!_mouseButtonFlow.tryEmit(btn to actionBool)) {
            log.error("Failed to set mouse button state {} -> {}", btn, actionBool)
        } else if(KemonoEngine.debugInput) {
            log.debug("Emit mouse button state {} -> {}", btn, actionBool)
        }
    }

    private var _availableJoysticks = mutableMapOf<PairOf<Long>, Joystick>()

    /**
     * A list of all available joysticks, indexed by their GUID.
     * You can save a joystick GUID to remember the user's preference of
     * joystick, as JIDs are not static.
     */
    public actual val availableJoysticks: Map<PairOf<Long>, Joystick>
        get() = _availableJoysticks.toMap()

    init {
        for(i in 0..<16) {
            if (glfwJoystickPresent(i) == GLFW_TRUE &&
                glfwJoystickIsGamepad(i) == GLFW_TRUE) {
                val joystick = Joystick(
                    i,
                    glfwGetJoystickGUID(i).let {
                        val longs = it?.reinterpret<LongVar>()
                            ?: throw GLFWError()
                        longs[0] to longs[1]
                    },
                    glfwGetJoystickName(i)?.toKString()
                        ?: throw GLFWError()
                )

                _availableJoysticks[joystick.guid] = joystick
            }
        }
    }

    /**
     * The currently selected joystick.
     * Setting this to anything has consequences and will emit events.
     */
    public actual var selectedJoystick: Joystick? = null
        set(value) {
            field = value
            lastGamepadButtonsArray = null
            lastGamepadAxesArray = null
            _gamepadButtonsFlow.value = BooleanArray(InputTrigger.GamepadButton.entries.size)
            _gamepadAxesFlow.value = FloatArray(InputTrigger.GamepadAxis.entries.size)
        }

    private var lastGamepadButtonsArray: BooleanArray? = null
    private var lastGamepadAxesArray: FloatArray? = null

    init {
        if(availableJoysticks.isNotEmpty()) {
            selectedJoystick = availableJoysticks.values.first()
        }
    }

    /**
     * Processes gamepad state.
     */
    @RenderFun
    public actual fun processGamepad() {
        selectedJoystick?.let { joystick ->
            val buttons = joystick.getButtons()
            val axes = joystick.getAxes()

            if (lastGamepadButtonsArray == null ||
                !lastGamepadButtonsArray!!.contentEquals(buttons)) {
                if(KemonoEngine.debugInput) {
                    log.debug("New gamepad button states -> {}", buttons.joinToString())
                }
                _gamepadButtonsFlow.value = buttons
                lastGamepadButtonsArray = buttons
            }

            if (lastGamepadAxesArray == null ||
                !lastGamepadAxesArray!!.contentEquals(axes)) {
                if(KemonoEngine.debugInput) {
                    log.debug("New gamepad button axes -> {}", axes.joinToString())
                }
                _gamepadAxesFlow.value = axes
                lastGamepadAxesArray = axes
            }
        }
    }

    init {
        glfwSetWindowUserPointer(glfw.reinterpret(), _stableRef.asCPointer())
        glfwSetWindowSizeCallback(glfw.reinterpret(), staticCFunction { glfw, width, height ->
            windowCallback(glfw!!) { window -> window.onSizeCallback(width, height) }
        })
        glfwSetWindowPosCallback(glfw.reinterpret(), staticCFunction { glfw, x, y ->
            windowCallback(glfw!!) { window -> window.onPositionCallback(x, y) }
        })
        glfwSetCursorPosCallback(glfw.reinterpret(), staticCFunction { glfw, x, y ->
            windowCallback(glfw!!) { window -> window.onCursorPositionCallback(x, y) }
        })
        glfwSetMouseButtonCallback(glfw.reinterpret(), staticCFunction { glfw, button, action, _ ->
            windowCallback(glfw!!) { window -> window.onMouseButtonCallback(button, action) }
        })
        glfwSetKeyCallback(glfw.reinterpret(), staticCFunction { glfw, key, _, action, _ ->
            if(action == GLFW_REPEAT) return@staticCFunction
            windowCallback(glfw!!) { window -> window.onKeyCallback(InputTrigger.Key.valuesByGlfw[key]!!, action) }
        })
    }

    /**
     * Makes this window's context active.
     * The active context will be used when OpenGL functions are called.
     */
    public actual fun makeActive() {
        if (isActive) return
        glfwMakeContextCurrent(glfw.reinterpret())
    }

    /**
     * Polls for events.
     *
     * @throws GLFWError maybe.
     */
    @RenderFun
    public actual fun pollEvents() {
        glfwPollEvents()
    }

    /**
     * Swaps the buffers in the window, causing the currently selected
     * default framebuffer to be switched with the one that is actually being
     * displayed.
     * This technique is called double-buffering.
     *
     * Also, if [isVsyncEnabled] is `true`, this function waits for the next
     * v-blank.
     */
    @RenderFun
    public actual fun swapBuffers() {
        glfwSwapBuffers(glfw.reinterpret())
    }

    /**
     * Closes this window.
     * Also discards the OpenGL context and state that goes along with it.
     */
    @RenderFun
    actual override fun close() {
        glfwDestroyWindow(glfw.reinterpret())
    }

    public actual companion object {
        /**
         * Initializes the window system.
         *
         * @throws GLFWError Initialization failure!
         */
        @RenderFun
        public actual fun initWindowSystem() {
            if(glfwInit() == GLFW_FALSE)
                throw GLFWError()
        }

        /**
         * Terminates the window system.
         *
         * @throws Nothing Tear-down functions are not to throw.
         */
        @RenderFun
        public actual fun terminateWindowSystem() {
            glfwTerminate()
        }

        /**
         * Gets the current error's description if there is an error currently
         * in context.
         *
         * @return The current error's description, or `null` if there is no
         * error.
         *
         * @throws Nothing This function is not to throw.
         */
        @RenderFun
        public actual fun getErrorDescription(): String? = memScoped {
            val str = alloc<CPointerVar<ByteVar>>()
            glfwGetError(str.ptr)
            str.value?.toKString()
        }

        /**
         * Creates a new window in a windowed state.
         *
         * @param platformService The game to create it for.
         * All windows must be associated with a game.
         * @param width The width of the usable window area, in pixels.
         * @param height The height of the usable window area, in pixels.
         * @param title User-facing title of the window.
         *
         * @return A brand new [Window]. The window is not active.
         *
         * @throws GLFWError Creation error!
         */
        @RenderFun
        public actual fun createWindowed(
            width: Int,
            height: Int,
            title: String
        ): Window {
            glfwDefaultWindowHints()
            glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE)
            glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE)

            if (OperatingSystem.isMacOS) {
                glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GLFW_TRUE)
            }

            if (getenv("KM_DEBUG")?.toKString() == "YES") {
                glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4)
                glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
                glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE)
            } else {
                glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4)
                glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1)
            }

            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

            val glfw = glfwCreateWindow(width, height, title, null, null)
                ?: throw GLFWError()

            return Window(glfw, title)
        }

        /**
         * Creates a new window in a windowed state.
         *
         * @param game The game to create it for.
         * @param width The width of the usable window area, in pixels.
         * @param height The height of the usable window area, in pixels.
         * @param title User-facing title of the window.
         *
         * @return A brand new [Window]. The window is not active.
         *
         * @throws GLFWError Creation error!
         */
        @RenderFun
        @Deprecated("Replaced by the gameless variant", ReplaceWith(
            "createWindowed(width, height, title)",
            "net.derfruhling.kemono.platform.Window.Companion.createWindowed"
        ))
        public actual fun createWindowed(
            game: Game,
            width: Int,
            height: Int,
            title: String
        ): Window {
            return createWindowed(width, height, title)
        }
    }
}