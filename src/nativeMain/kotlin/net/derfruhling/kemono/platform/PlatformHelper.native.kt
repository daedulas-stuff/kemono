/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import kotlinx.cinterop.*
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4

/**
 * Allocates two pointers to [Int] and constructs a [Vector2] from their
 * values assigned by [fn].
 */
@OptIn(ExperimentalForeignApi::class)
public inline fun vector2iFromPtrs(crossinline fn: (x: CPointer<IntVar>, y: CPointer<IntVar>) -> Unit): Vector2<Int> =
    memScoped {
        val x = alloc<IntVar>()
        val y = alloc<IntVar>()

        fn(x.ptr, y.ptr)

        return Vector2(x.value, y.value)
    }

/**
 * Allocates two pointers to [Double] and constructs a [Vector2] from their
 * values assigned by [fn].
 */
@OptIn(ExperimentalForeignApi::class)
public inline fun vector2dFromPtrs(crossinline fn: (x: CPointer<DoubleVar>, y: CPointer<DoubleVar>) -> Unit): Vector2<Double> =
    memScoped {
        val x = alloc<DoubleVar>()
        val y = alloc<DoubleVar>()

        fn(x.ptr, y.ptr)

        return Vector2(x.value, y.value)
    }

/**
 * Converts the vector to an [IntArray] of its elements.
 */
public inline fun Vector2<Int>.asIntArray(): IntArray = intArrayOf(x, y)

/**
 * Converts the vector to an [IntArray] of its elements.
 */
public inline fun Vector3<Int>.asIntArray(): IntArray = intArrayOf(x, y, z)

/**
 * Converts the vector to an [IntArray] of its elements.
 */
public inline fun Vector4<Int>.asIntArray(): IntArray = intArrayOf(x, y, z, w)

/**
 * Converts the vector to an [FloatArray] of its elements.
 */
public inline fun Vector2<Float>.asFloatArray(): FloatArray = floatArrayOf(x, y)

/**
 * Converts the vector to an [FloatArray] of its elements.
 */
public inline fun Vector3<Float>.asFloatArray(): FloatArray = floatArrayOf(x, y, z)

/**
 * Converts the vector to an [FloatArray] of its elements.
 */
public inline fun Vector4<Float>.asFloatArray(): FloatArray = floatArrayOf(x, y, z, w)
