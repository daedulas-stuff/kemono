/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import kotlinx.serialization.Serializable
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.ui.Screen
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.input.sources.WindowGamepadButtonInputSource
import net.derfruhling.kemono.input.sources.WindowGamepadLeftAxisSnappedInputSource
import net.derfruhling.kemono.input.sources.WindowKeyboardInputSource
import net.derfruhling.kemono.input.sources.WindowKeyboardVectorInputSource
import net.derfruhling.kemono.map.*
import net.derfruhling.kemono.map.entities.PlayerEntity
import net.derfruhling.kemono.map.entities.SpriteEntity
import net.derfruhling.kemono.math.*
import net.derfruhling.kemono.physics.BoundingBox
import net.derfruhling.kemono.physics.Interactable
import net.derfruhling.kemono.physics.TriggerAnchor
import net.derfruhling.kemono.platform.OperatingSystem
import net.derfruhling.kemono.savedata.SaveDataCodec
import net.derfruhling.kemono.savedata.SaveDataFile
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import net.derfruhling.kemono.text.LocaleService
import net.derfruhling.kemono.text.TextService
import me.derfruhling.quantum.core.log.logger

/**
 * A test game.
 */
private class TestGame(@Suppress("UNUSED_PARAMETER") args: List<String>) : Game() {
    private val log by logger

    override val referenceName: String = when {
        OperatingSystem.isWindows -> "ThisIsAnExample"
        OperatingSystem.isLinux -> "this-is-an-example"
        OperatingSystem.isMacOS -> "This Is An Example"
        else -> "this-is-an-example" // default to something acceptable
    }

    @Serializable
    data class TestSaveData(
        var x: Float = 0.0f,
        var y: Float = 0.0f
    )

    private val grass = Tile.Oriented("grass", 0, 0, 0, canBeCollidedWith = false)

    private val movementInput by input.vectorInputAction(
        xTriggers = listOf(
            InputTrigger.KeyboardAxis.X,
            InputTrigger.GamepadAxis.LEFT_X
        ),
        yTriggers = listOf(
            InputTrigger.KeyboardAxis.Y,
            InputTrigger.GamepadAxis.LEFT_Y
        )
    )

    private val interactInput by input.buttonInputAction(
        InputTrigger.Key.SPACE
    )

    private var interactInputValueLastFrame = false

    private val testTriggerAnchor = TriggerAnchor(BoundingBox(
        8.0f to 16.0f,
        8.0f to 16.0f
    ))

    private var playerWasInTestTriggerAnchorLastFrame = false

    override suspend fun initialize() {
        super.initialize()

        platform.primaryWindow.title = "test game"
        locale.loadArbitraryLocale("kemono:lang/km_info.lang")

        saveData.registerSaveDataCodec(SaveDataCodec.fromSerializer(TestSaveData.serializer(), ::TestSaveData))

        map.registerTileSpritesheet(0, "kemono:textures/tilemap0.tex", Vector2(16, 16))

        map.tileSet = MutableTileSet()
            .withTiles(grass)
            .build()

        input.addButtonSource(WindowKeyboardInputSource)
        input.addButtonSource(WindowGamepadButtonInputSource)
        input.addAxisSource(WindowGamepadLeftAxisSnappedInputSource)
        input.addAxisSource(WindowKeyboardVectorInputSource::wasd)

        val currentSave = saveData.getSave<TestSaveData>(SaveDataFile.named("save-test"))
        map.setMap(Map(map, MapSource.Null, Vector2(currentSave.x, currentSave.y), Direction.NORTH).apply {
            loadChunk(Vector2(0, 0), explicit = true).fill(grass.borderTopRight)
            loadChunk(Vector2(-1, 0), explicit = true).fill(grass.borderTopLeft)
            loadChunk(Vector2(0, -1), explicit = true).fill(grass.borderBottomRight)
            loadChunk(Vector2(-1, -1), explicit = true).fill(grass.borderBottomLeft)

            insertEntity(TestEntity(1, Vector2(2f, 2f), Direction.NORTH))
            physicsEngine.refresh(this)
        })
    }

    override suspend fun globalUpdate() {
        super.globalUpdate()

        val delta = (movementInput.state.value / Vector2(10.0f,10.0f))
        map.currentMap!!.let { map ->
            map.playerEntity.tryMove(delta * lastFrameTimeAvg)
            map.playerEntity.direction = when {
                delta.y > 0.1f -> Direction.NORTH
                delta.y < 0.1f -> Direction.SOUTH
                delta.x > 0.1f -> Direction.EAST
                delta.x < 0.1f -> Direction.WEST
                else -> map.playerEntity.direction
            }

            if(interactInput.state.value && !interactInputValueLastFrame) {
                map.playerEntity.triggerInteraction()
                interactInputValueLastFrame = true
            } else if(!interactInput.state.value && interactInputValueLastFrame) {
                interactInputValueLastFrame = false
            }
        }



        if(map.currentMap!!.playerEntity in testTriggerAnchor) {
            if(!playerWasInTestTriggerAnchorLastFrame) {
                playerWasInTestTriggerAnchorLastFrame = true
                log.debug("Player is now in test trigger")
            }
        } else if(playerWasInTestTriggerAnchorLastFrame) {
            playerWasInTestTriggerAnchorLastFrame = false
            log.debug("Player no longer in test trigger")
        }
    }

    override suspend fun tearDown() {
        saveData.getSave<TestSaveData>(SaveDataFile.named("save-test")).apply {
            map.currentMap!!.playerEntity.let { entity ->
                x = entity.position.x
                y = entity.position.y
            }
        }

        super.tearDown()
    }

    private class TestEntity(
        override val id: Int,
        @set:Deprecated("Use setPosition()")
        override var position: Vector2<Float>,
        override var direction: Direction
    ) : SpriteEntity(), Component, Interactable {
        private val log by logger
        private val renderService: RenderService by service()
        private val textService: TextService by service()

        private val textureRef = renderService.texturePool["kemono:textures/fonts/km_mono.tex"]

        override val size: Vector2<Float> = Vector2(1f, 1f)
        override val originOffset: Vector2<Float> = Vector2(0.5f, 0.5f)
        override val texture: Texture get() = textureRef.value

        override val textureCorners: Corners<Vector2<Float>>
            get() = textService.monospaceFont.mapCharacter('A')
                ?: textService.monospaceFont.mapCharacter('?')!!

        override suspend fun onInteract(playerEntity: PlayerEntity, interactingRay: Ray) {
            log.debug("Interaction! {} with {}", playerEntity, interactingRay)
        }

        override fun dispose() {
            super.dispose()
            textureRef.dispose()
        }
    }

    private class TestScreen : Screen(), Component {
        private val localeService: LocaleService by service()

        val text = text("${localeService["km.name"]} ${localeService["km.version"]}")

        override fun render() {
            text.render()
        }

        override suspend fun update() {}

        override fun dispose() {
            text.dispose()
        }
    }
}

/**
 * Main function.
 *
 * kotlin why do i need to document this fun
 */
public fun main(args: Array<String>): Unit = runGame(args, ::TestGame)
