/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.savedata

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString
import net.derfruhling.kemono.scopedGame
import okio.Path
import okio.Path.Companion.toPath
import platform.posix.getenv
import platform.posix.getpwuid
import platform.posix.getuid

@OptIn(ExperimentalForeignApi::class)
private val homeDir: String
    get() {
        val homeDirEnv = getenv("HOME")
        if(homeDirEnv != null)
            return homeDirEnv.toKString()

        // otherwise, default to the passwd entry
        val pw = getpwuid(getuid())
        return pw?.pointed?.pw_dir?.toKString()
            ?: throw RuntimeException("Could not find user home directory")
    }

public actual val gameDataDir: Path by lazy {
    homeDir.toPath()
        .resolve(".local")
        .resolve("share")
        .resolve("kemono-game")
        .resolve(scopedGame.referenceName)
}

public actual val saveDataDir: Path
    get() = gameDataDir.resolve("save-data")
