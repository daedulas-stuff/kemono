import net.derfruhling.kemono.map.MapChunk
import net.derfruhling.kemono.map.Tile
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.physics.BoundingBox
import net.derfruhling.kemono.physics.PhysicsChunk
import kotlin.test.*

class PhysicsTest {
    @Test
    fun testBoundingBoxIntersectSuccess() {
        val a = BoundingBox(0.25f, 1.25f, 0.5f, 0.75f)
        val b = BoundingBox(0.5f, 1.75f, 0.6f, 0.8f)
        assertTrue("a.x does not intersect b.x") { a xIntersectsWith b }
        assertTrue("a.y does not intersect b.y") { a yIntersectsWith b }
        assertTrue("a as whole does not intersect b") { a intersectsWith b }
        assertNotNull(a.introspect(b))
    }

    @Test
    fun testBoundingBoxIntersectOnXAxis() {
        val a = BoundingBox(0.25f, 1.25f, 0.5f, 0.75f)
        val b = BoundingBox(0.5f, 1.75f, 0.8f, 1.0f)
        assertTrue("a.x does not intersect b.x") { a xIntersectsWith b }
        assertFalse("a.y intersects b.y") { a yIntersectsWith b }
        assertFalse("a as whole intersects b") { a intersectsWith b }
        assertNull(a.introspect(b))
    }

    @Test
    fun testBoundingBoxIntersectOnYAxis() {
        val a = BoundingBox(0.25f, 1.25f, 0.5f, 0.75f)
        val b = BoundingBox(-2.25f, -1.75f, 0.6f, 0.8f)
        assertFalse("a.x intersects b.x") { a xIntersectsWith b }
        assertTrue("a.y does not intersect b.y") { a yIntersectsWith b }
        assertFalse("a as whole intersects b") { a intersectsWith b }
        assertNull(a.introspect(b))
    }

    @Test
    fun testBoundingBoxIntersectNoIntersection() {
        val a = BoundingBox(0.25f, 1.25f, 0.5f, 0.75f)
        val b = BoundingBox(-2.25f, -1.75f, 0.8f, 1.0f)
        assertFalse("a.x intersects b.x") { a xIntersectsWith b }
        assertFalse("a.y intersects b.y") { a yIntersectsWith b }
        assertFalse("a as whole intersects b") { a intersectsWith b }
        assertNull(a.introspect(b))
    }
    
    @Test
    fun testPhysicsChunkConstruction() {
        val stone = Tile("stone", Tile.INVISIBLE, Vector2(0, 0), true)
        val mapChunk = MapChunk(
            position = Vector2(2, 3),
            tiles = Array(MapChunk.SIZE * MapChunk.SIZE) { Tile.AIR }.also {
                for (x in 0..1) {
                    for (y in 0..<MapChunk.SIZE) {
                        it[(y * MapChunk.SIZE) + x] = stone
                    }
                }
            },
            isExplicitlyLoaded = true
        )
        
        val chunk = PhysicsChunk(mapChunk)
        val expectedChunk = PhysicsChunk(boxes = listOf(
            BoundingBox(
                32.0f,
                34.0f,
                48.0f,
                64.0f
            )
        ))
        
        assertEquals(expectedChunk, chunk)
    }
}