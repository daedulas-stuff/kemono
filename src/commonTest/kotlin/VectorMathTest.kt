/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.asAngle
import kotlin.test.Test
import kotlin.test.assertEquals

class VectorMathTest {
    @Test
    fun testAsAngle() {
        val vec = Vector2(0.70710677f, -0.70710677f)
        assertEquals(2.3561945f, vec.asAngle.radians, 0.000005f)
    }
}