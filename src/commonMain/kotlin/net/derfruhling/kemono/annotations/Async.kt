/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.annotations

import kotlinx.coroutines.*
import net.derfruhling.kemono.Game

/**
 * Indicates that a function follows the [@Async][Async] pattern,
 * which entails:
 * - The function returns [Job] or [Deferred].
 * - The function returns as soon as possible.
 * - The function starts at least one async task on the [Game]-level [CoroutineScope].
 * - The returned object is completed when the async task has finished.
 *
 * @see CompletableJob.complete
 * @see CompletableJob.completeExceptionally
 * @see CompletableDeferred.complete
 * @see CompletableDeferred.completeExceptionally
 */
@MustBeDocumented
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.BINARY)
public annotation class Async
