/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.annotations

import net.derfruhling.kemono.Game

/**
 * Marks a render function.
 * Functions marked as [@RenderFun][RenderFun] should only be accessed from
 * the render thread.
 * Doing so outside the render thread is considered a mistake, as the render
 * API used by Kemono (OpenGL) is not thread-safe.
 *
 * The annotation can also be applied to a function type to show that the
 * function will be executed on the render thread, so it's safe to run render
 * functions inside the block.
 *
 * Use [Game.runOnRenderThread] if you're not sure that you're running
 * on the right thread.
 * It will optimize in the case that you are running on the right thread.
 */
@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.TYPE,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.SOURCE)
public annotation class RenderFun
