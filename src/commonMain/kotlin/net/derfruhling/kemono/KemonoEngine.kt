/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import net.derfruhling.kemono.platform.Window
import me.derfruhling.quantum.core.log.Log
import me.derfruhling.quantum.core.log.LogReceiver
import me.derfruhling.quantum.core.log.print
import me.derfruhling.quantum.core.log.rollingFile
import okio.Path.Companion.toPath

/**
 * Debug values kept globally.
 */
public interface KemonoEngineValues {
    /**
     * If `true`, shader contents will be dumped to a file, and the path will
     * be logged as `DEBUG`.
     */
    public val dumpShaderContents: Boolean

    /**
     * If `true`, buffer contents will be dumped to a file, and the path will
     * be logged as `DEBUG`.
     */
    public val dumpBufferContents: Boolean

    /**
     * If `true`, `MeshBuilder` returns a special instance of itself that logs
     * all important operations as `DEBUG`.
     */
    public val debugMeshBuilder: Boolean

    /**
     * If `true`, [Window] will emit a ton of debug messages whenever something
     * about input happens.
     */
    public val debugInput: Boolean

    /**
     * Definitely!
     */
    public data class Definite(
        override val dumpBufferContents: Boolean,
        override val dumpShaderContents: Boolean,
        override val debugMeshBuilder: Boolean,
        override val debugInput: Boolean
    ) : KemonoEngineValues

    /**
     * A mutable values object. Can be mutated.
     */
    public data class Mutable(
        override var dumpShaderContents: Boolean = false,
        override var dumpBufferContents: Boolean = false,
        override var debugMeshBuilder: Boolean = false,
        override var debugInput: Boolean = false
    ) : KemonoEngineValues {
        /**
         * Converts this mutable render values object into a not mutable
         * [Definite] object.
         */
        public fun build(): KemonoEngineValues = Definite(
            dumpBufferContents,
            dumpShaderContents,
            debugMeshBuilder,
            debugInput
        )

        /**
         * Sets everything to true.
         */
        public fun setAll() {
            dumpShaderContents = true
            dumpBufferContents = true
            debugMeshBuilder = true
            debugInput = true
        }
    }

    /**
     * An entire values object that returns `false` for everything.
     */
    public object Default : KemonoEngineValues {
        override val dumpShaderContents: Boolean
            get() = false
        override val dumpBufferContents: Boolean
            get() = false
        override val debugMeshBuilder: Boolean
            get() = false
        override val debugInput: Boolean
            get() = false
    }

    /**
     * Overrides values from [base].
     *
     * @property base The base values.
     * @property dumpShaderContentsOverride [dumpShaderContents]
     * @property dumpBufferContentsOverride [dumpBufferContents]
     * @property debugMeshBuilderOverride [debugMeshBuilder]
     * @property debugInputOverride [debugInput]
     */
    public data class Overrides(
        public val base: KemonoEngineValues,
        public val dumpShaderContentsOverride: Boolean? = null,
        public val dumpBufferContentsOverride: Boolean? = null,
        public val debugMeshBuilderOverride: Boolean? = null,
        public val debugInputOverride: Boolean? = null
    ) : KemonoEngineValues {
        override val dumpShaderContents: Boolean
            get() = dumpShaderContentsOverride ?: base.dumpShaderContents
        override val dumpBufferContents: Boolean
            get() = dumpBufferContentsOverride ?: base.dumpBufferContents
        override val debugMeshBuilder: Boolean
            get() = debugMeshBuilderOverride ?: base.debugMeshBuilder
        override val debugInput: Boolean
            get() = debugInputOverride ?: base.debugInput
    }
}

/**
 * Hello, world!
 */
public object KemonoEngine {
    private val _stack = mutableListOf<KemonoEngineValues>(KemonoEngineValues.Default)

    /**
     * Gets the current set of values that the engine has set.
     */
    public var currentValues: KemonoEngineValues = KemonoEngineValues.Default
        private set

    /**
     * Alias of [currentValues].[dumpShaderContents][KemonoEngineValues.dumpShaderContents]
     */
    public inline val dumpShaderContents: Boolean
        get() = currentValues.dumpShaderContents

    /**
     * Alias of [currentValues].[dumpBufferContents][KemonoEngineValues.dumpBufferContents]
     */
    public inline val dumpBufferContents: Boolean
        get() = currentValues.dumpBufferContents

    /**
     * Alias of [currentValues].[debugMeshBuilder][KemonoEngineValues.debugMeshBuilder]
     */
    public inline val debugMeshBuilder: Boolean
        get() = currentValues.debugMeshBuilder

    /**
     * Alias of [currentValues].[debugInput][KemonoEngineValues.debugInput]
     */
    public inline val debugInput: Boolean
        get() = currentValues.debugInput

    private const val LOG_FORMAT = "{date} {level:3U} @{source} -> {message}{n}{exception}"

    /**
     * Initializes the engine.
     */
    public fun initialize() {
        Log.registerReceiver(LogReceiver.print(LOG_FORMAT))
        Log.registerReceiver(LogReceiver.rollingFile("logs".toPath().resolve("latest.log"), LOG_FORMAT))

        Window.initWindowSystem()
    }

    @PublishedApi
    internal fun pushStack(newEntry: KemonoEngineValues) {
        _stack.add(newEntry)
        currentValues = newEntry
    }

    @PublishedApi
    internal inline fun pushStack(values: (KemonoEngineValues) -> KemonoEngineValues) {
        pushStack(values(currentValues))
    }

    @PublishedApi
    internal fun popStack() {
        if (_stack.size == 1)
            throw IllegalStateException("Cannot pop last stack value")
        _stack.removeLast()
        currentValues = _stack.last()
    }

    /**
     * Executes block [fn] with a set of debug values in context.
     */
    public inline fun withDebug(
        dumpShaderContentsOverride: Boolean? = null,
        dumpBufferContentsOverride: Boolean? = null,
        debugMeshBuilderOverride: Boolean? = null,
        debugInputOverride: Boolean? = null,
        fn: () -> Unit
    ) {
        try {
            pushStack { base ->
                KemonoEngineValues.Overrides(
                    base,
                    dumpShaderContentsOverride,
                    dumpBufferContentsOverride,
                    debugMeshBuilderOverride,
                    debugInputOverride
                )
            }

            fn()
        } finally {
            popStack()
        }
    }

    /**
     * Executes block [fn] with a set of debug values in context.
     */
    public inline fun withDebug(
        values: KemonoEngineValues,
        fn: () -> Unit
    ) {
        try {
            pushStack(values)
            fn()
        } finally {
            popStack()
        }
    }

    /**
     * Executes block [fn] with [KemonoEngineValues.Default] values
     * that disable debugging.
     */
    public inline fun withoutDebug(fn: () -> Unit) {
        try {
            pushStack { _ -> KemonoEngineValues.Default }
            fn()
        } finally {
            popStack()
        }
    }

    /**
     * Tears down the engine.
     */
    public fun tearDown() {
        Window.terminateWindowSystem()

        Log.close()
    }
}

/**
 * Initializes and tears down the engine, running block in-between.
 * Also catches [spoons][ThereIsNoSpoonException].
 */
public inline fun kmProgram(fn: () -> Unit) {
    try {
        KemonoEngine.initialize()
        fn()
    } catch (e: ThereIsNoSpoonException) {
        throw IllegalStateException(
            "spoon found in stack; definite mistake, ThereIsNoSpoonException is not to be thrown",
            e
        )
    } finally {
        KemonoEngine.tearDown()
    }
}

/**
 * Configures specific debug values in the engine.
 *
 * @param configure The configuration block.
 * @param fn The code block to execute.
 *
 * @see KemonoEngine.withDebug
 */
public inline fun kmConfigure(configure: KemonoEngineValues.Mutable.() -> Unit, fn: () -> Unit) {
    val mutable = KemonoEngineValues.Mutable()
    mutable.configure()
    KemonoEngine.withDebug(mutable.build(), fn)
}

/**
 * Initializes and tears down the engine, running block in-between.
 * Also catches [spoons][ThereIsNoSpoonException].
 * Also also configures the debug value stack with [configure].
 *
 * @param configure The configuration block.
 * @param fn The code block to execute.
 *
 * @see kmProgram
 * @see kmConfigure
 */
public inline fun kmProgram(configure: KemonoEngineValues.Mutable.() -> Unit, fn: () -> Unit) {
    kmProgram {
        kmConfigure(configure, fn)
    }
}
