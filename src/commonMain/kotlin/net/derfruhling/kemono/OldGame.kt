/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

//package net.derfruhling.kemono
//
//import kotlinx.coroutines.*
//import kotlinx.datetime.Clock
//import net.derfruhling.kemono.annotations.InternalAPI
//import net.derfruhling.kemono.annotations.RenderFun
//import net.derfruhling.kemono.error.IllegalResourceTypeException
//import net.derfruhling.kemono.graphics.*
//import net.derfruhling.kemono.graphics.interfaces.*
//import net.derfruhling.kemono.graphics.structures.UniformValue
//import net.derfruhling.kemono.input.AxisInputTrigger
//import net.derfruhling.kemono.input.ButtonInputTrigger
//import net.derfruhling.kemono.input.InputService
//import net.derfruhling.kemono.input.actions.*
//import net.derfruhling.kemono.map.Map
//import net.derfruhling.kemono.map.MapChunk
//import net.derfruhling.kemono.map.Tile
//import net.derfruhling.kemono.map.TileSet
//import net.derfruhling.kemono.map.entities.EntityUniformToken
//import net.derfruhling.kemono.math.Vector2
//import net.derfruhling.kemono.platform.AutoDisposingDelegate
//import net.derfruhling.kemono.platform.DisposingDelegate
//import net.derfruhling.kemono.platform.Window
//import net.derfruhling.kemono.resources.ResourceStorage
//import net.derfruhling.kemono.resources.Resources
//import net.derfruhling.kemono.resources.husks.ShaderResource.UniformAcceptValue.*
//import net.derfruhling.kemono.resources.read
//import me.derfruhling.quantum.core.async.ThreadId
//import me.derfruhling.quantum.core.async.maxConcurrency
//import me.derfruhling.quantum.core.interfaces.Disposable
//import me.derfruhling.quantum.core.interfaces.use
//import me.derfruhling.quantum.core.log.Logger
//import me.derfruhling.quantum.core.log.logger
//import kotlin.math.absoluteValue
//import kotlin.reflect.KProperty
//import net.derfruhling.kemono.map.Map as KmMap
//import net.derfruhling.kemono.resources.husks.ShaderResource as ShaderResourceHusk
//import net.derfruhling.kemono.resources.husks.TextureResource as TextureResourceHusk
//import kotlin.collections.Map as KMap
//
///**
// * Specifies a game and it's top-level logic.
// */
//public abstract class OldGame : Disposable {
//    private val log by logger
//
//    init {
//        log.info("Starting game")
//    }
//
//    /**
//     * Creates the [ResourceStorage] instance for this [Game].
//     * All resources should be loaded from this storage instance.
//     *
//     * @return A new [ResourceStorage] instance.
//     * The default points to `./resources/`.
//     *
//     * @throws okio.IOException Maybe.
//     */
//    protected open fun createGameResourceStorage(): ResourceStorage =
//        Resources.createFileSystemStorage()
//
//    /**
//     * Creates the [Graphics] instance for this [Game].
//     * This exists to allow a game to override [Graphics] and implement
//     * their own functionality overtop it, while still using the existing
//     * graphics code.
//     */
//    protected open fun createGraphics(): Graphics = Graphics()
//
//    /**
//     * The platform [Window] that this [Game] renders to.
//     */
//    public val window: Window = Window.createWindowed(this, 800, 600, "kemono")
//
//    /**
//     * The input system. TODO
//     */
//    public val inputSystem: InputService = InputService(this)
//
//    /**
//     * The thread that this [Game] started on.
//     * This is assumed to be the main thread.
//     * Whether it actually is... is not relevant (or known).
//     */
//    public val mainThread: ThreadId = ThreadId.current()
//
//    /**
//     * The [ResourceStorage] instance for this [Game].
//     * All resources are to be loaded from here.
//     *
//     * @see createGameResourceStorage
//     */
//    public val resourceStorage: ResourceStorage by lazy { createGameResourceStorage() }
//
//    /**
//     * The [Graphics] instance for this [Game].
//     *
//     * @see createGraphics
//     */
//    public val graphics: Graphics by lazy { createGraphics() }
//
//    /**
//     * A [Pool] that automatically loads and destroys shader resources.
//     */
//    public val shaderPool: Pool<ShaderResource> = object : Pool<ShaderResource>() {
//        override fun load(name: String): ShaderResource {
//            return createShaderResource(name)
//        }
//    }
//
//    /**
//     * A [Pool] that automatically loads and destroys shader resources.
//     */
//    public val texturePool: Pool<Texture> = object : Pool<Texture>() {
//        override fun load(name: String): Texture {
//            return createTextureResource(name)
//        }
//    }
//
//    /**
//     * The currently set map.
//     * [Game] will render and update this, if it's not `null`.
//     */
//    public var map: KmMap? = null
//        private set
//
//    /**
//     * The tile set used by [map]s.
//     */
//    public abstract val tileSet: TileSet
//
//    /**
//     * The number of tiles from the center to the top of the rendered window.
//     * This is used to calculate how much to scale down the graphics.
//     */
//    public open val tilesToTop: Int get() = 6
//
//    init {
//        window.isVsyncEnabled = true
//        window.makeActive()
//    }
//
//    private val renderThreadDispatcher = RenderThreadDispatcher()
//    private val renderThreadScope = CoroutineScope(renderThreadDispatcher)
//    private val _asyncThreadPool = newFixedThreadPoolContext(maxConcurrency, "km_async")
//    private val _tileSpritesheets = mutableMapOf<Int, Spritesheet>()
//
//    /**
//     * An asynchronous thread pool for running things asynchronously.
//     * Disposed of by this [Game] when it dies.
//     */
//    public val asyncThreadPool: CoroutineDispatcher get() = _asyncThreadPool
//
//    /**
//     * A [CoroutineScope] wrapping [asyncThreadPool], allowing tasks to be
//     * started arbitrarily.
//     */
//    public val asyncCoroutineScope: CoroutineScope = CoroutineScope(asyncThreadPool)
//
//    /**
//     * This is the desired frame rate, in frames per second.
//     */
//    public open val desiredFrameRate: Int get() = 60
//
//    /**
//     * The shader that is used for tile rendering.
//     * The `CHUNK_POSITION` uniform source is provided to this shader whenever
//     * it's used.
//     */
//    public open val tileRendererShader: ShaderResource = createShaderResource("kemono:shaders/textured/basic/2d/cam.shdr")
//
//    /**
//     * A set of spritesheets that contain the textures used for drawing tiles.
//     */
//    public val tileSpritesheets: KMap<Int, Spritesheet> = _tileSpritesheets
//
//    private val frameTimeStream: FrameTimeStream = object : FrameTimeStream() {
//        override val desiredFrameRate: Int
//            get() = this@Game.desiredFrameRate
//    }
//
//    /**
//     * This the amount of time compared to [desiredFrameRate] that the last
//     * frame took.
//     * This allows the game to adjust its actions based on the current frame
//     * rate by multiplying additive values with this.
//     */
//    public var lastFrameTimeAvg: Float = frameTimeStream.next()
//        private set
//
//    override fun dispose() {
//        // dispose some things before async thread pool
//        map?.dispose()
//
//        tileRendererShader.dispose()
//
//        // some things may break by this being disposed!
//        log
//        _asyncThreadPool.close()
//
//        AutoDisposingDelegate.dispose()
//        window.close()
//    }
//
//    /**
//     * Sets the current [map][Game.map].
//     * If a map already exists in [map][Game.map], it is [Map.dispose]d.
//     *
//     * @param map The new map.
//     */
//    public fun setMap(map: Map) {
//        log.info("Changing map to {}", map)
//        this.map?.dispose()
//        this.map = map
//    }
//
//    /**
//     * Registers a tile spritesheet for use in map rendering.
//     *
//     * @param slot The slot number of the spritesheet.
//     * See [Tile.textureSlot].
//     * @param name The name of the texture resource to load as a spritemap.
//     * @param spriteCount This is the number of sprites contained in the
//     * texture per dimension of the texture.
//     * Ex.
//     * `[16, 16]` means that there are 16 sprites over the X axis and 16 sprites
//     * over the Y axis, and `[8, 24]` would mean that there are 8 sprites over
//     * the X axis and 24 sprites over Y axis.
//     */
//    protected fun registerTileSpritesheet(slot: Int, name: String, spriteCount: Vector2<Int>) {
//        _tileSpritesheets[slot] = createSpritesheet(name, spriteCount)
//    }
//
//    private fun createShaderResource(name: String): ShaderResource {
//        try {
//            val shader = resourceStorage[name].read<ShaderResourceHusk>()
//            return graphics.createShaderFromResource(shader)
//        } catch (e: RuntimeException) {
//            throw RuntimeException("Exception encountered loading resource $name", e)
//        }
//    }
//
//    private fun createTextureResource(name: String): Texture {
//        val texture = resourceStorage[name].read<TextureResourceHusk>()
//        return graphics.createTextureFromResource(texture)
//    }
//
//    @Throws(IllegalResourceTypeException::class)
//    private fun createSpritesheet(name: String, spriteCount: Vector2<Int>): Spritesheet {
//        val texture = createTextureResource(name)
//
//        if(texture !is Texture2D) {
//            try {
//                texture.dispose()
//            } catch (e: Exception) {
//                log.error("Error disposing texture due to illegal resource type", e)
//            }
//
//            throw IllegalResourceTypeException("Texture $name is not a Texture2D")
//        }
//
//        return Spritesheet(
//            texture,
//            spriteCount
//        )
//    }
//
//    /**
//     * Creates a new button input action.
//     * Whenever any of the [triggers] are triggered, this value's
//     * [InputAction.state] is set to `true`, and remains `true` until all
//     * the triggers are finished.
//     *
//     * @param triggers A list of triggers.
//     *
//     * @return A delegate for the action.
//     * This value should not be disposed manually.
//     */
//    public fun buttonInputAction(vararg triggers: ButtonInputTrigger): DisposingDelegate<SimpleInputAction> {
//        return object : DisposingDelegate<SimpleInputAction> {
//            private val action = BasicSimpleInputAction(*triggers)
//
//            init {
//                inputSystem.registerButtonAcceptor(action)
//
//                AutoDisposingDelegate.register(this)
//            }
//
//            override fun getValue(self: Any, property: KProperty<*>): SimpleInputAction {
//                return action
//            }
//
//            override fun dispose() {
//                inputSystem.deregisterButtonAcceptor(action)
//            }
//        }
//    }
//
//    /**
//     * Creates a new axis input action.
//     * Whenever any of the [triggers] are triggered, this value's
//     * [InputAction.state] is set to the value of the trigger with the
//     * most significant value, which is defined by comparing [absoluteValue].
//     *
//     * @param triggers A list of triggers.
//     *
//     * @return A delegate for the action.
//     * This value should not be disposed manually.
//     */
//    public fun axisInputAction(vararg triggers: AxisInputTrigger): DisposingDelegate<AxisInputAction> {
//        return object : DisposingDelegate<AxisInputAction> {
//            private val action = BasicAxisInputAction(*triggers)
//
//            init {
//                inputSystem.registerAxisAcceptor(action)
//
//                AutoDisposingDelegate.register(this)
//            }
//
//            override fun getValue(self: Any, property: KProperty<*>): AxisInputAction {
//                return action
//            }
//
//            override fun dispose() {
//                inputSystem.deregisterAxisAcceptor(action)
//            }
//        }
//    }
//
//    /**
//     * Creates a new vector input action.
//     * Whenever any of the `triggers` are triggered, this value's
//     * [InputAction.state] is set to a vector with the most significant values
//     * of [xTriggers] and [yTriggers], which go into `x` and `y`, respectively.
//     *
//     * @param xTriggers A list of triggers used for the `x` part.
//     * @param yTriggers A list of triggers used for the `y` part.
//     *
//     * @return A delegate for the action.
//     * This value should not be disposed manually.
//     */
//    public fun vectorInputAction(
//        xTriggers: List<AxisInputTrigger>,
//        yTriggers: List<AxisInputTrigger>
//    ): DisposingDelegate<VectorInputAction> {
//        return object : DisposingDelegate<VectorInputAction> {
//            private val action = BasicVectorInputAction(xTriggers, yTriggers)
//
//            init {
//                inputSystem.registerAxisAcceptor(action)
//
//                AutoDisposingDelegate.register(this)
//            }
//
//            override fun getValue(self: Any, property: KProperty<*>): VectorInputAction {
//                return action
//            }
//
//            override fun dispose() {
//                inputSystem.deregisterAxisAcceptor(action)
//            }
//        }
//    }
//
//    /**
//     * Renders the game.
//     * Calls all render related functions in this game in the correct order.
//     */
//    public fun render() {
//        if (ThreadId.current() != mainThread)
//            throw IllegalStateException("render() must be called on main thread")
//
//        globalPreRender()
//
//        @OptIn(InternalAPI::class)
//        renderThreadDispatcher.endFrame()
//
//        runBlocking {
//            try {
//                @OptIn(InternalAPI::class)
//                renderThreadDispatcher.asFlow().collect(Runnable::run)
//            } catch (ignored: CancellationException) {}
//        }
//
//        execRender()
//        globalPostRender()
//    }
//
//    /**
//     * Updates the game state.
//     * Supports asynchronous coroutines!
//     *
//     * @see execPreUpdate
//     * @see execUpdate
//     * @see execPostUpdate
//     */
//    public suspend fun update(): Unit = withContext(asyncThreadPool) {
//        coroutineScope { execPreUpdate() }
//        coroutineScope { execUpdate() }
//        coroutineScope { execPostUpdate() }
//    }
//
//    /**
//     * _Updates_ the [Pool] objects contained in this game.
//     * This function does not call [Pool.cleanup].
//     *
//     * @see cleanupPools
//     */
//    protected open suspend fun updatePools(): Unit = coroutineScope {
//        launch { shaderPool.update() }
//        launch { texturePool.update() }
//    }
//
//    /**
//     * Cleans up [Pool] objects. Called after [updatePools].
//     */
//    protected open suspend fun cleanupPools(): Unit = coroutineScope {
//        launch { shaderPool.cleanup(this@Game) }
//        launch { texturePool.cleanup(this@Game) }
//    }
//
//    /**
//     * Executes the pre-update stage of the game.
//     * This can be overriden, but it's probably more helpful to override
//     * [globalPreUpdate].
//     */
//    protected open fun CoroutineScope.execPreUpdate() {
//        launch {
//            updatePools()
//
//            // isn't it beautiful?
//            launch { globalPreUpdate() }
//            launch { map?.updateRenderDistance() }
//        }
//    }
//
//    /**
//     * Executes the update stage of the game.
//     * This can be overriden, but it's probably more helpful to override
//     * [globalUpdate].
//     */
//    protected open fun CoroutineScope.execUpdate() {
//        launch { globalUpdate() }
//        launch { map?.update() }
//    }
//
//    /**
//     * Executes the post-update stage of the game.
//     * This can be overriden, but it's probably more helpful to override
//     * [globalPostUpdate].
//     */
//    protected open fun CoroutineScope.execPostUpdate() {
//        launch { globalPostUpdate() }
//    }
//
//    /**
//     * The global side of the pre-update stage in the game.
//     * The default implementation tells [Window] to process gamepad events,
//     * if a gamepad is set.
//     */
//    protected open suspend fun globalPreUpdate() {
//        window.processGamepad()
//    }
//
//    /**
//     * The global side of the update stage in the game.
//     */
//    protected open suspend fun globalUpdate() {}
//
//    /**
//     * The global side of the post-update stage in the game.
//     */
//    protected open suspend fun globalPostUpdate() {}
//
//    /**
//     * The global side of the pre-render stage in the game.
//     * The default implementation clears the screen.
//     */
//    protected open fun globalPreRender() {
//        graphics.clear()
//    }
//
//    /**
//     * Executes the render stage in the game.
//     *
//     * @see globalPreRenderGame
//     * @see globalPostRenderGame
//     */
//    protected open fun execRender() {
//        globalPreRenderGame()
//        renderMap()
//        globalPostRenderGame()
//    }
//
//    /**
//     * Run in [execRender]'s default implementation before rendering the game.
//     * Perform any additional preparation before each frame in here.
//     */
//    protected open fun globalPreRenderGame() {}
//
//    /**
//     * Run in [execRender]'s default implementation after rendering the game.
//     * Perform any additional UI rendering or cleanup in here.
//     */
//    protected open fun globalPostRenderGame() {}
//
//    /**
//     * Renders the map if [map] is not null.
//     */
//    public open fun renderMap() {
//        map?.render()
//    }
//
//    /**
//     * The global side of the post-render stage in the game.
//     */
//    protected open fun globalPostRender() {}
//
//    /**
//     * Run by [run] before everything else.
//     */
//    protected open suspend fun initialize() {}
//
//    /**
//     * Run by [run] after the game is closed.
//     */
//    protected open suspend fun tearDown() {}
//
//    /**
//     * Runs the game.
//     */
//    public fun run(): Unit = runBlocking {
//        initialize()
//
//        var now = Clock.System.now()
//        window.isVisible = true
//
//        while (!window.shouldClose) {
//            window.pollEvents()
//
//            update()
//            render()
//
//            window.swapBuffers()
//
//            val end = Clock.System.now()
//
//            frameTimeStream.put(end - now)
//            lastFrameTimeAvg = frameTimeStream.next()
//
//            now = end
//        }
//
//        window.isVisible = false
//        tearDown()
//    }
//
//    /**
//     * Launches a coroutine in [asyncCoroutineScope].
//     *
//     * @param fn A block to execute.
//     */
//    public inline fun launch(noinline fn: suspend CoroutineScope.() -> Unit): Job {
//        return asyncCoroutineScope.launch(block = fn)
//    }
//
//    /**
//     * Launches a coroutine in [asyncCoroutineScope] and returns a [Deferred]
//     * that will provide the returned value when the coroutine completes.
//     *
//     * @param fn A block to execute.
//     *
//     * @see launch
//     */
//    public inline fun <T> async(noinline fn: suspend CoroutineScope.() -> T): Deferred<T> {
//        return asyncCoroutineScope.async(block = fn)
//    }
//
//    @PublishedApi
//    internal fun publishToRenderThreadDispatcher(fn: () -> Unit) {
//        renderThreadScope.launch { fn() }
//    }
//
//    /**
//     * Runs a block on the render thread the next time [render] is called.
//     *
//     * @param fn The function to run.
//     * If [ThreadId.current()][ThreadId.current] is the same as [mainThread],
//     * `fn()` is instead immediately inlined into this function and executed.
//     */
//    public inline fun runOnRenderThread(crossinline fn: @RenderFun () -> Unit) {
//        if(ThreadId.current() == mainThread) {
//            fn()
//        } else {
//            publishToRenderThreadDispatcher { fn() }
//        }
//    }
//}
