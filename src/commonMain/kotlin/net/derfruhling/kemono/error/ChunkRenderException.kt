/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.error

import net.derfruhling.kemono.math.Vector2

/**
 * An exception that was thrown while any singular chunk was rendering in the
 * context of a map.
 */
public class ChunkRenderException : RuntimeException {
    /**
     * The position of the chunk that encountered the exception while rendering.
     */
    public val chunkPosition: Vector2<Int>

    public constructor(chunkPosition: Vector2<Int>) : super() {
        this.chunkPosition = chunkPosition
    }

    public constructor(message: String?, chunkPosition: Vector2<Int>) : super(message) {
        this.chunkPosition = chunkPosition
    }

    public constructor(message: String?, cause: Throwable?, chunkPosition: Vector2<Int>) : super(message, cause) {
        this.chunkPosition = chunkPosition
    }

    public constructor(cause: Throwable?, chunkPosition: Vector2<Int>) : super(cause) {
        this.chunkPosition = chunkPosition
    }
}