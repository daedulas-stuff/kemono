/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2

/**
 * Represents a four-way direction. Entities can only face one of these
 * four ways.
 *
 * @see Entity
 */
@Serializable
public enum class Direction {
    /**
     * North: towards positive Y.
     */
    @SerialName("north")
    NORTH,

    /**
     * East: towards positive X.
     */
    @SerialName("east")
    EAST,

    /**
     * South: towards negative Y.
     */
    @SerialName("south")
    SOUTH,

    /**
     * West: towards negative X.
     */
    @SerialName("west")
    WEST;

    public operator fun times(float: Float): Vector2<Float> {
        return when(this) {
            NORTH -> Vector2(0f, float)
            EAST -> Vector2(-float, 0f)
            SOUTH -> Vector2(0f, -float)
            WEST -> Vector2(float, 0f)
        }
    }
}

/**
 * It's like a [Direction], but for sides.
 */
public typealias Side = Direction
