/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.error.ChunkRenderException
import net.derfruhling.kemono.graphics.OpenGLException
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.map.entities.PlayerEntity
import net.derfruhling.kemono.math.*
import net.derfruhling.kemono.physics.PhysicsEngine
import me.derfruhling.quantum.core.dynamo.Dynamo
import me.derfruhling.quantum.core.dynamo.DynamoObject
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger
import kotlin.math.absoluteValue

/**
 * A map of chunks.
 *
 * @property game The game the map is associated with.
 * @property source The source of the map.
 */
public class Map(
    public val mapService: MapService,
    public val source: MapSource,
    explicitPlayerPosition: Vector2<Float>?,
    explicitPlayerDirection: Direction?
) : Disposable, Dynamo {
    /**
     * Allows this [Map] to contain properties and behaviors that are not
     * its own.
     */
    override val dyn: DynamoObject = DynamoObject()

    @Deprecated("Replace game with mapService", ReplaceWith(
        "Map(game.map, source)"
    ))
    public constructor(
        game: Game,
        source: MapSource,
        explicitPlayerPosition: Vector2<Float>?,
        explicitPlayerDirection: Direction?
    ) : this(game.map, source, explicitPlayerPosition, explicitPlayerDirection)

    private val log by logger

    // must be initialized before entities
    public val physicsEngine: PhysicsEngine = PhysicsEngine()

    private val loadSemaphore = Semaphore(1)
    private val storeSemaphore = Semaphore(1)
    private val loadedChunks = mutableMapOf<Vector2<Int>, MapChunk>()
    private val loadedEntities = mutableMapOf<Int, Entity>(
        Int.MAX_VALUE to mapService.createPlayerEntity(
            this,
            explicitPlayerPosition,
            explicitPlayerDirection
        ).also { it.initialize(this) }
    )

    /**
     * The player entity in this map.
     * All maps must have a player entity, and the player entity is never
     * unloaded.
     */
    public val playerEntity: PlayerEntity = loadedEntities.values.single { it is PlayerEntity } as PlayerEntity

    /**
     * Renders this map.
     */
    @RenderFun
    @Throws(OpenGLException::class, ChunkRenderException::class)
    public fun render() {
        // sort by proximity to player character
        loadedChunks.toList()
            .associateWith {
                val chunkCenterF = ((it.first * 16) + Vector2(8, 8)).toFloatVector()
                val cameraPositionF = playerEntity.position
                val difference = chunkCenterF - cameraPositionF
                difference.magnitude().absoluteValue
            }
            .toList()
            .sortedBy { it.second }.map { it.first }
            .forEach { (position, chunk) ->
                try {
                    chunk.render()
                } catch (e: Exception) {
                    throw ChunkRenderException("error rendering chunk at $position", e, chunk.position)
                }
            }

        loadedEntities.forEach { (_, entity) ->
            entity.render(playerEntity.position)
        }
    }

    /**
     * Updates this maps loaded chunks based on render distance.
     * Called during the pre-update phase of the game.
     */
    public suspend fun updateRenderDistance(): Unit = coroutineScope {
        for (x in -4..4) {
            for (y in -4..4) {
                launch {
                    val chunkPos = playerEntity.currentChunk + Vector2(x, y)
                    val proximity = run {
                        val chunkCenterF = ((chunkPos * 16) + Vector2(8, 8)).toFloatVector()
                        val cameraPositionF = playerEntity.position
                        val difference = chunkCenterF - cameraPositionF
                        difference.magnitude().absoluteValue
                    }

                    val shouldBeLoaded = proximity < 32 || (loadedChunks[chunkPos]?.isExplicitlyLoaded ?: false)
                    when {
                        shouldBeLoaded && !loadedChunks.containsKey(chunkPos) -> loadChunk(chunkPos, explicit = false)
                        !shouldBeLoaded && loadedChunks.containsKey(chunkPos) -> unloadChunk(chunkPos)
                    }
                }
            }
        }
    }

    /**
     * Updates this map.
     */
    public suspend fun update(): Unit = coroutineScope {
        loadedEntities.forEach { (_, entity) ->
            launch { entity.update(this@Map) }
        }
    }

    override fun dispose() {
        loadedChunks.forEach { it.value.dispose() }
        loadedEntities.forEach { (_, entity) -> entity.dispose() }
    }

    /**
     * Loads a chunk.
     *
     * @param position The position of the chunk to load.
     * @param explicit If `true`, this chunk will not be unloaded automatically.
     * It must be unloaded using [unloadChunk].
     *
     * @return The loaded [MapChunk], or the existing [MapChunk] if the chunk
     * is already loaded.
     *
     * @throws okio.IOException [MapSource.loadChunk] might throw this.
     */
    public suspend fun loadChunk(position: Vector2<Int>, explicit: Boolean = true): MapChunk {
        if(position in loadedChunks) {
            return loadedChunks[position]!!
        }

        log.debug("Loading chunk at {}", position)
        val savedChunk = loadSemaphore.withPermit { source.loadChunk(position) }

        val chunk = savedChunk?.let { _ ->
            MapChunk(
                position,
                savedChunk.tiles.map {
                    mapService.tileSet[it.toInt()].tile
                }.toTypedArray(),
                isExplicitlyLoaded = explicit
            )
        } ?: MapChunk(
            position,
            Array(MapChunk.SIZE * MapChunk.SIZE) { Tile.AIR },
            isExplicitlyLoaded = explicit
        )

        storeSemaphore.withPermit {
            physicsEngine.createChunk(chunk)
            loadedChunks[position] = chunk

            savedChunk?.entities?.forEach { entity ->
                if(loadedEntities.containsKey(entity.id)) return@forEach
                loadedEntities[entity.id] = entity.also { it.initialize(this) }
            }
        }

        return chunk
    }

    /**
     * Removes a chunk from this map and does not unload its entities that
     * would otherwise be unloaded by a call to [unloadChunk].
     *
     * Does not throw if the chunk provided is not loaded.
     *
     * @param position The chunk.
     */
    public suspend fun removeChunk(position: Vector2<Int>) {
        storeSemaphore.withPermit {
            loadedChunks.remove(position)?.also {
                physicsEngine.removeChunk(it)
            }
        }?.dispose() ?: return
    }

    /**
     * Unloads a chunk and its entities.
     * Does not throw if the chunk provided is not loaded.
     *
     * @param position The chunk.
     */
    public suspend fun unloadChunk(position: Vector2<Int>) {
        storeSemaphore.withPermit {
            log.debug("Unloading chunk at {}", position)
            val toDelete = loadedEntities.values.filter { it.currentChunk == position }.filter { entity ->
                if (entity.despawnsWhenChunkUnloaded) {
                    log.debug("Despawning entity {}", entity)
                    entity.dispose()
                    true
                } else false
            }

            toDelete.forEach { loadedEntities.remove(it.id) }
            loadedChunks.remove(position)?.also {
                physicsEngine.removeChunk(it)
            }
        }?.dispose()
    }

    /**
     * Gets an already loaded chunk from the map.
     *
     * @see loadChunk
     */
    public operator fun get(position: Vector2<Int>): MapChunk? {
        return loadedChunks[position]
    }

    /**
     * Inserts a new chunk into this map.
     *
     * @throws IllegalArgumentException The chunk is already loaded.
     */
    public suspend fun insertChunk(position: Vector2<Int>, chunk: MapChunk) {
        require(position !in loadedChunks) { "Chunk at $position already present in this map" }

        storeSemaphore.withPermit {
            physicsEngine.createChunk(chunk)
            loadedChunks.put(position, chunk)
        }
    }

    /**
     * Checks if a chunk is already loaded in this map.
     *
     * @see loadChunk
     * @see get
     */
    public operator fun contains(position: Vector2<Int>): Boolean {
        return position in loadedChunks
    }

    public fun insertEntity(entity: Entity) {
        require(entity.id !in loadedEntities) { "Entity ${entity.id} already present in this map" }

        loadedEntities[entity.id] = entity.also { it.initialize(this) }
    }

    public fun removeEntity(entity: Entity) {
        removeEntity(entity.id)
    }

    public fun removeEntity(id: Int) {
        loadedEntities.remove(id)?.dispose()
    }

    public fun getEntity(id: Int): Entity? {
        return loadedEntities[id]
    }

    // overcomplicated iterator structure
    // you're welcome
    public val entities: Iterable<Entity> = object : Iterable<Entity> {
        override fun iterator(): Iterator<Entity> = iterator {
            yieldAll(loadedEntities.values)
        }
    }

    /** [Any.toString] */
    override fun toString(): String {
        return "Map(source=$source, dyn=$dyn, playerEntity=$playerEntity)"
    }

    /** [Any.equals] */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Map) return false

        if (source != other.source) return false
        if (dyn != other.dyn) return false
        if (loadedChunks != other.loadedChunks) return false
        if (loadedEntities != other.loadedEntities) return false
        if (playerEntity != other.playerEntity) return false

        return true
    }

    /** [Any.hashCode] */
    override fun hashCode(): Int {
        var result = source.hashCode()
        result = 31 * result + dyn.hashCode()
        result = 31 * result + loadedChunks.hashCode()
        result = 31 * result + loadedEntities.hashCode()
        result = 31 * result + playerEntity.hashCode()
        return result
    }
}