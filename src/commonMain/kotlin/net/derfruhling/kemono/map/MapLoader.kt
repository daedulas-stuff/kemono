/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.log.logger
import okio.FileSystem
import okio.Path
import kotlin.collections.Map
import net.derfruhling.kemono.map.Map as KmMap

private fun enumerateDirectory(
    map: MutableMap<String, () -> MapSource>,
    fileSystem: FileSystem,
    directory: Path,
    relative: String
) {
    if(fileSystem.exists(directory.resolve("map.dat"))) {
        map[relative.ifEmpty { "/" }] = {
            DirectoryMapSource(fileSystem, directory)
        }

        return
    }

    for (path in fileSystem.list(directory)) {
        if(fileSystem.metadata(path).isDirectory) {
            enumerateDirectory(map, fileSystem, directory, relative + '/' + path.name)
        }

        if(path.name.endsWith(".kmp")) {
            map[relative + '/' + path.name] = {
                PackagedMapSource(fileSystem, path)
            }
        }
    }
}

public class MapLoader(fileSystem: FileSystem, directory: Path) : Component {
    private val log by logger
    private val mapService: MapService by service()

    private val maps: Map<String, () -> MapSource>

    init {
        maps = if(!fileSystem.exists(directory)) {
            log.warn("Directory {} does not exist in {}", directory, fileSystem)
            emptyMap()
        } else if(!fileSystem.metadata(directory).isDirectory) {
            log.warn("File {} in {} is not a directory", directory, fileSystem)
            emptyMap()
        } else {
            buildMap {
                enumerateDirectory(this, fileSystem, directory, "")
            }
        }
    }

    public fun loadMap(
        name: String,
        explicitPlayerPosition: Vector2<Float>? = null,
        explicitPlayerDirection: Direction? = null
    ): KmMap {
        val source = maps[name]
        require(source != null) { "Map $name does not exist" }

        return KmMap(
            mapService,
            source(),
            explicitPlayerPosition,
            explicitPlayerDirection
        )
    }
}
