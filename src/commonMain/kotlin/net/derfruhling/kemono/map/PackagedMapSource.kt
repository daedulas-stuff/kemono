/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import okio.*
import kotlin.collections.Map

/**
 * A map source that is loaded from a single file.
 *
 * Also, see [PackagedMapSource.Companion.packageMap] for a function to create
 * these packages
 */
public class PackagedMapSource(
    fileSystem: FileSystem,
    path: Path
) : MapSource, Component {
    private val mapService: MapService by service()
    private val file: FileHandle = fileSystem.openReadOnly(path)

    private val chunkIndex: Map<Vector2<Int>, Long> = buildMap {
        file.source(0).buffer().use { source ->
            fun readIndexEntry() {
                val x = source.readInt()
                val y = source.readInt()
                val offset = source.readLong()
                put(Vector2(x, y), offset)
            }

            repeat(source.readInt()) {
                readIndexEntry()
            }
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    @Throws(IOException::class)
    override fun loadChunk(position: Vector2<Int>): SavedMapChunk? {
        if (!chunkIndex.containsKey(position)) return null
        val offset = chunkIndex[position]!!
        val buffer = Buffer()

        file.read(offset, buffer, 4)
        file.read(offset + 4, buffer, buffer.readInt().toLong())

        val palette = List(buffer.readByte().toInt()) {
            mapService.tileSet[buffer.readShort().toInt()].index
        }

        val tiles = buffer.readByteArray(256).toUByteArray()
        val entities = buildList {
            repeat(buffer.readInt()) {
                val entitySize = buffer.readInt()
                val entityData = buffer.readByteArray(entitySize.toLong())
                add(Cbor.decodeFromByteArray<Entity>(entityData))
            }
        }

        return SavedMapChunk(position, palette, tiles, entities)
    }

    public companion object {
        /**
         * Packages a map stored by [DirectoryMapSource], converting it into a
         * file that can be read with [PackagedMapSource].
         *
         * @param fileSystem The source file system.
         * @param rootPath The path to the map directory in [fileSystem].
         * @param targetFileSystem File system to write into.
         * @param targetPath The path to write in the [targetFileSystem].
         */
        @OptIn(ExperimentalSerializationApi::class)
        public fun packageMap(
            fileSystem: FileSystem,
            rootPath: Path,
            targetFileSystem: FileSystem,
            targetPath: Path
        ) {
            val indexBuffer = Buffer()
            val dataBuffer = Buffer()
            val index = mutableMapOf<Vector2<Int>, Long>()

            fileSystem.listRecursively(rootPath.resolve("chunks"), false).forEach { path ->
                if (path.name == "chunk.dat") {
                    val chunk = fileSystem.read(path) { Cbor.decodeFromByteArray<SavedMapChunk>(readByteArray()) }
                    val buffer = Buffer()

                    buffer.writeByte(chunk.palette.size)
                    chunk.palette.forEach { entry -> buffer.writeShort(entry) }

                    buffer.write(chunk.tiles.toByteArray())

                    buffer.writeInt(chunk.entities.size)
                    chunk.entities.forEach { entity ->
                        val bytes = Cbor.encodeToByteArray(entity)
                        buffer.writeInt(bytes.size)
                        buffer.write(bytes)
                    }

                    index[chunk.position] = dataBuffer.size
                    dataBuffer.writeInt(buffer.size.toInt())
                    dataBuffer.write(buffer, buffer.size)
                }
            }

            index.mapValues { it.value + (index.size * 16) }.forEach { entry ->
                indexBuffer.writeInt(entry.key.x)
                indexBuffer.writeInt(entry.key.y)
                indexBuffer.writeLong(entry.value)
            }

            targetFileSystem.write(targetPath, false) {
                writeAll(indexBuffer)
                writeAll(dataBuffer)
            }
        }
    }
}