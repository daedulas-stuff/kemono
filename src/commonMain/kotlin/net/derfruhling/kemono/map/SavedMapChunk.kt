/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.serialization.Serializable
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2

/**
 * A serialized map chunk.
 * Tiles are represented by [palette], which is indexed into by [tiles],
 * which is exactly [MapChunk.SIZE] * [MapChunk.SIZE] bytes long.
 *
 * @property position The position of the chunk in the map.
 * @property palette A palette indexed by [tiles].
 * @property tiles An array exactly [MapChunk.SIZE] * [MapChunk.SIZE] bytes
 * long that indexes values in [palette].
 * @property entities The entities contained in this chunk.
 */
@Serializable
public class SavedMapChunk(
    public val position: Vector2<Int>,
    public val palette: List<Int>,
    public val tiles: UByteArray,
    public val entities: List<Entity> = emptyList()
)
