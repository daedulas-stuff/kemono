/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.Game
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.services.Component
import me.derfruhling.quantum.core.interfaces.Disposable
import kotlin.math.absoluteValue

/**
 * Represents a loaded map chunk.
 *
 * @property position The position of the chunk.
 * @property isExplicitlyLoaded If this is `true`, the chunk will not be
 * unloaded automatically.
 */
public class MapChunk(
    public val position: Vector2<Int>,
    private val tiles: Array<Tile>,
    public val isExplicitlyLoaded: Boolean
) : Disposable, Component, Iterable<Pair<Vector2<Int>, Tile>> {
    public companion object {
        /**
         * The number of blocks from one side of the chunk to the other.
         */
        public const val SIZE: Int = 16
    }

    private var renderChunk: MapRenderChunk? = null

    private fun indexOf(x: Int, y: Int) = ((y.absoluteValue % SIZE) * SIZE) + (x.absoluteValue % SIZE)

    /**
     * Gets a tile inside this chunk.
     */
    public operator fun get(x: Int, y: Int): Tile = tiles[indexOf(x, y)]

    /**
     * Sets a tile inside this chunk.
     */
    public operator fun set(x: Int, y: Int, tile: Tile) {
        tiles[indexOf(x, y)] = tile
    }

    /**
     * Fills this chunk with [tile].
     */
    public fun fill(tile: Tile) {
        tiles.indices.forEach { i -> tiles[i] = tile }
    }

    /**
     * Renders this chunk.
     * If this chunk has never been rendered before, this function also
     * initializes the render chunk.
     */
    public fun render() {
        if(renderChunk == null) {
            renderChunk = MapRenderChunk(this)
        }

        renderChunk!!.render()
    }

    override fun dispose() {
        renderChunk?.dispose()
    }

    override fun iterator(): Iterator<Pair<Vector2<Int>, Tile>> {
        return iterator {
            repeat(SIZE) { x ->
                repeat(SIZE) { y ->
                    yield(Vector2(x, y) to get(x, y))
                }
            }
        }
    }
}
