/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.graphics.NamedPool
import net.derfruhling.kemono.graphics.Pool
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.Spritesheet
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.map.entities.PlayerEntity
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger
import okio.FileSystem
import okio.Path.Companion.toPath

public open class MapService(
    override val serviceLoader: ServiceLoader
) : Service, Disposable {
    private val log by logger
    private val renderService: RenderService by service()

    /**
     * The currently set map.
     * [Game] will render and update this, if it's not `null`.
     */
    public var currentMap: Map? = null
        private set

    public var nextMapAwaiting: Map? = null
        private set

    /**
     * The tile set used by [map]s.
     */
    public lateinit var tileSet: TileSet

    /**
     * The number of tiles from the center to the top of the rendered window.
     * This is used to calculate how much to scale down the graphics.
     */
    public open val tilesToTop: Int get() = 6

    public lateinit var loader: MapLoader
        private set

    protected open fun getTileRendererShaderResource(): NamedPool.Ref<ShaderResource> =
        renderService.shaderPool["kemono:shaders/textured/basic/2d/cam.shdr"]

    protected open fun createMapLoader(): MapLoader =
        MapLoader(FileSystem.SYSTEM, "./maps".toPath())

    public open fun createPlayerEntity(
        map: Map,
        explicitPosition: Vector2<Float>?,
        explicitDirection: Direction?
    ): PlayerEntity = PlayerEntity(
        position = explicitPosition ?: Vector2(0f, 0f),
        direction = explicitDirection ?: Direction.SOUTH
    )

    private lateinit var tileRendererShaderRef: NamedPool.Ref<ShaderResource>

    /**
     * The shader that is used for tile rendering.
     * The `CHUNK_POSITION` uniform source is provided to this shader whenever
     * it's used.
     */
    public val tileRendererShader: ShaderResource
        get() = tileRendererShaderRef.value

    private val _tileSpritesheets: MutableMap<Int, Spritesheet> = mutableMapOf()

    /**
     * A set of spritesheets that contain the textures used for drawing tiles.
     */
    public val tileSpritesheets: kotlin.collections.Map<Int, Spritesheet> = _tileSpritesheets

    override fun initialize() {
        tileRendererShaderRef = getTileRendererShaderResource()
        loader = createMapLoader()
    }

    override fun dispose() {
        tileRendererShaderRef.dispose()
    }

    /**
     * Registers a tile spritesheet for use in map rendering.
     *
     * @param slot The slot number of the spritesheet.
     * See [Tile.textureSlot].
     * @param name The name of the texture resource to load as a spritemap.
     * @param spriteCount This is the number of sprites contained in the
     * texture per dimension of the texture.
     * Ex.
     * `[16, 16]` means that there are 16 sprites over the X axis and 16 sprites
     * over the Y axis, and `[8, 24]` would mean that there are 8 sprites over
     * the X axis and 24 sprites over Y axis.
     */
    public fun registerTileSpritesheet(slot: Int, name: String, spriteCount: Vector2<Int>) {
        _tileSpritesheets[slot] = renderService.createSpritesheet(name, spriteCount)
    }

    /**
     * Sets the current [map][Game.map].
     * If a map already exists in [map][Game.map], it is [Map.dispose]d.
     *
     * @param map The new map.
     */
    public open fun setMap(map: Map) {
        log.info("Changing map to {}", map)
        currentMap?.dispose()
        currentMap = map
    }

    public open fun setMapLater(map: Map) {
        log.info("Changing map to {} later", map)
        nextMapAwaiting?.dispose()
        nextMapAwaiting = map
    }

    public open suspend fun preUpdate(): Unit = coroutineScope {
        launch { currentMap?.updateRenderDistance() }
    }

    public open suspend fun update(): Unit = coroutineScope {
        launch { currentMap?.update() }
    }

    protected open fun readyToApplyNewMap(): Boolean = true

    public open suspend fun postUpdate(): Unit = coroutineScope {
        nextMapAwaiting?.let {
            if(readyToApplyNewMap()) {
                launch {
                    setMap(it)
                    nextMapAwaiting = null
                }
            }
        }
    }

    public open fun render() {
        currentMap?.render()
    }
}