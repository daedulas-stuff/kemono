/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.math.Vector2
import okio.IOException

/**
 * Represents a source that a map's chunks can be loaded from arbitrarily.
 * A [MapSource] may also be [Iterable]`<SavedMapChunk>` if the implementation
 * allows such an operation.
 */
public interface MapSource {
    /**
     * Represents a [MapSource] with no chunks.
     */
    public object Null : MapSource, Iterable<SavedMapChunk> {
        override fun loadChunk(position: Vector2<Int>): SavedMapChunk? {
            return null
        }

        override fun iterator(): Iterator<SavedMapChunk> {
            return iterator {}
        }
    }

    /**
     * Loads a single chunk from this source.
     *
     * @param position The chunk's position.
     * @return The saved chunk, or `null` if the chunk is not present in the
     * source.
     *
     * @throws IOException The underlying implementation may throw an
     * exception if an IO operation fails in a way that does not indicate the
     * absence of a chunk, but rather something being weird and wrong.
     */
    @Throws(IOException::class)
    public fun loadChunk(position: Vector2<Int>): SavedMapChunk?
}
