/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import net.derfruhling.kemono.math.Vector2
import okio.FileSystem
import okio.IOException
import okio.Path

/**
 * Represents a map stored in the directory-based structure.
 * This is really only useful in an editor and during testing.
 * Staged release games should use [PackagedMapSource].
 *
 * @property fileSystem The filesystem where the directory is stored.
 * @property rootPath The path to the initial directory where the map is stored.
 */
public class DirectoryMapSource(
    public val fileSystem: FileSystem,
    public val rootPath: Path
) : MutableMapSource {
    init {
        fileSystem.createDirectories(rootPath)
        fileSystem.write(rootPath.resolve("map.dat")) {}
    }

    private fun resolveChunkRootPath(position: Vector2<Int>) = rootPath
        .resolve("chunks")
        .resolve(position.x.toString(36))
        .resolve(position.y.toString(36))

    @OptIn(ExperimentalSerializationApi::class)
    @Throws(IOException::class)
    override fun loadChunk(position: Vector2<Int>): SavedMapChunk? {
        val chunkRootPath = resolveChunkRootPath(position)
        if (!fileSystem.exists(chunkRootPath)) return null

        return fileSystem.read(chunkRootPath.resolve("chunk.dat")) {
            val bytes = readByteArray()
            Cbor.decodeFromByteArray<SavedMapChunk>(bytes)
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    override fun saveChunk(position: Vector2<Int>, savedMapChunk: SavedMapChunk) {
        val chunkRootPath = resolveChunkRootPath(position)
        if (!fileSystem.exists(chunkRootPath)) fileSystem.createDirectories(chunkRootPath, true)

        fileSystem.write(chunkRootPath.resolve("chunk.dat"), false) {
            val bytes = Cbor.encodeToByteArray(savedMapChunk)
            write(bytes)
        }
    }

    override fun deleteChunk(position: Vector2<Int>) {
        val chunkRootPath = resolveChunkRootPath(position)
        fileSystem.deleteRecursively(chunkRootPath, false)
    }
}