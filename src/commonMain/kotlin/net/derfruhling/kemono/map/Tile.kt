/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.math.Vector2

/**
 * A tile that can be placed in a chunk.
 *
 * @property id A string identifier of this tile.
 * @property textureSlot The spritemap texture slot to use for this tile's
 * rendering.
 * @property texturePosition The position in the spritemap that contains this
 * tile's texture.
 * @property canBeCollidedWith `true` if this tile can be collided with and
 * interacts with physics.
 * TODO There are no physics.
 */
public open class Tile(
    public val id: String,
    public val textureSlot: Int,
    public val texturePosition: Vector2<Int>,
    public val canBeCollidedWith: Boolean = true
) {
    public companion object {
        /**
         * A texture slot that can be used to indicate that the tile is
         * invisible and should not be rendered.
         * Useful for the [AIR] tile.
         */
        public const val INVISIBLE: Int = Int.MAX_VALUE

        /**
         * The air tile. Always present in tilesets as tile [Int.MAX_VALUE].
         */
        public val AIR: Tile = Tile("air", canBeCollidedWith = false)

        /**
         * Creates an invisible tile with [id].
         *
         * @return A brand new [Tile]
         */
        public fun invisible(id: String): Tile = Tile(id)
    }

    private constructor(
        id: String,
        canBeCollidedWith: Boolean = true
    ) : this(id, INVISIBLE, Vector2(0, 0), canBeCollidedWith)

    /**
     * Represents a collection of tiles that are grouped by their
     * kind and can be oriented with member properties in this class.
     * This class is not a [Tile] in itself.
     */
    @Suppress("KDocMissingDocumentation")
    public open class Oriented(
        id: String,
        textureSlot: Int,
        offsetX: Int,
        offsetY: Int,
        canBeCollidedWith: Boolean = true,
        public val open: Tile =
            Tile(id, textureSlot, Vector2(offsetX + 1, offsetY + 1), canBeCollidedWith),
        public val borderTopLeft: Tile =
            Tile("${id}_b_tl", textureSlot, Vector2(offsetX + 0, offsetY + 0), canBeCollidedWith),
        public val borderTop: Tile =
            Tile("${id}_b_t", textureSlot, Vector2(offsetX + 1, offsetY + 0), canBeCollidedWith),
        public val borderTopRight: Tile =
            Tile("${id}_b_tr", textureSlot, Vector2(offsetX + 2, offsetY + 0), canBeCollidedWith),
        public val borderRight: Tile =
            Tile("${id}_b_r", textureSlot, Vector2(offsetX + 2, offsetY + 1), canBeCollidedWith),
        public val borderBottomRight: Tile =
            Tile("${id}_b_br", textureSlot, Vector2(offsetX + 2, offsetY + 2), canBeCollidedWith),
        public val borderBottom: Tile =
            Tile("${id}_b_b", textureSlot, Vector2(offsetX + 1, offsetY + 2), canBeCollidedWith),
        public val borderBottomLeft: Tile =
            Tile("${id}_b_bl", textureSlot, Vector2(offsetX + 0, offsetY + 2), canBeCollidedWith),
        public val borderLeft: Tile =
            Tile("${id}_b_l", textureSlot, Vector2(offsetX + 0, offsetY + 1), canBeCollidedWith),
        public val borderTopBottom: Tile =
            Tile("${id}_b_tb", textureSlot, Vector2(offsetX + 3, offsetY + 0), canBeCollidedWith),
        public val borderLeftRight: Tile =
            Tile("${id}_b_lr", textureSlot, Vector2(offsetX + 4, offsetY + 0), canBeCollidedWith),
        public val borderCornerBottomRight: Tile =
            Tile("${id}_bc_br", textureSlot, Vector2(offsetX + 3, offsetY + 1), canBeCollidedWith),
        public val borderCornerBottomLeft: Tile =
            Tile("${id}_bc_bl", textureSlot, Vector2(offsetX + 4, offsetY + 1), canBeCollidedWith),
        public val borderCornerTopRight: Tile =
            Tile("${id}_bc_tr", textureSlot, Vector2(offsetX + 3, offsetY + 2), canBeCollidedWith),
        public val borderCornerTopLeft: Tile =
            Tile("${id}_bc_tl", textureSlot, Vector2(offsetX + 4, offsetY + 2), canBeCollidedWith),
        public val borderTopLeftBottom: Tile =
            Tile("${id}_b_tlb", textureSlot, Vector2(offsetX + 5, offsetY + 1), canBeCollidedWith),
        public val borderTopRightBottom: Tile =
            Tile("${id}_b_tlb", textureSlot, Vector2(offsetX + 6, offsetY + 1), canBeCollidedWith),
        public val borderLeftBottomRight: Tile =
            Tile("${id}_b_tlb", textureSlot, Vector2(offsetX + 5, offsetY + 2), canBeCollidedWith),
        public val borderLeftTopRight: Tile =
            Tile("${id}_b_tlb", textureSlot, Vector2(offsetX + 6, offsetY + 2), canBeCollidedWith),
    ) : Iterable<Tile> {
        override fun iterator(): Iterator<Tile> {
            return listOf(
                open,
                borderTopLeft,
                borderTop,
                borderTopRight,
                borderRight,
                borderBottomRight,
                borderBottom,
                borderBottomLeft,
                borderLeft,
                borderTopBottom,
                borderLeftRight,
                borderCornerBottomRight,
                borderCornerBottomLeft,
                borderCornerTopRight,
                borderCornerTopLeft,
                borderTopLeftBottom,
                borderTopRightBottom,
                borderLeftBottomRight,
                borderLeftTopRight
            ).iterator()
        }
    }
}
