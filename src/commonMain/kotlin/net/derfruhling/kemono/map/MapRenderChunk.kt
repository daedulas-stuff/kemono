/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.AsyncService
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.RenderObject
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.mesh.*
import net.derfruhling.kemono.resources.husks.ShaderResource
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import net.derfruhling.kemono.uniforms
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * Helps render a chunk.
 */
public class MapRenderChunk(chunk: MapChunk) : Disposable, Component {
    private val game: Game by service()
    private val renderService: RenderService by service()
    private val mapService: MapService by service()
    private val asyncService: AsyncService by service()

    private data class Stage(
        val spritesheetSlot: Int,
        val mesh: Mesh,
        val renderObject: RenderObject
    )

    private val position = chunk.position

    private val stages = run {
        val map = mutableMapOf<Int, MeshBuilder>()

        for(x in 0..<MapChunk.SIZE) {
            for(y in 0..<MapChunk.SIZE) {
                val tile = chunk[x, y]

                if(tile.textureSlot == Tile.INVISIBLE) {
                    continue
                }

                val builder = if(map[tile.textureSlot] == null) {
                    @OptIn(InternalAPI::class)
                    MeshBuilder(mapService.tileRendererShader).also {
                        map[tile.textureSlot] = it
                    }
                } else map[tile.textureSlot]!!

                val corners = (
                    mapService.tileSpritesheets[tile.textureSlot]
                        ?: throw IllegalStateException("No tile spritesheet ${tile.textureSlot}")
                )[tile.texturePosition]

                builder.addQuad(Quad(
      /* a  */      Vertex(Vector3(x.toFloat(), y.toFloat(), 0f), textureCoords = corners.bottomLeft),
      /* ab */      Vertex(Vector3(x.toFloat(), y.toFloat() + 1f, 0f), textureCoords = corners.topLeft),
      /* ab */      Vertex(Vector3(x.toFloat() + 1f, y.toFloat(), 0f), textureCoords = corners.bottomRight),
      /*  b */      Vertex(Vector3(x.toFloat() + 1f, y.toFloat() + 1f, 0f), textureCoords = corners.topRight),
                ))
            }
        }

        map.map { (slot, builder) ->
            val mesh = builder.build(renderService)
            Stage(
                slot,
                mesh,
                renderService.graphics.createRenderObject {
                    shader = mapService.tileRendererShader
                    bindMesh(mesh)

                    mapService.tileRendererShader.uniforms(ShaderResource.UniformAcceptValue.CHUNK_POSITION)
                        .forEach { uniform -> uniform from position }

                    uniforms(game)
                }
            )
        }
    }

    /**
     * Renders this render chunk.
     */
    public fun render() {
        stages.take(1).forEach { stage ->
            mapService.tileSpritesheets[stage.spritesheetSlot]?.ensureBound()
                ?: throw IllegalStateException("Unknown tile spritesheet ${stage.spritesheetSlot}")
            renderService.graphics.draw(stage.renderObject)
        }
    }

    override fun dispose() {
        asyncService.runOnRenderThread {
            stages.forEach { stage ->
                stage.renderObject.dispose()
                stage.mesh.dispose()
            }
        }
    }
}