/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import net.derfruhling.kemono.graphics.RenderConfigContext
import net.derfruhling.kemono.graphics.onUniformToken
import net.derfruhling.kemono.graphics.structures.UniformValue

/**
 * Sets [EntityUniformToken] values for this draw operation.
 *
 * @receiver [Graphics.draw]
 * @param entity The entity to write values from.
 */
public inline fun RenderConfigContext.withEntityToken(entity: Entity) {
    onUniformToken<EntityUniformToken> { _, token ->
        when(token) {
            EntityUniformToken.ENTITY_POSITION ->
                UniformValue.Vector2F(entity.position)
            EntityUniformToken.ENTITY_DIRECTION ->
                UniformValue.Int(entity.direction.ordinal)
        }
    }
}