/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import me.derfruhling.quantum.core.dynamo.Dynamo
import me.derfruhling.quantum.core.dynamo.DynamoObject
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.*
import net.derfruhling.kemono.map.Direction
import net.derfruhling.kemono.map.Map
import net.derfruhling.kemono.map.MapService
import net.derfruhling.kemono.map.Side
import net.derfruhling.kemono.math.*
import net.derfruhling.kemono.physics.BoundingBox
import net.derfruhling.kemono.physics.PhysicsAnchor
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service

/**
 * Represents an object in the world that is capable of complex behavior
 * and arbitrary placement.
 *
 * Entities are serialized inside the map chunk in which they are spawned and
 * persist until the chunk they are currently standing in (not necessarily the
 * same chunk they spawned in) is unloaded, unless [despawnsWhenChunkUnloaded]
 * is set to `true`, in which the entity does not care and will do whatever
 * it fkn wants.
 *
 * Entities do not duplicate based on their assigned [id].
 * Multiple entities with the same [id] will not be present on one particular
 * map at any time.
 * [Int.MAX_VALUE] as an [id] is reserved for the player entity, and will cause
 * an exception if used as an [id] for an arbitrary entity.
 *
 * An acceptable convention for [id]s could be that inanimate entities should
 * take up the negative number space, while animate entities should take the
 * positive space.
 *
 * [PlayerEntity] is a special case.
 * It is always present in every map without being serialized in a chunk, and
 * cannot despawn, no matter what happens.
 */
@Serializable
@Polymorphic
public abstract class Entity : RefKeeper(), Component, Disposable, Dynamo {
    private val log by logger
    private val mapService: MapService by service()

    /**
     * The [id] of the entity.
     * Should be unique across all entities in the same map.
     * That is, unless you plan to do some strange trickery and cause entities
     * to only spawn if one does not already exist.
     */
    public abstract val id: Int

    /**
     * Entities support Quantum Core Dynamo.
     * They can provide custom properties and behavior.
     */
    @Transient
    override val dyn: DynamoObject = DynamoObject()

    /**
     * The position (in game units) in which this entity currently resides.
     */
    @set:Deprecated("Use setPosition()")
    public abstract var position: Vector2<Float>

    /**
     * The current directory of the entity.
     */
    public abstract var direction: Direction

    /**
     * The size of the entity (in game units).
     * While entities are permitted to render outside these bounds, they are
     * used for [onEntityCollide] and [onEntityUse], and must be constant and
     * accurate.
     */
    public abstract val size: Vector2<Float>

    public open val physicalSize: Vector2<Float>
        get() = size

    /**
     * If this value is set to `true`, this entity despawns when the chunk it's
     * standing in is unloaded.
     * Otherwise, it remains.
     * A value of `false` is probably not too useful outside of [PlayerEntity],
     * so it is recommended to keep this as the default, `true`.
     */
    public var despawnsWhenChunkUnloaded: Boolean = true
        protected set

    /**
     * If this is set to `true`, this entity contributes to physics in a similar
     * way to tiles.
     * This should be disabled for entities that can move.
     */
    public var isPhysicallyColliable: Boolean = true
        protected set

    /**
     * The current chunk of the entity.
     */
    public val currentChunk: Vector2<Int>
        get() = (position * (1f / 16f)).roundDownToIntVector()

    /**
     * A list of all currently colliding entities.
     * Updated by the default implementation of [onEntityCollide].
     */
    protected val collidingEntities: MutableList<Entity> = mutableListOf()

    @Transient
    public var physicsAnchor: PhysicsAnchor? = null
        private set

    protected open fun createPhysicsAnchor(map: Map): PhysicsAnchor? =
        map.physicsEngine.anchor(this)

    public open fun initialize(map: Map) {
        physicsAnchor = createPhysicsAnchor(map)
    }

    /**
     * Renders this entity at [position] in looking towards [direction].
     * Note that [render] should not be used to mutate the entity, use [update]
     * for that instead.
     * Since scheduled render functions are run before these functions, it is
     * wise to use [Game.runOnRenderThread] in [update] rather than initializing
     * things on first run here.
     *
     * @param game The game that is requesting this entity to render.
     * @param cameraPosition The current position of the camera.
     * This is not necessarily the same as the player's current position.
     *
     * @throws OpenGLException An OpenGL error occurred in the render function.
     */
    @RenderFun
    public abstract fun render(cameraPosition: Vector2<Float>)

    /**
     * Updates this entity.
     *
     * @param game The game that is requesting this entity to update.
     * @param map The map this entity is contained in.
     */
    public abstract suspend fun update(map: Map)

    /**
     * Executed when an entity moves to collide with this one.
     * This function should not be called if the entity was already colliding
     * with this one when the pre-update started.
     *
     * Overrides should call their super's [onEntityCollide].
     */
    public open suspend fun onEntityCollide(game: Game, other: Entity, side: Side) {
        collidingEntities.add(other)
    }

    /**
     * Executed when an entity moves away and stops colliding with this one.
     *
     * Overrides should call their super's [onEntityStopColliding].
     */
    public open suspend fun onEntityStopColliding(game: Game, other: Entity, side: Side) {
        collidingEntities.remove(other)
    }

    /**
     * Called when a [PlayerEntity] presses the use button while this entity
     * is in front of them.
     */
    public open suspend fun onEntityUse(game: Game, other: PlayerEntity, side: Side) {}

    /**
     * Creates a [BoundingBox] for this entity. This allows it to participate
     * in physics simulation. The default implementation should be sufficient
     * for most uses, but it can be safely overridden for whatever reason you
     * may want to do so.
     *
     * @return [BoundingBox]
     *
     * Don't go too crazy.
     */
    public open fun createBoundingBox(position: Vector2<Float>): BoundingBox {
        val start = position
        val end = start + physicalSize

        return BoundingBox(start.x, end.x, start.y, end.y)
    }

    /**
     * Sets the position and updates the bounding box of the entity.
     *
     * You may want to try [trySetPosition] or [setPositionWithCorrection]
     * instead, as those take into account the physical constraints of space
     * and time.
     */
    public open fun setPosition(value: Vector2<Float>) {
        @Suppress("DEPRECATION")
        position = value
        physicsAnchor?.updateBoundingBox()
    }

    /**
     * Tries to move this entity exactly to [value].
     * If moving to the new position would cause an intersection with the
     * bounding box of something else in the world, throw an exception.
     *
     * @param value The exact position to move to.
     *
     * @throws IllegalArgumentException The new position would cause an
     * intersection with another object.
     *
     * @see setPositionWithCorrection
     */
    public suspend fun trySetPosition(value: Vector2<Float>) {
        if(physicsAnchor == null) {
            setPosition(value)
            return
        }

        val newBoundingBox = createBoundingBox(value)
        val correction = physicsAnchor!!.runPhysicsSimulation(newBoundingBox)

        require(correction != null) { "Position is invalid! Collision correction: $correction. Maybe try setPositionWithCorrection()?" }
        setPosition(value)
    }

    /**
     * Tries to move this entity exactly to [value].
     * If moving to the new position would cause an intersection with the
     * bounding box of something else in the world, apply a correction.
     *
     * @param value The exact position to move to.
     *
     * @return `true` if a correction was applied, `false` otherwise.
     */
    public suspend fun setPositionWithCorrection(value: Vector2<Float>): Boolean {
        if(physicsAnchor == null) {
            setPosition(value)
            return false
        }

        val newBoundingBox = createBoundingBox(value)
        val correction = physicsAnchor!!.runPhysicsSimulation(newBoundingBox)
        val newPosition = value + (correction ?: Vector2(0f, 0f))

        setPosition(newPosition)
        return correction != null
    }

    /**
     * Tries to move this entity [delta] units, taking into account physical
     * boundaries of space and time.
     *
     * @param delta The number of units on each axis to move.
     *
     * @return `true` if any movement occurred, `false` otherwise, such as if
     * there is a wall directly next to the entity preventing movement, or
     * [delta] is full of zeroes.
     */
    public suspend fun tryMove(delta: Vector2<Float>): Boolean {
        if(physicsAnchor == null) {
            setPosition(position + delta)
            return delta.magnitude() > 0f
        }

        val newBoundingBox = createBoundingBox(position) + delta
        val correction = physicsAnchor!!.runPhysicsSimulation(newBoundingBox)
        val newPosition = position + delta + (correction ?: Vector2(0f, 0f))

        return if(position != newPosition) {
            setPosition(newPosition)
            true
        } else false
    }
}
