/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.map.Direction
import net.derfruhling.kemono.map.MapService
import net.derfruhling.kemono.map.Ray
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.physics.Interactable
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service

/**
 * The main player entity.
 * This entity is the base of the camera and houses all logic for the player.
 */
// PlayerEntity is never serialized.
public open class PlayerEntity(
    @set:Deprecated("Use setPosition()")
    override var position: Vector2<Float>,
    override var direction: Direction
) : SpriteEntity(), Component {
    private val mapService: MapService by service()
    private val renderService: RenderService by service()

    override val id: Int
        get() = Int.MAX_VALUE

    override val originOffset: Vector2<Float> = Vector2(0.5f, 0.5f)
    override val size: Vector2<Float> = Vector2(1f, 2f)
    override val physicalSize: Vector2<Float> = Vector2(1f, 1f)

    override val texture: Texture by
        renderService.texturePool["kemono:textures/stick_figure.tex"].ref()

    init {
        despawnsWhenChunkUnloaded = false
    }

    /**
     * Attempts to trigger an interaction in the direction the player is
     * facing, looking at most 0.6 units ahead of the player for an [Entity].
     * This will stop at any entity, not just ones that are [Interactable], so
     * if the nearest hit is not [Interactable] this will not go any further.
     *
     * @return `true` if an interaction was triggered, `false` if no entity was
     * found.
     */
    public open suspend fun triggerInteraction(): Boolean {
        val ray = Ray.cast(
            mapService.currentMap!!,
            position,
            direction,
            this,
            distancePerIteration = 0.20f,
            iterationCount = 5
        )

        return if(ray.isHit && ray.nearestHit is Interactable) {
            ray.nearestHit.onInteract(this, ray)
            true
        } else false
    }

    override fun dispose() {
        texture.dispose()
    }
}