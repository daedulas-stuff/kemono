/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.map.Direction
import net.derfruhling.kemono.map.Map
import net.derfruhling.kemono.map.MapLoader
import net.derfruhling.kemono.math.Vector2

@Polymorphic
@Serializable
public abstract class NamedMapPortalSpriteEntity : PortalSpriteEntity() {
    public abstract val mapName: String
    public abstract val explicitPlayerPosition: Vector2<Float>?
    public abstract val explicitPlayerDirection: Direction?

    override fun loadNextMap(mapLoader: MapLoader): Map {
        return mapLoader.loadMap(
            mapName,
            explicitPlayerPosition,
            explicitPlayerDirection
        )
    }
}