/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.map.Map
import net.derfruhling.kemono.map.MapLoader
import net.derfruhling.kemono.map.MapService
import net.derfruhling.kemono.map.Ray
import net.derfruhling.kemono.physics.Interactable
import net.derfruhling.kemono.services.service

@Polymorphic
@Serializable
public abstract class PortalSpriteEntity : SpriteEntity(), Interactable {
    private val mapService: MapService by service()

    public abstract fun loadNextMap(mapLoader: MapLoader): Map

    override suspend fun onInteract(playerEntity: PlayerEntity, interactingRay: Ray) {
        mapService.setMap(loadNextMap(mapService.loader))
    }
}