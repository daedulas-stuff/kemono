/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map.entities

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import net.derfruhling.kemono.AsyncService
import net.derfruhling.kemono.physics.BoundingBox
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.map.Direction
import net.derfruhling.kemono.map.Map
import net.derfruhling.kemono.math.*
import net.derfruhling.kemono.mesh.*
import net.derfruhling.kemono.services.service
import net.derfruhling.kemono.uniforms

/**
 * A form of [Entity] that renders a sprite.
 */
@Serializable
@Polymorphic
public abstract class SpriteEntity : Entity() {
    @Transient
    private var isInitialized = false

    private val game: Game by service()
    private val renderService: RenderService by service()
    private val asyncService: AsyncService by service()

    /**
     * The offset of the origin point.
     * The texture is offset by `-`[originOffset].
     */
    public abstract val originOffset: Vector2<Float>

    /**
     * The texture that is used for the entity.
     *
     * @see texture2D
     */
    public abstract val texture: Texture

    public open val textureCorners: Corners<Vector2<Float>>
        get() = Corners.unitBox

    /**
     * [texture] cast as [Texture2D]
     */
    public inline val texture2D: Texture2D
        get() = texture as Texture2D

    /**
     * The shader that is used to render the sprite entity.
     */
    public open val shader: ShaderResource by
        renderService.shaderPool["kemono:shaders/textured/basic/2d/entity.shdr"].ref()

    /**
     * A mesh generated when this entity is first rendered.
     */
    @Transient
    public lateinit var mesh: Mesh
        private set

    /**
     * The render object generated when this entity is first rendered.
     */
    @Transient
    public lateinit var renderObject: RenderObject
        private set

    /**
     * Renders this sprite.
     *
     * @param cameraPosition The camera position.
     * @param entityPosition The position of the entity.
     * @param entityDirection The direction the entity is facing.
     */
    protected open fun renderSprite(
        cameraPosition: Vector2<Float>,
        entityPosition: Vector2<Float> = position,
        entityDirection: Direction = direction
    ) {
        texture2D.ensureBound()

        renderService.graphics.draw(renderObject) {
            withEntityToken(this@SpriteEntity)
        }
    }

    /**
     * Builds the mesh of this entity.
     * Safe to override.
     */
    protected open fun MeshBuilder.buildMesh(): MeshBuilder = addQuad(Quad(
        Vertex(
            position = Vector3(-originOffset.x, -originOffset.y, 0f),
            color = Vector4(1f, 1f, 1f, 1f),
            textureCoords = textureCorners.bottomLeft
        ),
        Vertex(
            position = Vector3(-originOffset.x, -originOffset.y + size.y, 0f),
            color = Vector4(1f, 1f, 1f, 1f),
            textureCoords = textureCorners.topLeft
        ),
        Vertex(
            position = Vector3(-originOffset.x + size.x, -originOffset.y, 0f),
            color = Vector4(1f, 1f, 1f, 1f),
            textureCoords = textureCorners.bottomRight
        ),
        Vertex(
            position = Vector3(-originOffset.x + size.x, -originOffset.y + size.y, 0f),
            color = Vector4(1f, 1f, 1f, 1f),
            textureCoords = textureCorners.topRight
        ),
    ))

    /**
     * Configures the render object.
     */
    protected open fun RenderObjectConfigurator.renderObjectConfig() {
        shader = this@SpriteEntity.shader

        uniforms(game)
        bindMesh(mesh)
    }

    /**
     * Creates a [BoundingBox] for this entity. This allows it to participate
     * in physics simulation. The default implementation should be sufficient
     * for most uses, but it can be safely overridden for whatever reason you
     * may want to do so.
     *
     * [SpriteEntity] extends the default behavior to include [originOffset].
     *
     * @return [BoundingBox]
     *
     * Don't go too crazy.
     */
    override fun createBoundingBox(position: Vector2<Float>): BoundingBox {
        return super.createBoundingBox(position) - originOffset;
    }

    override fun render(cameraPosition: Vector2<Float>) {
        renderSprite(cameraPosition)
    }

    override suspend fun update(map: Map) {
        if(!isInitialized) {
            asyncService.runOnRenderThread {
                mesh = Mesh.build(renderService, shader) { buildMesh() }

                renderObject = renderService.graphics.createRenderObject {
                    renderObjectConfig()
                }

                isInitialized = true
            }
        }
    }

    override fun dispose() {
        super.dispose()

        renderObject.dispose()
        mesh.dispose()
    }
}