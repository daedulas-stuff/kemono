/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.map

import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.minus

public data class Ray(
    val nearestHit: Entity?,
    val distance: Float
) {
    public val isHit: Boolean
        get() = nearestHit != null

    public companion object {
        private const val DISTANCE_PER_ITERATION: Float = 0.15f
        private const val ITERATION_COUNT: Int = 32

        public fun cast(
            map: Map,
            position: Vector2<Float>,
            direction: Direction,
            sourceEntity: Entity? = null,
            distancePerIteration: Float = DISTANCE_PER_ITERATION,
            iterationCount: Int = ITERATION_COUNT
        ): Ray {
            var pos = position

            repeat(iterationCount) { i ->
                pos -= direction * distancePerIteration

                val hit = map.entities.firstOrNull { entity ->
                    // ignore source entity, if provided
                    if(entity == sourceEntity) return@firstOrNull false

                    entity.physicsAnchor?.boundingBox
                        ?.let { box -> pos in box } ?: false
                } ?: return@repeat

                return Ray(hit, i * DISTANCE_PER_ITERATION)
            }

            return Ray(null, Float.POSITIVE_INFINITY)
        }
    }
}