/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import net.derfruhling.kemono.math.Angle
import net.derfruhling.kemono.math.angleOfLineTo
import net.derfruhling.kemono.math.distanceTo
import net.derfruhling.kemono.math.xy

/**
 * A line between two vertices.
 *
 * @property a First vertex.
 * @property b Second vertex.
 */
public data class Line(
    val a: Vertex,
    val b: Vertex
) {
    public constructor(pair: Pair<Vertex, Vertex>) : this(pair.first, pair.second)

    /**
     * The angle between the positions of the two lines compared to north.
     */
    public inline val relativeAngle: Angle
        get() = a.position!!.xy angleOfLineTo b.position!!.xy

    /**
     * The distance between the two vertices.
     */
    public inline val distance: Float
        get() = a.position!!.xy distanceTo b.position!!.xy
}
