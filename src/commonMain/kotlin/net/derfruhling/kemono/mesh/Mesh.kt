/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.Buffer
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * Represents a constructed mesh that is ready to be rendered.
 *
 * @property vertexBuffer The buffer of vertices.
 * @property vertexCount The number of vertices in the buffer. Unused!
 * @property indexBuffer The buffer of indices into [vertexBuffer].
 * @property indexCount The number of indices in [indexBuffer].
 */
public class Mesh(
    public val renderService: RenderService,
    public val shader: ShaderResource,
    public val vertexBuffer: Buffer,
    vertexCount: Int,
    public val indexBuffer: Buffer,
    indexCount: Int
) : Disposable {
    public var vertexCount: Int = vertexCount
        private set
    public var indexCount: Int = indexCount
        private set

    public companion object {
        /**
         * Builds a mesh.
         *
         * @param game The game to build for.
         * @param shader The shader to build for.
         * @param fn The builder.
         *
         * @return A brand new [Mesh]
         */
        @Deprecated("Replace with render-service-accepting build()", ReplaceWith(
            "build(game.render, shader, fn)",
            "net.derfruhling.kemono.mesh.Mesh.Companion.build"
        ))
        public inline fun build(game: Game, shader: ShaderResource, fn: MeshBuilder.() -> Unit): Mesh {
            return build(game.render, shader, fn)
        }

        /**
         * Builds a mesh.
         *
         * @param graphics The graphics instance to build for.
         * @param shader The shader to build for.
         * @param fn The builder.
         *
         * @return A brand new [Mesh]
         */
        public inline fun build(renderService: RenderService, shader: ShaderResource, fn: MeshBuilder.() -> Unit): Mesh {
            val builder = @OptIn(InternalAPI::class) MeshBuilder(shader)
            builder.fn()
            return builder.build(renderService)
        }
    }

    public inner class MeshEditor : MeshBuilder(shader) {
        override fun build(renderService: RenderService): Mesh {
            this@Mesh.vertexBuffer.write(vertexBuffer)
            this@Mesh.vertexCount = vertexCount
            this@Mesh.indexBuffer.write(indexBuffer)
            this@Mesh.indexCount = indexCount

            return this@Mesh
        }
    }

    public inline fun rebuild(fn: MeshBuilder.() -> Unit) {
        MeshEditor().apply(fn).build(renderService)
    }

    override fun dispose() {
        renderService.bufferPool.enpool(vertexBuffer)
        renderService.bufferPool.enpool(indexBuffer)
    }
}
