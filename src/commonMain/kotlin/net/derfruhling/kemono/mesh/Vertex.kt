/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4

/**
 * A singular vertex.
 * This class specifies all standard parts of the vertex as used in the engine
 * itself.
 *
 * @property position The position of the vertex, in game units (hereby KmU).
 * Optional, but recommended.
 * @property color The color of the vertex. Optional.
 * @property textureCoords The texture coords into the currently bound texture
 * that this vertex represents.
 * Optional.
 */
public data class Vertex(
    val position: Vector3<Float>? = null,
    val color: Vector4<Float>? = null,
    val textureCoords: Vector2<Float>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vertex) return false

        if (position != other.position) return false
        if (color != other.color) return false
        if (textureCoords != other.textureCoords) return false

        return true
    }

    override fun hashCode(): Int {
        var result = position?.hashCode() ?: 0
        result = 31 * result + (color?.hashCode() ?: 0)
        result = 31 * result + (textureCoords?.hashCode() ?: 0)
        return result
    }
}
