/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import me.derfruhling.quantum.core.dsl.PairOf

/**
 * A collection of four vertices that form a Quad.
 *
 * @property a [Vertex]
 * @property b [Vertex]
 * @property c [Vertex]
 * @property d [Vertex]
 */
public data class Quad(
    val a: Vertex,
    val b: Vertex,
    val c: Vertex,
    val d: Vertex
) {
    /**
     * Sorts the vertices of this Quad clockwise.
     */
    public fun sortedClockwise(): Quad {
        val lines = listOf(
            Line(a, b),
            Line(b, c),
            Line(c, d),
            Line(d, a)
        ).sortedBy { it.relativeAngle }

        return Quad(
            lines[0].a,
            lines[1].a,
            lines[2].a,
            lines[3].a,
        )
    }

    /**
     * Sorts the vertices of this Quad counterclockwise.
     */
    @Deprecated(
        "intoTriangles() expects clockwise points",
        replaceWith = ReplaceWith("sortedClockwise()")
    )
    public fun sortedCounterClockwise(): Quad {
        val lines = listOf(
            Line(a, b),
            Line(b, c),
            Line(c, d),
            Line(d, a)
        ).sortedByDescending { it.relativeAngle }

        return Quad(
            lines[0].a,
            lines[1].a,
            lines[2].a,
            lines[3].a,
        )
    }

    /**
     * Converts this [Quad] into a [PairOf] two [Triangle]s.
     */
    public fun intoTriangles(): PairOf<Triangle> {
        return Triangle(a, b, c) to Triangle(b, c, d)
    }

    /**
     * Convenience function.
     * Calls [sortedClockwise], then calls [intoTriangles] on the result.
     */
    public inline fun intoTrianglesSorted(): PairOf<Triangle> {
        return sortedClockwise().intoTriangles()
    }

    /**
     * Convenience function.
     * Calls [sortedCounterClockwise], then calls [intoTriangles] on the result.
     */
    @Deprecated(
        "intoTriangles() expects clockwise points",
        ReplaceWith("intoTrianglesSorted()")
    )
    public inline fun intoTrianglesSortedCounterClockwise(): PairOf<Triangle> {
        @Suppress("DEPRECATION") // intentionally wrong
        return sortedCounterClockwise().intoTriangles()
    }
}