/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import net.derfruhling.kemono.KemonoEngine
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.resources.husks.ShaderResource.InputValueSource.*
import me.derfruhling.quantum.core.log.logger
import okio.Buffer
import okio.Buffer as OkioBuffer

/**
 * Builds a [Mesh] instance.
 *
 * @property shader The shader to build for.
 */
public open class MeshBuilder protected constructor(public val shader: ShaderResource) {
    private val log by logger

    private val vertexParts = shader.inputOrder!!
        .map { shader.boundInputs[it] }
        .flatMap { it!!.asIterable() }

    /**
     * The number of vertices that were added to this builder.
     */
    public var vertexCount: Int = 0
        private set

    /**
     * The number of indices that were added to this builder.
     */
    public var indexCount: Int = 0
        private set

    /**
     * A byte buffer of vertices.
     */
    public val vertexBuffer: Buffer = OkioBuffer()

    /**
     * A byte buffer of indices.
     */
    public val indexBuffer: Buffer = OkioBuffer()

    private val vertices = mutableMapOf<Vertex, Int>()

    /**
     * Adds an individual vertex.
     * Maybe.
     *
     * If the vertex is already added into this builder, returns the index
     * of the original vertex in the buffer.
     *
     * @param vertex The vertex to add.
     */
    protected open fun addVertex(vertex: Vertex): Int {
        if (!vertices.containsKey(vertex)) {
            val index = vertexCount

            vertexParts.forEach { part ->
                vertexBuffer.writeIntLe(
                    when (part) {
                        ZERO -> 0f
                        ONE -> 1f
                        POSITION_X -> vertex.position!!.x
                        POSITION_Y -> vertex.position!!.y
                        POSITION_Z -> vertex.position!!.z
                        COLOR_R -> vertex.color!!.x
                        COLOR_G -> vertex.color!!.y
                        COLOR_B -> vertex.color!!.z
                        COLOR_A -> vertex.color!!.w
                        TEX_COORD_X -> vertex.textureCoords!!.x
                        TEX_COORD_Y -> vertex.textureCoords!!.y
                    }.toRawBits()
                )
            }
            vertices[vertex] = vertexCount++

            return index
        } else {
            return vertices[vertex]!!
        }
    }

    /**
     * Adds an individual index value.
     *
     * @param idx The index value.
     */
    protected open fun addIndex(idx: UShort) {
        indexBuffer.writeShortLe(idx.toInt())
        indexCount++
    }

    /**
     * Adds a bunch of vertices.
     *
     * @param indices The indices to add.
     */
    protected open fun addIndices(vararg indices: UShort) {
        indices.forEach {
            indexBuffer.writeShortLe(it.toInt())
        }

        indexCount += indices.size
    }

    /**
     * Adds a complete triangle to this builder.
     *
     * @param triangle The triangle to add. Unsorted.
     *
     * @return `this`
     */
    public open fun addTriangle(triangle: Triangle): MeshBuilder = apply {
        addIndices(
            addVertex(triangle.a).toUShort(),
            addVertex(triangle.b).toUShort(),
            addVertex(triangle.c).toUShort()
        )
    }

    /**
     * Builds this builder into a new [Mesh].
     *
     * @param graphics The graphics instance to build with.
     *
     * @return A brand new [Mesh]
     */
    public open fun build(renderService: RenderService): Mesh {
        log.debug("Building mesh [#vertices {}] [#indices {}]", vertexCount, indexCount)
        val vertexBufferReal = renderService.allocateVertexBuffer()
        val indexBufferReal = renderService.allocateIndexBuffer()

        vertexBufferReal.write(vertexBuffer)
        indexBufferReal.write(indexBuffer)

        return Mesh(
            renderService,
            shader,
            vertexBufferReal,
            vertexCount,
            indexBufferReal,
            indexCount
        )
    }

    public companion object {
        private class DebugMeshBuilder(shader: ShaderResource) : MeshBuilder(shader) {
            private val log by logger

            override fun addVertex(vertex: Vertex): Int {
                return super.addVertex(vertex).also { index ->
                    log.debug("DEBUG: Put {} at index {} in mesh builder {}", vertex, index, this)
                }
            }

            override fun addIndex(idx: UShort) {
                log.debug("DEBUG: Adding index {} to mesh builder {}", idx, this)
                super.addIndex(idx)
            }

            override fun addIndices(vararg indices: UShort) {
                log.debug("DEBUG: Adding indices [{}] to mesh builder {}", indices.joinToString(), this)
                super.addIndices(*indices)
            }

            override fun addTriangle(triangle: Triangle): MeshBuilder {
                log.debug("DEBUG: Adding {} to mesh builder {}", triangle, this)
                return super.addTriangle(triangle)
            }
        }

        /**
         * **Internal:** Use [Mesh.build] instead.
         */
        @PublishedApi
        @InternalAPI
        internal operator fun invoke(shader: ShaderResource): MeshBuilder {
            return if (KemonoEngine.debugMeshBuilder) DebugMeshBuilder(shader)
            else MeshBuilder(shader)
        }
    }
}
