/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.mesh

import net.derfruhling.kemono.graphics.interfaces.RenderObjectConfigurator
import me.derfruhling.quantum.core.log.Logger

/**
 * Convenience function to bind all the parts of a [Mesh] into a
 * [RenderObjectConfigurator].
 *
 * @receiver The configurator.
 * @param mesh The mesh to bind.
 */
public fun RenderObjectConfigurator.bindMesh(mesh: Mesh) {
    vertexBuffer = mesh.vertexBuffer
    vertexCount = mesh.vertexCount
    indexBuffer = mesh.indexBuffer
    indexCount = mesh.indexCount
    Logger.of<RenderObjectConfigurator>().debug("Binding mesh {}", mesh)
}
