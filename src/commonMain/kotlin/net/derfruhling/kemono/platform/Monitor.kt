/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import net.derfruhling.kemono.math.Vector2

/**
 * Represents a GLFW monitor, which can be used to make a window fullscreen.
 */
public expect class Monitor {
    /**
     * The current video mode the monitor is using.
     */
    public val currentVideoMode: VideoMode

    /**
     * Every video mode the monitor supports.
     */
    public val allVideoModes: List<VideoMode>

    /**
     * The physical size of the monitor, in millimeters.
     */
    public val physicalSize: Vector2<Int>?

    /**
     * The virtual position of the monitor in the arrangement of monitors.
     */
    public val virtualPosition: Vector2<Int>

    /**
     * The display name of the monitor.
     */
    public val name: String

    public companion object {
        /**
         * The primary monitor of the system.
         */
        public val primary: Monitor

        /**
         * All monitors of the system.
         */
        public val all: List<Monitor>
    }
}
