/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader

public open class PlatformService(
    override val serviceLoader: ServiceLoader
) : Service {
    protected open fun createPrimaryWindow(): Window =
        Window.createWindowed(800, 600, "kemono")

    /**
     * The primary [Window] that is rendered to.
     * Other windows may co-exist with this one.
     */
    public lateinit var primaryWindow: Window
        private set

    override fun initialize() {
        primaryWindow = createPrimaryWindow().apply { makeActive() }
    }
}
