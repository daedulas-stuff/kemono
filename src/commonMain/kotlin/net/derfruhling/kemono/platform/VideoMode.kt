/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

/**
 * Represents a GLFW video mode contained within a [Monitor].
 *
 * @property width The width, in pixels, of the video mode.
 * @property height The height, in pixels, of the video mode.
 * @property refreshRate The refresh rate, in frames per second, of the video
 * mode.
 * @property redBits The number of red bits the video mode supports.
 * @property greenBits The number of green bits the video mode supports.
 * @property blueBits The number of blue bits the video mode supports.
 */
public data class VideoMode(
    val width: Int,
    val height: Int,
    val refreshRate: Int,

    val redBits: Int,
    val greenBits: Int,
    val blueBits: Int
)