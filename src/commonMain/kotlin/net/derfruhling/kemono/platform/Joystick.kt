/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import net.derfruhling.kemono.input.InputTrigger
import me.derfruhling.quantum.core.dsl.PairOf

/**
 * Represents a GLFW joystick.
 */
public expect class Joystick {
    /**
     * The id of the joystick.
     */
    public val jid: Int

    /**
     * The GUID of the joystick.
     * Can be used to identify a specific joystick.
     */
    public val guid: PairOf<Long> // TODO

    /**
     * The user-facing name of the joystick.
     */
    public val name: String

    /**
     * Gets the button values of the joystick.
     *
     * @return An array of buttons.
     *
     * @see InputTrigger.GamepadButton
     */
    public fun getButtons(): BooleanArray

    /**
     * Gets the axis values of the joystick.
     *
     * @return An array of buttons.
     *
     * @see InputTrigger.GamepadAxis
     */
    public fun getAxes(): FloatArray
}
