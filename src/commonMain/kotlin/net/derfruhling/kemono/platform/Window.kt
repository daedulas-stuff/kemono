/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.error.GLFWError
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.math.Vector2
import me.derfruhling.quantum.core.dsl.PairOf
import me.derfruhling.quantum.core.interfaces.Closeable

/**
 * Represents a GLFW window.
 */
public expect class Window : Closeable {
    /**
     * The title of the window.
     */
    @set:RenderFun
    public var title: String

    /**
     * The size, in pixels, of the window.
     *
     * @see framebufferSize
     * @see sizeFlow
     */
    @set:RenderFun
    public var size: Vector2<Int>

    /**
     * The size of the window's framebuffer, in pixels.
     *
     * @see framebufferSizeFlow
     */
    public val framebufferSize: Vector2<Int>

    /**
     * The position of the window on the screen, in pixels.
     *
     * @see positionFlow
     */
    @set:RenderFun
    public var position: Vector2<Int>

    /**
     * Whether this window wants to close.
     * If this is `true`, the user probably clicked the X to close the window,
     * and it should be closed / hidden asap.
     */
    @set:RenderFun
    public var shouldClose: Boolean

    /**
     * Whether the window is currently visible.
     * Set this to change the window's visibility.
     * This is not the same as iconifying the window, which does not make it
     * invisible according to this property.
     */
    @set:RenderFun
    public var isVisible: Boolean

    /**
     * Whether this window will wait for the next v-blank of the monitor before
     * returning from [swapBuffers].
     *
     * This is `true` by default.
     * Use [Game.lastFrameTimeAvg]!
     */
    @set:RenderFun
    public var isVsyncEnabled: Boolean

    /**
     * Whether the window is currently focused.
     */
    public val isActive: Boolean

    /**
     * Checks whether mouse delta input hooks are even supported at all.
     * Some systems may return `false`, so ensure that this is `true` before
     * committing to using mouse delta.
     *
     * It may be worth throwing an error if this value must be `true`.
     */
    public val isMouseDeltaSupported: Boolean

    /**
     * If this is `true`, mouse delta mode is enabled and input events will be
     * output for mouse delta, replacing mouse position.
     *
     * For this to be meaningful, [isMouseDeltaSupported] must also be `true`.
     */
    public var isMouseDeltaEnabled: Boolean

    /**
     * A list of all available joysticks, indexed by their GUID.
     * You can save a joystick GUID to remember the user's preference of
     * joystick, as JIDs are not static.
     */
    public val availableJoysticks: Map<PairOf<Long>, Joystick>

    /**
     * The currently selected joystick.
     * Setting this to anything has consequences and will emit events.
     */
    public var selectedJoystick: Joystick?

    /**
     * A flow that is emitted to whenever the window's size changes.
     *
     * @see size
     */
    public val sizeFlow: StateFlow<Vector2<Int>>

    /**
     * A flow that is emitted to whenever the window's framebuffer size changes.
     * This is always fired alongside [sizeFlow].
     *
     * @see framebufferSize
     */
    public val framebufferSizeFlow: StateFlow<Vector2<Int>>

    /**
     * A flow that is emitted to whenever the window's position changes.
     *
     * @see position
     */
    public val positionFlow: StateFlow<Vector2<Int>>

    /**
     * A shared flow that is emitted to whenever a key's state changes and
     * [pollEvents] is called.
     * The [Boolean] is the new state of the key.
     * `true` for "pressed", `false` for "released"
     */
    public val keyFlow: SharedFlow<Pair<InputTrigger.Key, Boolean>>

    /**
     * A shared flow that is emitted to whenever a mouse button's state changes
     * and [pollEvents] is called.
     * The [Boolean] is the new state of the mouse button.
     * `true` for "pressed", `false` for "released"
     */
    public val mouseButtonFlow: SharedFlow<Pair<InputTrigger.MouseButton, Boolean>>

    /**
     * A shared flow that is emitted to whenever the mouse cursor's position
     * changes and [pollEvents] is called.
     * The position is in pixels relative to the top-left corner of the window.
     *
     * There are no values emitted to this flow if [isMouseDeltaEnabled] is
     * `true` and [isMouseDeltaSupported] is also `true`.
     * If [isMouseDeltaEnabled] is `true` but [isMouseDeltaSupported] is
     * `false`, this flow will emit values as a fallback as the values are
     * readily available.
     */
    public val mousePositionFlow: SharedFlow<Pair<InputTrigger.MousePosition, Float>>

    /**
     * A shared flow that is emitted to whenever the mouse is moved while
     * [isMouseDeltaEnabled] and [pollEvents] is called.
     * The delta is in pixels.
     *
     * There are no values emitted to this flow by default.
     * Only if [isMouseDeltaEnabled] is `true` and [isMouseDeltaSupported] is
     * `true` will values be emitted to this flow.
     * Otherwise, [mousePositionFlow] receives events instead.
     */
    public val mouseDeltaFlow: SharedFlow<Pair<InputTrigger.MouseDelta, Float>>

    internal val gamepadButtonsFlow: StateFlow<BooleanArray>
    internal val gamepadAxesFlow: StateFlow<FloatArray>

    /**
     * Processes gamepad state.
     */
    @RenderFun
    public fun processGamepad()

    /**
     * Makes this window's context active.
     * The active context will be used when OpenGL functions are called.
     */
    public fun makeActive()

    /**
     * Polls for events.
     *
     * @throws GLFWError maybe.
     */
    @RenderFun
    public fun pollEvents()

    /**
     * Swaps the buffers in the window, causing the currently selected
     * default framebuffer to be switched with the one that is actually being
     * displayed.
     * This technique is called double-buffering.
     *
     * Also, if [isVsyncEnabled] is `true`, this function waits for the next
     * v-blank.
     */
    @RenderFun
    public fun swapBuffers()

    /**
     * Closes this window.
     * Also discards the OpenGL context and state that goes along with it.
     */
    @RenderFun
    override fun close()

    public companion object {
        /**
         * Initializes the window system.
         *
         * @throws GLFWError Initialization failure!
         */
        @RenderFun
        public fun initWindowSystem()

        /**
         * Terminates the window system.
         *
         * @throws Nothing Tear-down functions are not to throw.
         */
        @RenderFun
        public fun terminateWindowSystem()

        /**
         * Gets the current error's description if there is an error currently
         * in context.
         *
         * @return The current error's description, or `null` if there is no
         * error.
         *
         * @throws Nothing This function is not to throw.
         */
        @RenderFun
        public fun getErrorDescription(): String?

        /**
         * Creates a new window in a windowed state.
         *
         * @param width The width of the usable window area, in pixels.
         * @param height The height of the usable window area, in pixels.
         * @param title User-facing title of the window.
         *
         * @return A brand new [Window]. The window is not active.
         *
         * @throws GLFWError Creation error!
         */
        @RenderFun
        public fun createWindowed(
            width: Int,
            height: Int,
            title: String
        ): Window

        /**
         * Creates a new window in a windowed state.
         *
         * @param game The game to create it for.
         * All windows must be associated with a game.
         * @param width The width of the usable window area, in pixels.
         * @param height The height of the usable window area, in pixels.
         * @param title User-facing title of the window.
         *
         * @return A brand new [Window]. The window is not active.
         *
         * @throws GLFWError Creation error!
         */
        @RenderFun
        @Deprecated("Replaced by the PlatformService variant")
        public fun createWindowed(
            game: Game,
            width: Int,
            height: Int,
            title: String
        ): Window
    }
}
