/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.platform

import net.derfruhling.kemono.Game
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger
import kotlin.reflect.KProperty

/**
 * Allows automatically disposing a resource when the [Game] itself is
 * [Disposable.dispose]d.
 *
 * The context is kept globally by this class's companion object.
 */
public class AutoDisposingDelegate<T : Disposable>(value: T) : DisposingDelegate<T> {
    private var value: T? = value

    init {
        toDispose.add(this)
    }

    /**
     * Gets the value kept by this delegate.
     *
     * @return [value!!][value] (never returning `null`)
     * @throws IllegalStateException The value is already disposed.
     */
    override fun getValue(self: Any, property: KProperty<*>): T {
        return value ?: throw IllegalStateException("value disposed")
    }

    override fun dispose() {
        value?.dispose()
        value = null
    }

    public companion object : Disposable {
        private val log by logger
        private val toDispose = mutableListOf<Disposable>()

        /**
         * Registers an arbitrary disposable to be disposed automagically.
         *
         * @param disposable The value to dispose later.
         */
        public fun register(disposable: Disposable) {
            toDispose.add(disposable)
        }

        override fun dispose() {
            log.debug("Auto disposing all applicable resources")

            toDispose.reversed().forEach {
                log.trace("Auto disposing {}", it)
                it.dispose()
            }

            log.debug("Finished auto disposing all applicable resources")
        }
    }
}

/**
 * Wraps a disposable value in an [AutoDisposingDelegate], which disposes
 * the value when the [Game] itself is [Disposable.dispose]d.
 *
 * @param value The value to wrap.
 *
 * @return A brand new [AutoDisposingDelegate].
 *
 * @throws Nothing This function does not throw.
 */
public fun <T : Disposable> autoDisposing(value: T): AutoDisposingDelegate<T> = AutoDisposingDelegate(value)

/**
 * Wraps a disposable value in an [AutoDisposingDelegate], which disposes
 * the value when the [Game] itself is [Disposable.dispose]d.
 *
 * @receiver The value to wrap.
 *
 * @return A brand new [AutoDisposingDelegate].
 *
 * @throws Nothing This function does not throw.
 */
public fun <T : Disposable> T.autoDisposing(): AutoDisposingDelegate<T> = AutoDisposingDelegate(this)
