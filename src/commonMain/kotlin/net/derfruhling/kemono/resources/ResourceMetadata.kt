/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.listSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import kotlinx.serialization.encoding.encodeStructure
import net.derfruhling.kemono.resources.husks.Resource

/**
 * Specifies a resources [type] and [name].
 *
 * @property type Important in deserialization as it's the most reliable form
 * of identification for resources.
 *
 * @property name Important in organization.
 */
@Serializable(with = ResourceMetadata.Serializer::class)
public data class ResourceMetadata(
    val type: String,
    val name: String
) {
    /**
     * Serializable data class imitating a [Resource].
     * It only includes the [Resource.metadata] field.
     *
     * @property value The metadata.
     */
    @Serializable
    public data class Wrapped(
        @SerialName("~")
        val value: ResourceMetadata
    )

    @Suppress("KDocMissingDocumentation")
    public object Serializer : KSerializer<ResourceMetadata> {
        @OptIn(ExperimentalSerializationApi::class)
        override val descriptor: SerialDescriptor = listSerialDescriptor<String>()

        override fun deserialize(decoder: Decoder): ResourceMetadata {
            return decoder.decodeStructure(descriptor) {
                val type = decodeStringElement(descriptor, index = 0)
                val name = decodeStringElement(descriptor, index = 1)

                ResourceMetadata(type, name)
            }
        }

        override fun serialize(encoder: Encoder, value: ResourceMetadata) {
            encoder.encodeStructure(descriptor) {
                encodeStringElement(descriptor, index = 0, value.type)
                encodeStringElement(descriptor, index = 1, value.name)
            }
        }
    }
}