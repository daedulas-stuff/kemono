/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources

import net.derfruhling.kemono.resources.husks.Resource

/**
 * This is a pre-skimmed resource where the [metadata] is already
 * read and known.
 * The actual resource is only a [read] away.
 * Or [readAnonymously], if that's your thing.
 */
public interface ResourceInfo {
    /**
     * The metadata of the resource.
     */
    public val metadata: ResourceMetadata

    /**
     * Reads this resource anonymously.
     * See [ResourceEncoding.readAnonymously] for a better description of
     * what this actually does.
     *
     * @return The read resource.
     */
    public fun readAnonymously(): Resource
}

/**
 * Reads this resource.
 *
 * Actually calls [ResourceInfo.readAnonymously] and casts it to [T].
 *
 * @return [T]
 * @throws IllegalStateException The resource could not be cast to [T].
 */
public inline fun <reified T : Resource> ResourceInfo.read(): T {
    return readAnonymously() as? T
        ?: throw IllegalStateException("cannot cast resource to ${T::class}")
}
