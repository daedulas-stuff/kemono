/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources

import net.derfruhling.kemono.resources.husks.Resource
import me.derfruhling.quantum.core.log.logger
import okio.*
import okio.Path.Companion.toPath

/**
 * Allows creating various objects relating to resource management.
 */
public object Resources {
    private val log by logger

    private data class FileSystemResourceNode(
        override val metadata: ResourceMetadata,
        val fileSystem: FileSystem,
        val path: Path
    ) : ResourceInfo {
        override fun readAnonymously(): Resource {
            return ResourceEncoding.readAnonymously(fileSystem.read(path) { readByteArray() })
        }
    }

    private data class ArbitraryResourceInfo(
        val value: Resource
    ) : ResourceInfo {
        override val metadata: ResourceMetadata by value::metadata

        override fun readAnonymously() = value
    }

    /**
     * Creates a new [FileSystem] based [ResourceStorage] that reads resources
     * from a directory.
     * When this function is called, it will iterate across all files in that
     * directory recursively, making note of every resource it comes across.
     *
     * @param fileSystem The [FileSystem] to read from.
     * @param rootPath The path to the directory that contains resources.
     */
    public fun createFileSystemStorage(
        fileSystem: FileSystem = FileSystem.SYSTEM,
        rootPath: Path = "resources".toPath()
    ): ResourceStorage = object : ResourceStorage {
        override val allResources: Map<String, ResourceInfo> = run {
            fun iterateDir(fileSystem: FileSystem, rootPath: Path): Sequence<Pair<String, ResourceInfo>> = sequence {
                fileSystem.list(rootPath).forEach { p ->
                    val fileMetadata = fileSystem.metadata(p)

                    if (fileMetadata.isDirectory) yieldAll(iterateDir(fileSystem, p))
                    else {
                        val resourceMetadata = fileSystem.source(p).use { source ->
                            val byteArray = source.buffer().readByteArray()
                            log.debug("Loading resource data from {}: {} bytes", p.toString(), byteArray)
                            ResourceEncoding.skim(byteArray)
                        }

                        yield(resourceMetadata.name to FileSystemResourceNode(resourceMetadata, fileSystem, p))
                    }
                }
            }

            iterateDir(fileSystem, rootPath).toMap()
        }
    }

    /**
     * Creates an arbitrary [ResourceStorage] that contains preloaded
     * resources.
     *
     * @param resources The resources to preload.
     */
    public fun createArbitraryStorage(vararg resources: Resource): ResourceStorage = object : ResourceStorage {
        override val allResources: Map<String, ResourceInfo> = resources.associate {
            it.metadata.name to ArbitraryResourceInfo(it)
        }
    }

    /**
     * Combines multiple [resourceStorages] together to create an amalgamate
     * [ResourceStorage] that references all of their resources at once.
     *
     * @param resourceStorages The [ResourceStorage]s. They do not all need
     * to be the same type.
     */
    public fun createAmalgamation(vararg resourceStorages: ResourceStorage): ResourceStorage =
        object : ResourceStorage {
            override val allResources: Map<String, ResourceInfo> = resourceStorages
                .map { it.allResources.toList() }
                .flatten()
                .toMap()
        }
}