/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.decodeFromByteArray
import net.derfruhling.kemono.resources.husks.*

/**
 * Manages the loading of resources from binary CBOR format.
 * Also allows "skimming" resources to determine their type and name.
 */
@OptIn(ExperimentalSerializationApi::class)
public object ResourceEncoding {
    /**
     * [ResourceEncoding]'s very default and generic CBOR instance.
     */
    public val cbor: Cbor = Cbor {

    }

    private val cborSkim: Cbor = Cbor {
        ignoreUnknownKeys = true
    }

    /**
     * "Skims" a resource for its common metadata that describes its type
     * and name.
     *
     * @param byteArray CBOR encoded resource.
     * It is safe to pass the full resource in here, even though just the
     * metadata part is actually parsed here.
     *
     * @return [ResourceMetadata]
     */
    public fun skim(byteArray: ByteArray): ResourceMetadata {
        return cborSkim.decodeFromByteArray<ResourceMetadata.Wrapped>(byteArray).value
    }

    /**
     * Completely reads a resource as [T].
     * If you're looking to determine what type [T] should be, try using [skim]
     * to retrieve the serialized type of the resource.
     *
     * @param T The resource type. Serializable.
     * @param byteArray CBOR encoded resource.
     *
     * @return A brand new [T]
     */
    public inline fun <reified T : Resource> read(byteArray: ByteArray): T {
        return cbor.decodeFromByteArray(byteArray)
    }

    /**
     * Reads a resource "anonymously."
     * The resource is first [skim]ed, and then based on its type, it is read
     * as the correct [Resource] subclass.
     */
    public fun readAnonymously(byteArray: ByteArray): Resource {
        val meta = skim(byteArray)

        return when (meta.type) {
            "kmShader" -> read<ShaderResource>(byteArray)
            "kmTexture" -> read<TextureResource>(byteArray)
            "kmFontDef" -> read<FontDefResource>(byteArray)
            "kmLocale" -> read<LocaleResource>(byteArray)
            else -> throw IllegalArgumentException("not a known resource")
        }
    }
}