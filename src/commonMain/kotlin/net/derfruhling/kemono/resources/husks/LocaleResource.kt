/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources.husks

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import net.derfruhling.kemono.resources.Language
import net.derfruhling.kemono.resources.ResourceMetadata

@Serializable
public class LocaleResource(
    @SerialName("~") override val metadata: ResourceMetadata,
    @SerialName("l") public val language: Language,
    @SerialName("t") public val texts: List<LocaleEntry>
) : Resource() {
    @Suppress("KDocMissingDocumentation")
    public enum class LocaleEntryType {
        TEXT,
        SEQUENCE
    }

    /**
     * Represents an entry in the locale table.
     *
     * @property type The type of this entry.
     * @property key The key that this entry fills.
     * @property value The value of the entry.
     */
    @Serializable(with = LocaleEntry.Serializer::class)
    public data class LocaleEntry(
        val type: LocaleEntryType,
        val key: String,
        val value: String // sequences
    ) {
        @Suppress("KDocMissingDocumentation")
        public object Serializer : KSerializer<LocaleEntry> {
            @OptIn(ExperimentalSerializationApi::class, InternalSerializationApi::class)
            override val descriptor: SerialDescriptor = buildSerialDescriptor(
                "net.derfruhling.kemono.resources.husks.LocaleResource.LocaleEntry",
                StructureKind.LIST
            )

            override fun deserialize(decoder: Decoder): LocaleEntry {
                return decoder.decodeStructure(descriptor) {
                    LocaleEntry(
                        LocaleEntryType.entries[decodeIntElement(descriptor, index = 0)],
                        decodeStringElement(descriptor, index = 1),
                        decodeStringElement(descriptor, index = 2)
                    )
                }
            }

            override fun serialize(encoder: Encoder, value: LocaleEntry) {
                throw UnsupportedOperationException("Cannot serialize")
            }
        }
    }
}