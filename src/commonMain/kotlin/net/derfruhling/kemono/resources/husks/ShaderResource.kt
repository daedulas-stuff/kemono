/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources.husks

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.descriptors.serialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import net.derfruhling.kemono.graphics.enums.ValueType
import net.derfruhling.kemono.resources.ResourceMetadata

/**
 * A serialized shader resource husk.
 */
@Serializable
@Suppress("KDocMissingDocumentation")
public data class ShaderResource(
    @SerialName("~") override val metadata: ResourceMetadata,
    @SerialName("i") public val inputs: List<ShaderInput> = emptyList(),
    @SerialName("o") public val outputs: List<ShaderOutput> = emptyList(),
    @SerialName("d") public val definitions: Map<String, String> = emptyMap(),
    @SerialName("a") public val uniformAccepts: Map<String, UniformAcceptValue> = emptyMap(),
    @SerialName("v") public val vertexShaderSource: String,
    @SerialName("f") public val fragmentShaderSource: String,
    @SerialName("g") public val geometryShaderSource: String? = null,
) : Resource() {
    @Serializable(with = InputValueSource.Serializer::class)
    public enum class InputValueSource {
        // ordinals important
        ZERO,
        ONE,
        POSITION_X,
        POSITION_Y,
        POSITION_Z,
        COLOR_R,
        COLOR_G,
        COLOR_B,
        COLOR_A,
        TEX_COORD_X,
        TEX_COORD_Y;

        public object Serializer : KSerializer<InputValueSource> {
            override val descriptor: SerialDescriptor
                get() = serialDescriptor<Int>()

            override fun deserialize(decoder: Decoder): InputValueSource {
                return entries[decoder.decodeInline(descriptor).decodeInt()]
            }

            override fun serialize(encoder: Encoder, value: InputValueSource) {
                encoder.encodeInline(descriptor).encodeInt(value.ordinal)
            }
        }
    }

    @Serializable
    public enum class UniformAcceptValue {
        CAMERA_POSITION,
        ENTITY_POSITION,
        ENTITY_DIRECTION,
        CHUNK_POSITION,
        CHUNK_SIZE,

        UI_BOUNDS,

        RENDER_TARGET_BOUNDS,
        RENDER_TARGET_ASPECT_RATIO_W_OVER_H,
        RENDER_TARGET_ASPECT_RATIO_H_OVER_W,
        TILES_CENTER_TO_TOP,

        TEXTURE_0,
        TEXTURE_1,
        TEXTURE_2,
        TEXTURE_3,
        TEXTURE_4,
        TEXTURE_5,
        TEXTURE_6,
        TEXTURE_7,
    }

    @Serializable(with = ShaderInput.Serializer::class)
    public data class ShaderInput(
        val name: String,
        val type: ValueType,
        val size: Int,
        val source: Array<InputValueSource>
    ) {
        @Suppress("DuplicatedCode")
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is ShaderInput) return false

            if (name != other.name) return false
            if (type != other.type) return false
            if (size != other.size) return false
            if (!source.contentEquals(other.source)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = name.hashCode()
            result = 31 * result + type.hashCode()
            result = 31 * result + size.hashCode()
            result = 31 * result + source.contentHashCode()
            return result
        }

        @OptIn(ExperimentalSerializationApi::class)
        public object Serializer : KSerializer<ShaderInput> {
            @OptIn(InternalSerializationApi::class)
            override val descriptor: SerialDescriptor = buildSerialDescriptor(
                "net.derfruhling.kemono.resources.husks.ShaderResource.ShaderInput",
                StructureKind.LIST
            )

            override fun deserialize(decoder: Decoder): ShaderInput {
                return decoder.decodeStructure(descriptor) {
                    val name = decodeStringElement(descriptor, index = 0)
                    val type = decodeSerializableElement(descriptor, index = 1, ValueType.serializer())
                    val size = decodeIntElement(descriptor, index = 2)
                    val inputValueSources =
                        decodeSerializableElement(descriptor, index = 3, serializer<Array<InputValueSource>>())
                    ShaderInput(name, type, size, inputValueSources)
                }
            }

            override fun serialize(encoder: Encoder, value: ShaderInput) {
                throw UnsupportedOperationException("Cannot serialize resource husks")
            }
        }
    }

    @Serializable(with = ShaderOutput.Serializer::class)
    public data class ShaderOutput(
        val name: String,
        val into: Int
    ) {
        @OptIn(ExperimentalSerializationApi::class)
        public object Serializer : KSerializer<ShaderOutput> {
            @OptIn(InternalSerializationApi::class)
            override val descriptor: SerialDescriptor = buildSerialDescriptor(
                "net.derfruhling.kemono.resources.husks.ShaderResource.ShaderOutput",
                StructureKind.LIST
            )

            override fun deserialize(decoder: Decoder): ShaderOutput {
                return decoder.decodeStructure(descriptor) {
                    val name = decodeStringElement(descriptor, index = 0)
                    val into = decodeIntElement(descriptor, index = 1)
                    ShaderOutput(name, into)
                }
            }

            override fun serialize(encoder: Encoder, value: ShaderOutput) {
                throw UnsupportedOperationException("Cannot serialize resource husks")
            }
        }
    }
}