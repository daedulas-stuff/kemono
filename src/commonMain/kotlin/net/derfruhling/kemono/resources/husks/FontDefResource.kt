/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources.husks

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import net.derfruhling.kemono.resources.ResourceMetadata

@Serializable
public class FontDefResource(
    @SerialName("~") override val metadata: ResourceMetadata,
    @SerialName("a") public val textureAssetName: String,
    @SerialName("t") public val type: Int,

    // Spritemap values
    @SerialName("w") public val spritemapWidth: Int = 0,
    @SerialName("h") public val spritemapHeight: Int = 0,
    @SerialName("xo") public val spritemapXOffset: Int = 0,
    @SerialName("yo") public val spritemapYOffset: Int = 0,
    @SerialName("xi") public val spritemapXIgnored: Int = 0,
    @SerialName("yi") public val spritemapYIgnored: Int = 0,

    // Common
    @SerialName("c") public val characters: List<CharDef>
) : Resource() {
    @Serializable(with = CharDef.Serializer::class)
    public data class CharDef(
        val target: Char,
        val x: Int = 0,
        val y: Int = 0
    ) {
        public object Serializer : KSerializer<CharDef> {
            @OptIn(ExperimentalSerializationApi::class, InternalSerializationApi::class)
            override val descriptor: SerialDescriptor = buildSerialDescriptor(
                "net.derfruhling.kemono.resources.husks.FontDefResource.CharacterRef",
                StructureKind.LIST
            )

            @OptIn(ExperimentalSerializationApi::class)
            override fun deserialize(decoder: Decoder): CharDef {
                return decoder.decodeStructure(descriptor) {
                    if(decodeSequentially()) {
                        CharDef(
                            target = decodeIntElement(descriptor, index = 0).toChar(),
                            x = decodeIntElement(descriptor, index = 1),
                            y = decodeIntElement(descriptor, index = 2),
                        )
                    } else {
                        var target = '\u0000'
                        var x = 0
                        var y = 0

                        while(true) {
                            val index = decodeElementIndex(descriptor)
                            if(index == CompositeDecoder.DECODE_DONE) {
                                break
                            } else if(index >= 0) {
                                when(index) {
                                    0 -> target = decodeIntElement(descriptor, index = 0).toChar()
                                    1 -> x = decodeIntElement(descriptor, index = 1)
                                    2 -> y = decodeIntElement(descriptor, index = 2)
                                }
                            }
                        }

                        CharDef(target, x, y)
                    }
                }
            }

            override fun serialize(encoder: Encoder, value: CharDef) {
                throw UnsupportedOperationException("Cannot serialize CharDef")
            }
        }
    }
}