/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.resources.husks

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.cbor.ByteString
import net.derfruhling.kemono.graphics.enums.*
import net.derfruhling.kemono.math.Vector4
import net.derfruhling.kemono.resources.ResourceMetadata
import net.derfruhling.kemono.graphics.enums.ValueType as KmValueType

/**
 * Represents a texture resource husk, which can be loaded into a complete
 * texture.
 */
@Serializable
@Suppress("KDocMissingDocumentation")
public data class TextureResource(
    @SerialName("~") override val metadata: ResourceMetadata,
    @SerialName("x") public val options: Options,
    @SerialName("s") public val size: UIntArray,

    @SerialName("b")
    @OptIn(ExperimentalSerializationApi::class)
    @ByteString
    val byteArray: ByteArray
) : Resource() {
    @Serializable
    public data class Options(
        @SerialName("t") val type: Type,
        @SerialName("a") val arrangement: Arrangement,
        @SerialName("v") val valueType: ValueType,
        @SerialName("e") val edgeModes: Array<EdgeMode>,
        @SerialName("b") val borderColor: Vector4<Float>? = null,
        @SerialName("f-") val minifyFilter: MinifyFilter,
        @SerialName("f+") val magnifyFilter: MagnifyFilter,
        @SerialName("m") val enableMipmaps: Boolean
    ) {
        @Suppress("DuplicatedCode")
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Options) return false

            if (type != other.type) return false
            if (arrangement != other.arrangement) return false
            if (valueType != other.valueType) return false
            if (!edgeModes.contentEquals(other.edgeModes)) return false
            if (borderColor != other.borderColor) return false
            if (minifyFilter != other.minifyFilter) return false
            if (magnifyFilter != other.magnifyFilter) return false
            if (enableMipmaps != other.enableMipmaps) return false

            return true
        }

        override fun hashCode(): Int {
            var result = type.hashCode()
            result = 31 * result + arrangement.hashCode()
            result = 31 * result + valueType.hashCode()
            result = 31 * result + edgeModes.contentHashCode()
            result = 31 * result + (borderColor?.hashCode() ?: 0)
            result = 31 * result + minifyFilter.hashCode()
            result = 31 * result + magnifyFilter.hashCode()
            result = 31 * result + enableMipmaps.hashCode()
            return result
        }
    }

    @Serializable
    public enum class Type {
        @SerialName("1d")
        TEXTURE_1D,

        @SerialName("2d")
        TEXTURE_2D,

        @SerialName("3d")
        TEXTURE_3D
    }

    @Serializable
    public enum class Arrangement(
        public val real: TextureFormat,
        public val internal: InternalTextureFormat
    ) {
        @SerialName("r")
        R(TextureFormat.R, InternalTextureFormat.R),

        @SerialName("rg")
        RG(TextureFormat.RG, InternalTextureFormat.RG),

        @SerialName("rgb")
        RGB(TextureFormat.RGB, InternalTextureFormat.RGB),

        @SerialName("rgba")
        RGBA(TextureFormat.RGBA, InternalTextureFormat.RGBA),

        @SerialName("bgr")
        BGR(TextureFormat.BGR, InternalTextureFormat.RGB),

        @SerialName("bgra")
        BGRA(TextureFormat.BGRA, InternalTextureFormat.RGBA)
    }

    @Serializable
    public enum class ValueType(
        public val real: KmValueType,
        public val requiresConversionToLittleEndian: Boolean
    ) {
        @SerialName("f4")
        FLOAT(KmValueType.FLOAT, true),

        @SerialName("u4")
        U_INT(KmValueType.U_INT, true),

        @SerialName("u2")
        U_SHORT(KmValueType.U_SHORT, true),

        @SerialName("u1")
        U_BYTE(KmValueType.U_BYTE, false),
    }

    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TextureResource) return false

        if (metadata != other.metadata) return false
        if (options != other.options) return false
        if (size != other.size) return false
        if (!byteArray.contentEquals(other.byteArray)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = metadata.hashCode()
        result = 31 * result + options.hashCode()
        result = 31 * result + size.hashCode()
        result = 31 * result + byteArray.contentHashCode()
        return result
    }
}