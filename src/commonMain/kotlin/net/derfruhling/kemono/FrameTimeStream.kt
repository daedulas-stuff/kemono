/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import kotlin.time.Duration
import kotlin.time.DurationUnit

internal abstract class FrameTimeStream {
    abstract val desiredFrameRate: Int

    private val desiredFrameTime: Double
        get() = 1.0 / desiredFrameRate.toDouble()

    private val array = FloatArray(3) { 1.0f }
    private var nextIndex = 0

    fun next(): Float {
        return array.average().toFloat()
    }

    internal fun put(frameDuration: Duration) {
        val frameTime = frameDuration.toDouble(DurationUnit.SECONDS)
        array[nextIndex++] = (frameTime / desiredFrameTime).toFloat()

        if(nextIndex >= array.size) {
            nextIndex = 0
        }
    }
}