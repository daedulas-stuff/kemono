/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input

import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import net.derfruhling.kemono.AsyncService
import net.derfruhling.kemono.Game
import net.derfruhling.kemono.input.actions.*
import net.derfruhling.kemono.platform.AutoDisposingDelegate
import net.derfruhling.kemono.platform.DisposingDelegate
import net.derfruhling.kemono.platform.PlatformService
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.interfaces.Closeable
import me.derfruhling.quantum.core.log.logger
import kotlin.math.absoluteValue
import kotlin.reflect.KProperty

/**
 * The input system service, contained inside a [Game].
 */
public class InputService(
    override val serviceLoader: ServiceLoader
) : Service, Closeable {
    private val platformService: PlatformService by service()
    private val asyncService: AsyncService by service()

    private val log by logger
    private val axisSources = mutableListOf<AxisInputSource>()
    private val axisAcceptors = mutableListOf<InputAcceptor<Float>>()
    private val buttonSources = mutableListOf<ButtonInputSource>()
    private val buttonAcceptors = mutableListOf<InputAcceptor<Boolean>>()
    private val jobs = mutableMapOf<InputSource<*>, Job>()

    private inline fun <T> addSource(
        sources: MutableList<InputSource<T>>,
        crossinline acceptors: () -> Iterable<InputAcceptor<T>>,
        inputSource: InputSource<T>
    ) {
        sources.add(inputSource)

        jobs.remove(inputSource)?.cancel()

        jobs[inputSource] = asyncService.launch {
            inputSource.flowFor(platformService.primaryWindow).collect { (trigger, newState) ->
                acceptors().forEach { acceptor ->
                    acceptor.accept(trigger, newState)
                }
            }

            log.warn("Flow for {} ended?", inputSource)
        }
    }

    /**
     * Adds an [AxisInputSource] to this input system.
     * It's values will be used and provided to [InputAcceptor]s.
     *
     * @param axisInputSource The source of the triggers.
     */
    public fun addAxisSource(axisInputSource: AxisInputSource) {
        log.debug("Adding axis input source {}", axisInputSource)
        addSource(axisSources, { axisAcceptors }, axisInputSource)
    }

    /**
     * Adds an [AxisInputSource] to this input system.
     * It's values will be used and provided to [InputAcceptor]s.
     *
     * @param constructor Constructs an [AxisInputSource]
     */
    public inline fun addAxisSource(constructor: () -> AxisInputSource) {
        addAxisSource(constructor())
    }

    /**
     * Adds an [ButtonInputSource] to this input system.
     * It's values will be used and provided to [InputAcceptor]s.
     *
     * @param buttonInputSource The source of the triggers.
     */
    public fun addButtonSource(buttonInputSource: ButtonInputSource) {
        log.debug("Adding button input source {}", buttonInputSource)
        addSource(buttonSources, { buttonAcceptors }, buttonInputSource)
    }

    /**
     * Adds an [ButtonInputSource] to this input system.
     * It's values will be used and provided to [InputAcceptor]s.
     *
     * @param constructor Constructs an [ButtonInputSource]
     */
    public inline fun addButtonSource(constructor: () -> ButtonInputSource) {
        addButtonSource(constructor())
    }

    override fun close() {
        jobs.forEach { (_, job) -> job.cancel("InputSystem closed") }
    }

    /**
     * Registers an acceptor to accept axis triggers.
     * The acceptor will receive events until [deregisterAxisAcceptor] is
     * called with the same parameter.
     */
    public fun registerAxisAcceptor(acceptor: InputAcceptor<Float>) {
        axisAcceptors.add(acceptor)
    }

    /**
     * Registers an acceptor to accept button triggers.
     * The acceptor will receive events until [deregisterButtonAcceptor] is
     * called with the same parameter.
     */
    public fun registerButtonAcceptor(acceptor: InputAcceptor<Boolean>) {
        buttonAcceptors.add(acceptor)
    }

    /**
     * Removed a registered axis acceptor.
     * The acceptor will not receive any further events.
     */
    public fun deregisterAxisAcceptor(acceptor: InputAcceptor<Float>) {
        axisAcceptors.remove(acceptor)
    }

    /**
     * Removed a registered button acceptor.
     * The acceptor will not receive any further events.
     */
    public fun deregisterButtonAcceptor(acceptor: InputAcceptor<Boolean>) {
        buttonAcceptors.remove(acceptor)
    }


    /**
     * Creates a new button input action.
     * Whenever any of the [triggers] are triggered, this value's
     * [InputAction.state] is set to `true`, and remains `true` until all
     * the triggers are finished.
     *
     * @param triggers A list of triggers.
     *
     * @return A delegate for the action.
     * This value should not be disposed manually.
     */
    public fun buttonInputAction(vararg triggers: ButtonInputTrigger): DisposingDelegate<SimpleInputAction> {
        return object : DisposingDelegate<SimpleInputAction> {
            private val action = BasicSimpleInputAction(*triggers)

            init {
                registerButtonAcceptor(action)

                AutoDisposingDelegate.register(this)
            }

            override fun getValue(self: Any, property: KProperty<*>): SimpleInputAction {
                return action
            }

            override fun dispose() {
                deregisterButtonAcceptor(action)
            }
        }
    }

    /**
     * Creates a new axis input action.
     * Whenever any of the [triggers] are triggered, this value's
     * [InputAction.state] is set to the value of the trigger with the
     * most significant value, which is defined by comparing [absoluteValue].
     *
     * @param triggers A list of triggers.
     *
     * @return A delegate for the action.
     * This value should not be disposed manually.
     */
    public fun axisInputAction(vararg triggers: AxisInputTrigger): DisposingDelegate<AxisInputAction> {
        return object : DisposingDelegate<AxisInputAction> {
            private val action = BasicAxisInputAction(*triggers)

            init {
                registerAxisAcceptor(action)

                AutoDisposingDelegate.register(this)
            }

            override fun getValue(self: Any, property: KProperty<*>): AxisInputAction {
                return action
            }

            override fun dispose() {
                deregisterAxisAcceptor(action)
            }
        }
    }

    /**
     * Creates a new vector input action.
     * Whenever any of the `triggers` are triggered, this value's
     * [InputAction.state] is set to a vector with the most significant values
     * of [xTriggers] and [yTriggers], which go into `x` and `y`, respectively.
     *
     * @param xTriggers A list of triggers used for the `x` part.
     * @param yTriggers A list of triggers used for the `y` part.
     *
     * @return A delegate for the action.
     * This value should not be disposed manually.
     */
    public fun vectorInputAction(
        xTriggers: List<AxisInputTrigger>,
        yTriggers: List<AxisInputTrigger>
    ): DisposingDelegate<VectorInputAction> {
        return object : DisposingDelegate<VectorInputAction> {
            private val action = BasicVectorInputAction(xTriggers, yTriggers)

            init {
                registerAxisAcceptor(action)

                AutoDisposingDelegate.register(this)
            }

            override fun getValue(self: Any, property: KProperty<*>): VectorInputAction {
                return action
            }

            override fun dispose() {
                deregisterAxisAcceptor(action)
            }
        }
    }
}
