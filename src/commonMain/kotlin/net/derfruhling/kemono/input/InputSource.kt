/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input

import kotlinx.coroutines.flow.Flow
import net.derfruhling.kemono.platform.Window

/**
 * A source of input triggers.
 */
public interface InputSource<T> {
    /**
     * Returns a flow of input triggers for a [window].
     *
     * @param window The window.
     *
     * @return A flow. The flow should not end.
     */
    public fun flowFor(window: Window): Flow<Pair<InputTrigger<T>, T>>
}

/**
 * An input source of button triggers.
 */
public typealias ButtonInputSource = InputSource<Boolean>

/**
 * An input source of axis triggers.
 */
public typealias AxisInputSource = InputSource<Float>
