/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input

/**
 * An input trigger. Can be arbitrary.
 */
public interface InputTrigger<T> {
    /**
     * A key on a keyboard.
     *
     * @property glfw The GLFW value of the key.
     * You probably will not need this.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class Key(public val glfw: Int) : ButtonInputTrigger {
        UNKNOWN(-1),
        SPACE(32),
        APOSTROPHE(39),
        COMMA(44),
        MINUS(45),
        PERIOD(46),
        SLASH(47),
        NUM_0(48),
        NUM_1(49),
        NUM_2(50),
        NUM_3(51),
        NUM_4(52),
        NUM_5(53),
        NUM_6(54),
        NUM_7(55),
        NUM_8(56),
        NUM_9(57),
        SEMICOLON(59),
        EQUAL(61),
        A(65),
        B(66),
        C(67),
        D(68),
        E(69),
        F(70),
        G(71),
        H(72),
        I(73),
        J(74),
        K(75),
        L(76),
        M(77),
        N(78),
        O(79),
        P(80),
        Q(81),
        R(82),
        S(83),
        T(84),
        U(85),
        V(86),
        W(87),
        X(88),
        Y(89),
        Z(90),
        LEFT_BRACKET(91),
        BACKSLASH(92),
        RIGHT_BRACKET(93),
        GRAVE_ACCENT(96),
        WORLD_1(161),
        WORLD_2(162),
        ESCAPE(256),
        ENTER(257),
        TAB(258),
        BACKSPACE(259),
        INSERT(260),
        DELETE(261),
        RIGHT(262),
        LEFT(263),
        DOWN(264),
        UP(265),
        PAGE_UP(266),
        PAGE_DOWN(267),
        HOME(268),
        END(269),
        CAPS_LOCK(280),
        SCROLL_LOCK(281),
        NUM_LOCK(282),
        PRINT_SCREEN(283),
        PAUSE(284),
        F1(290),
        F2(291),
        F3(292),
        F4(293),
        F5(294),
        F6(295),
        F7(296),
        F8(297),
        F9(298),
        F10(299),
        F11(300),
        F12(301),
        F13(302),
        F14(303),
        F15(304),
        F16(305),
        F17(306),
        F18(307),
        F19(308),
        F20(309),
        F21(310),
        F22(311),
        F23(312),
        F24(313),
        F25(314),
        KP_0(320),
        KP_1(321),
        KP_2(322),
        KP_3(323),
        KP_4(324),
        KP_5(325),
        KP_6(326),
        KP_7(327),
        KP_8(328),
        KP_9(329),
        KP_DECIMAL(330),
        KP_DIVIDE(331),
        KP_MULTIPLY(332),
        KP_SUBTRACT(333),
        KP_ADD(334),
        KP_ENTER(335),
        KP_EQUAL(336),
        LEFT_SHIFT(340),
        LEFT_CONTROL(341),
        LEFT_ALT(342),
        LEFT_SUPER(343),
        RIGHT_SHIFT(344),
        RIGHT_CONTROL(345),
        RIGHT_ALT(346),
        RIGHT_SUPER(347),
        MENU(348),

        ;

        public companion object {
            public val valuesByGlfw: Map<Int, Key> =
                entries.associateBy { it.glfw }
        }
    }

    /**
     * Specifies a custom two-dimensional axis.
     */
    @Suppress("KDocMissingDocumentation")
    public interface CustomAxisSpec<T> {
        public val x: InputTrigger<T>
        public val y: InputTrigger<T>
    }

    /**
     * The default keyboard access, eg. WASD.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class KeyboardAxis : AxisInputTrigger {
        X,
        Y

        ;

        public companion object : CustomAxisSpec<Float> {
            override val x: InputTrigger<Float>
                get() = X
            override val y: InputTrigger<Float>
                get() = Y
        }

    }

    /**
     * Represents a mouse _position_.
     * The position is in pixels and is relative to the top left corner of the
     * window.
     * Certain situations may cause this value to be greater than the window's
     * bounds or to become a negative value.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class MousePosition : AxisInputTrigger {
        X,
        Y
    }

    /**
     * Represents a mouse _delta_.
     * The delta is in pixels.
     * This event can only be supplied if a `Window`'s `isMouseDeltaEnabled`
     * event is `true` and `isMouseDeltaSupported` is also `true`.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class MouseDelta : AxisInputTrigger {
        X,
        Y
    }

    /**
     * Represents a mouse button.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class MouseButton(public val value: Int) : ButtonInputTrigger {
        BUTTON_1(0),
        BUTTON_2(1),
        BUTTON_3(2),
        BUTTON_4(3),
        BUTTON_5(4),
        BUTTON_6(5),
        BUTTON_7(6),
        BUTTON_8(7),

        ;

        public companion object {
            public inline val LEFT: MouseButton
                get() = BUTTON_1
            public inline val RIGHT: MouseButton
                get() = BUTTON_2
            public inline val MIDDLE: MouseButton
                get() = BUTTON_3
        }
    }

    /**
     * Represents a button on a gamepad.
     * Playstation buttons are provided via inline `val`s.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class GamepadButton(public val value: Int) : ButtonInputTrigger {
        A(0),
        B(1),
        X(2),
        Y(3),
        LEFT_BUMPER(4),
        RIGHT_BUMPER(5),
        BACK(6),
        START(7),
        GUIDE(8),
        LEFT_THUMB(9),
        RIGHT_THUMB(10),
        DPAD_UP(11),
        DPAD_RIGHT(12),
        DPAD_DOWN(13),
        DPAD_LEFT(14),

        ;

        public companion object {
            public inline val CROSS: GamepadButton
                get() = A
            public inline val CIRCLE: GamepadButton
                get() = B
            public inline val SQUARE: GamepadButton
                get() = X
            public inline val TRIANGLE: GamepadButton
                get() = Y
        }
    }

    /**
     * Represents an axis on a gamepad.
     * These are individual values.
     * The engine will automatically invert Y values to make sure that they are
     * relative to the map coordinate system.
     */
    @Suppress("KDocMissingDocumentation")
    public enum class GamepadAxis(public val value: Int, public val invert: Boolean): AxisInputTrigger {
        LEFT_X(0, false),
        LEFT_Y(1, true),
        RIGHT_X(2, false),
        RIGHT_Y(3, true),
        LEFT_TRIGGER(4, false),
        RIGHT_TRIGGER(5, true)
    }

    /**
     * Represents a special trigger that modifies an existing trigger.
     *
     * These can only be used in input sources that accept them.
     */
    public sealed interface Special<T> : InputTrigger<T> {
        /**
         * Inverts the provided [Boolean] trigger.
         *
         * @param trigger The trigger value to invert.
         */
        public data class Invert(public val trigger: ButtonInputTrigger) : Special<Boolean>
    }
}

/**
 * Represents a button input trigger.
 *
 * @see ButtonInputSource
 */
public typealias ButtonInputTrigger = InputTrigger<Boolean>

/**
 * Represents an axis input trigger.
 *
 * @see AxisInputSource
 */
public typealias AxisInputTrigger = InputTrigger<Float>

/**
 * Inverts a button input trigger.
 * This can only be used in input sources that accept [InputTrigger.Special].
 */
public operator fun ButtonInputTrigger.not(): ButtonInputTrigger {
    return InputTrigger.Special.Invert(this)
}
