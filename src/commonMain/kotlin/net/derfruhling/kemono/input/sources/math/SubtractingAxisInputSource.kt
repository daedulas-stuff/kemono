/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.sources.math

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import net.derfruhling.kemono.input.AxisInputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window
import me.derfruhling.quantum.core.properties.Property

/**
 * Subtracts [value] from every value produced by [real].
 */
public class SubtractingAxisInputSource(
    private val real: AxisInputSource,
    private val value: Property<Float>
) : AxisInputSource {
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        return real.flowFor(window)
            .map { (trigger, newState) ->
                trigger to (newState - value.value)
            }
    }
}
