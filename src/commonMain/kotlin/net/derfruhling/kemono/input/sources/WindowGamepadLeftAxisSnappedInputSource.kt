package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window
import me.derfruhling.quantum.core.properties.Property

/**
 * Sources gamepad left stick axis input triggers from a window and "snaps" them
 * to be only one of eight possible states.
 */
public class WindowGamepadLeftAxisSnappedInputSource(
    deadZoneProperty: Property<Float> = Property.const(0.5f)
) : InputSource<Float> {
    /**
     * The dead zone.
     * Any value that has less "significance" than this value will be discarded
     * and replaced with zero.
     * "Significance" is defined as [Float.absoluteValue].
     */
    public val deadZone: Float by deadZoneProperty

    private var previousX = 0.0f
    private var previousY = 0.0f

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        return window.gamepadAxesFlow.flatMapMerge { array ->
            val x = array[InputTrigger.GamepadAxis.LEFT_X.ordinal]
            val y = -array[InputTrigger.GamepadAxis.LEFT_Y.ordinal]

            flow {
                when {
                    x >= deadZone && previousX < deadZone -> emit(InputTrigger.GamepadAxis.LEFT_X to 1.0f)
                    x < deadZone && previousX >= deadZone -> emit(InputTrigger.GamepadAxis.LEFT_X to 0.0f)
                    x <= -deadZone && previousX > -deadZone -> emit(InputTrigger.GamepadAxis.LEFT_X to -1.0f)
                    x > -deadZone && previousX <= -deadZone -> emit(InputTrigger.GamepadAxis.LEFT_X to 0.0f)
                }

                when {
                    y >= deadZone && previousY < deadZone -> emit(InputTrigger.GamepadAxis.LEFT_Y to 1.0f)
                    y < deadZone && previousY >= deadZone -> emit(InputTrigger.GamepadAxis.LEFT_Y to 0.0f)
                    y <= -deadZone && previousY > -deadZone -> emit(InputTrigger.GamepadAxis.LEFT_Y to -1.0f)
                    y > -deadZone && previousY <= -deadZone -> emit(InputTrigger.GamepadAxis.LEFT_Y to 0.0f)
                }

                previousX = x
                previousY = y
            }
        }
    }

    public companion object : () -> WindowGamepadLeftAxisSnappedInputSource {
        /**
         * Constructs a new default instance of this input source.
         */
        override fun invoke(): WindowGamepadLeftAxisSnappedInputSource {
            return WindowGamepadLeftAxisSnappedInputSource()
        }
    }
}
