package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flatMapMerge
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window
import me.derfruhling.quantum.core.properties.Property
import kotlin.math.absoluteValue

/**
 * Sources gamepad input triggers from a window.
 */
public class WindowGamepadAxisInputSource(
    deadZoneProperty: Property<Float> = Property.const(0.1f)
) : InputSource<Float>  {
    /**
     * The dead zone.
     * Any value that has less "significance" than this value will be discarded
     * and replaced with zero.
     * "Significance" is defined as [Float.absoluteValue].
     */
    public val deadZone: Float by deadZoneProperty

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        return window.gamepadAxesFlow.flatMapMerge { array ->
            InputTrigger.GamepadAxis.entries.mapIndexed { index, gamepadAxis ->
                if(index !in array.indices) return@mapIndexed null
                if(array[index].absoluteValue < deadZone)
                    return@mapIndexed gamepadAxis to 0.0f

                gamepadAxis to when(gamepadAxis.invert) {
                    true -> -array[index]
                    false -> array[index]
                }
            }.filterNotNull().asFlow()
        }
    }

    public companion object : () -> WindowGamepadAxisInputSource {
        /**
         * Creates a new default value of this input source.
         */
        override fun invoke(): WindowGamepadAxisInputSource {
            return WindowGamepadAxisInputSource()
        }
    }
}
