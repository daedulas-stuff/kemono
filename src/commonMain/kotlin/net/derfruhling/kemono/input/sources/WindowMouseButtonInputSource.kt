package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.flow.Flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources mouse button input triggers from a window.
 */
public data object WindowMouseButtonInputSource : InputSource<Boolean> {
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Boolean>, Boolean>> {
        return window.mouseButtonFlow
    }
}