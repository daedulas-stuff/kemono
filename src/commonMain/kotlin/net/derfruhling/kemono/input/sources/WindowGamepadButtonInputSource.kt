package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flatMapMerge
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources gamepad button input triggers from a window.
 */
public data object WindowGamepadButtonInputSource : InputSource<Boolean>  {
    @OptIn(ExperimentalCoroutinesApi::class)
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Boolean>, Boolean>> {
        return window.gamepadButtonsFlow.flatMapMerge { array ->
            InputTrigger.GamepadButton.entries.mapIndexed { index, gamepadButton ->
                if(index !in array.indices) return@mapIndexed null
                gamepadButton to array[index]
            }.filterNotNull().asFlow()
        }
    }
}
