package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.flow.Flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources keyboard input triggers from a window.
 */
public data object WindowKeyboardInputSource : InputSource<Boolean> {
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Boolean>, Boolean>> {
        return window.keyFlow
    }
}