package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.flow.Flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources mouse position input triggers from a window.
 */
public data object WindowMousePositionInputSource : InputSource<Float> {
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        return window.mousePositionFlow
    }
}
