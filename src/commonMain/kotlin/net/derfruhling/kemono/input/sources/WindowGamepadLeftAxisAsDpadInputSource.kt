package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window
import me.derfruhling.quantum.core.properties.Property

/**
 * Sources gamepad left stick axis input triggers from a window and converts
 * them into events that imitate a D-Pad.
 */
public class WindowGamepadLeftAxisAsDpadInputSource(
    deadZoneProperty: Property<Float> = Property.const(0.5f)
) : InputSource<Boolean>  {
    /**
     * The dead zone.
     * Any value that has less "significance" than this value will be discarded
     * and replaced with zero.
     * "Significance" is defined as [Float.absoluteValue].
     */
    public val deadZone: Float by deadZoneProperty

    private var previousX = 0.0f
    private var previousY = 0.0f

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Boolean>, Boolean>> {
        return window.gamepadAxesFlow.flatMapMerge { array ->
            val x = array[InputTrigger.GamepadAxis.LEFT_X.ordinal]
            val y = -array[InputTrigger.GamepadAxis.LEFT_Y.ordinal]

            flow {
                when {
                    x >= deadZone && previousX < deadZone -> emit(InputTrigger.GamepadButton.DPAD_RIGHT to true)
                    x < deadZone && previousX >= deadZone -> emit(InputTrigger.GamepadButton.DPAD_RIGHT to false)
                    x <= -deadZone && previousX > -deadZone -> emit(InputTrigger.GamepadButton.DPAD_LEFT to true)
                    x > -deadZone && previousX <= -deadZone -> emit(InputTrigger.GamepadButton.DPAD_LEFT to false)
                }

                when {
                    y >= deadZone && previousY < deadZone -> emit(InputTrigger.GamepadButton.DPAD_UP to true)
                    y < deadZone && previousY >= deadZone -> emit(InputTrigger.GamepadButton.DPAD_UP to false)
                    y <= -deadZone && previousY > -deadZone -> emit(InputTrigger.GamepadButton.DPAD_DOWN to true)
                    y > -deadZone && previousY <= -deadZone -> emit(InputTrigger.GamepadButton.DPAD_DOWN to false)
                }

                previousX = x
                previousY = y
            }
        }
    }

    public companion object : () -> WindowGamepadLeftAxisAsDpadInputSource {
        /**
         * Constructs a new default instance of this input source.
         */
        override fun invoke(): WindowGamepadLeftAxisAsDpadInputSource {
            return WindowGamepadLeftAxisAsDpadInputSource()
        }
    }
}
