/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flowOf
import net.derfruhling.kemono.input.AxisInputSource
import net.derfruhling.kemono.input.ButtonInputTrigger
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources keyboard input events from a window and converts them into axis
 * events based on a predefined set of directions that each key would represent.
 *
 * @property up Key to press for a `+`[Y][InputTrigger.KeyboardAxis.Y] value.
 * @property down Key to press for a `-`[Y][InputTrigger.KeyboardAxis.Y] value.
 * @property right Key to press for a `+`[X][InputTrigger.KeyboardAxis.X] value.
 * @property left Key to press for a `-`[X][InputTrigger.KeyboardAxis.X] value.
 * @property spec A specification of the type of [InputTrigger.CustomAxisSpec]
 * to use.
 * This can be any implementation of [InputTrigger.CustomAxisSpec], all this is
 * used for is to determine what triggers to output.
 * The default is [InputTrigger.KeyboardAxis].
 */
public class WindowKeyboardVectorInputSource(
    public val up: ButtonInputTrigger,
    public val left: ButtonInputTrigger,
    public val down: ButtonInputTrigger,
    public val right: ButtonInputTrigger,
    public val spec: InputTrigger.CustomAxisSpec<Float> = InputTrigger.KeyboardAxis,
) : AxisInputSource {
    private val triggers = listOf(up, left, down, right)
        .distinct()
        .map { unwrap(it) }

    private val currentState = mutableMapOf(
        unwrap(up) to false,
        unwrap(left) to false,
        unwrap(down) to false,
        unwrap(right) to false
    )

    private fun Map<InputTrigger.Key, Boolean>.intoXScalar(): Float {
        var float = 0f
        if(resolve(left)) float -= 1f
        if(resolve(right)) float += 1f
        return float
    }

    private fun Map<InputTrigger.Key, Boolean>.intoYScalar(): Float {
        var float = 0f
        if(resolve(down)) float -= 1f
        if(resolve(up)) float += 1f
        return float
    }

    private fun Map<InputTrigger.Key, Boolean>.resolve(trigger: ButtonInputTrigger): Boolean {
        return when(trigger) {
            is InputTrigger.Special.Invert -> !resolve(trigger.trigger)
            else -> this[trigger as InputTrigger.Key]!!
        }
    }

    private fun unwrap(trigger: ButtonInputTrigger): InputTrigger.Key {
        return when(trigger) {
            is InputTrigger.Special.Invert -> unwrap(trigger.trigger)
            else -> trigger as? InputTrigger.Key
                ?: throw IllegalStateException("All triggers for ${this::class} must unwrap to InputTrigger.Key")
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        return window.keyFlow
            .filter { (key, _) -> key in triggers }
            .flatMapMerge { (key, newState) ->
                currentState[key] = newState

                flowOf(
                    spec.x to currentState.intoXScalar(),
                    spec.y to currentState.intoYScalar()
                )
            }
    }

    public companion object : () -> WindowKeyboardVectorInputSource {
        /**
         * Constructs a new default instance of this input source.
         * The instance will use W, A, S, and D as its keys.
         * This assumes QWERTY.
         */
        override fun invoke(): WindowKeyboardVectorInputSource = wasd()

        /**
         * Constructs a new instance of this input source.
         * The instance will use W, A, S, and D as its keys.
         * This assumes QWERTY.
         */
        public fun wasd(spec: InputTrigger.CustomAxisSpec<Float> = InputTrigger.KeyboardAxis): WindowKeyboardVectorInputSource =
            WindowKeyboardVectorInputSource(
                InputTrigger.Key.W,
                InputTrigger.Key.A,
                InputTrigger.Key.S,
                InputTrigger.Key.D,
                spec
            )

        /**
         * Constructs a new default instance of this input source.
         * The instance will use I, J, K, and L as its keys.
         * This assumes QWERTY.
         */
        public fun ijkl(spec: InputTrigger.CustomAxisSpec<Float> = InputTrigger.KeyboardAxis): WindowKeyboardVectorInputSource =
            WindowKeyboardVectorInputSource(
                InputTrigger.Key.I,
                InputTrigger.Key.J,
                InputTrigger.Key.K,
                InputTrigger.Key.L,
                spec
            )

        /**
         * Constructs a new default instance of this input source.
         * The instance will use the arrow keys as its keys.
         * This assumes QWERTY.
         */
        public fun arrowKeys(spec: InputTrigger.CustomAxisSpec<Float> = InputTrigger.KeyboardAxis): WindowKeyboardVectorInputSource =
            WindowKeyboardVectorInputSource(
                InputTrigger.Key.UP,
                InputTrigger.Key.LEFT,
                InputTrigger.Key.DOWN,
                InputTrigger.Key.RIGHT,
                spec
            )
    }
}