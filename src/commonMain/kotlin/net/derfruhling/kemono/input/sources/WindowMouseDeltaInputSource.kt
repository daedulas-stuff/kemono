package net.derfruhling.kemono.input.sources

import kotlinx.coroutines.flow.Flow
import net.derfruhling.kemono.input.InputSource
import net.derfruhling.kemono.input.InputTrigger
import net.derfruhling.kemono.platform.Window

/**
 * Sources mouse delta input events from a window.
 * Enables it if it is not already enabled.
 *
 * @throws IllegalStateException The window does not support mouse delta
 * (thrown in [flowFor], this @throws is placed here for visibility)
 */
public data object WindowMouseDeltaInputSource : InputSource<Float> {
    override fun flowFor(window: Window): Flow<Pair<InputTrigger<Float>, Float>> {
        if(!window.isMouseDeltaSupported) {
            throw IllegalStateException("Window does not support mouse delta")
        }

        if(!window.isMouseDeltaEnabled) {
            window.isMouseDeltaEnabled = true
        }

        return window.mouseDeltaFlow
    }
}
