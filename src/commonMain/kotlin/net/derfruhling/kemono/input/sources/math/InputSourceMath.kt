/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.sources.math

import net.derfruhling.kemono.input.AxisInputSource
import net.derfruhling.kemono.input.ButtonInputSource
import me.derfruhling.quantum.core.properties.Property

/*
 * Operators do not need documentation.
 */

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.plus(other: Float): AddingAxisInputSource {
    return AddingAxisInputSource(this, Property.const(other))
}

@Suppress("KDocMissingDocumentation")
public inline operator fun Float.plus(other: AxisInputSource): AddingAxisInputSource {
    return other + this
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.plus(other: Property<Float>): AddingAxisInputSource {
    return AddingAxisInputSource(this, other)
}

@Suppress("KDocMissingDocumentation")
public inline operator fun Property<Float>.plus(other: AxisInputSource): AddingAxisInputSource {
    return other + this
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.minus(other: Float): SubtractingAxisInputSource {
    return SubtractingAxisInputSource(this, Property.const(other))
}

@Suppress("KDocMissingDocumentation")
public operator fun Float.minus(other: AxisInputSource): SubtractedAxisInputSource {
    return SubtractedAxisInputSource(other, Property.const(this))
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.minus(other: Property<Float>): SubtractingAxisInputSource {
    return SubtractingAxisInputSource(this, other)
}

@Suppress("KDocMissingDocumentation")
public operator fun Property<Float>.minus(other: AxisInputSource): SubtractedAxisInputSource {
    return SubtractedAxisInputSource(other, this)
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.times(other: Float): MultiplyingAxisInputSource {
    return MultiplyingAxisInputSource(this, Property.const(other))
}

@Suppress("KDocMissingDocumentation")
public inline operator fun Float.times(other: AxisInputSource): MultiplyingAxisInputSource {
    return other * this
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.times(other: Property<Float>): MultiplyingAxisInputSource {
    return MultiplyingAxisInputSource(this, other)
}

@Suppress("KDocMissingDocumentation")
public inline operator fun Property<Float>.times(other: AxisInputSource): MultiplyingAxisInputSource {
    return other * this
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.div(other: Float): DividingAxisInputSource {
    return DividingAxisInputSource(this, Property.const(other))
}

@Suppress("KDocMissingDocumentation")
public operator fun Float.div(other: AxisInputSource): FractionalAxisInputSource {
    return FractionalAxisInputSource(other, Property.const(this))
}

@Suppress("KDocMissingDocumentation")
public operator fun AxisInputSource.div(other: Property<Float>): DividingAxisInputSource {
    return DividingAxisInputSource(this, other)
}

@Suppress("KDocMissingDocumentation")
public operator fun Property<Float>.div(other: AxisInputSource): FractionalAxisInputSource {
    return FractionalAxisInputSource(other, this)
}

@Suppress("KDocMissingDocumentation")
public operator fun ButtonInputSource.not(): InvertedButtonInputSource {
    return InvertedButtonInputSource(this)
}
