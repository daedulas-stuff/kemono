/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.actions

import net.derfruhling.kemono.input.AxisInputTrigger
import net.derfruhling.kemono.input.InputAcceptor
import net.derfruhling.kemono.math.Vector2
import kotlin.math.absoluteValue

/**
 * An input action that updates and keeps track of a vector and updates it
 * when any of its triggers are fired.
 */
public abstract class VectorInputAction :
    InputAction<Vector2<Float>>(Vector2(0f, 0f)), InputAcceptor<Float> {
    /**
     * Triggers that will update the `x` value.
     * The result will use the most significant value.
     *
     * @see Float.absoluteValue
     */
    public abstract val xTriggers: List<AxisInputTrigger>

    /**
     * Triggers that will update the `y` value.
     * The result will use the most significant value.
     *
     * @see Float.absoluteValue
     */
    public abstract val yTriggers: List<AxisInputTrigger>

    /**
     * All triggers of this action.
     * [xTriggers] and [yTriggers] combined.
     */
    public val triggers: List<AxisInputTrigger>
        get() = (xTriggers + yTriggers).distinct()

    private val triggerValues = mutableMapOf<AxisInputTrigger, Float>()

    override fun accept(trigger: AxisInputTrigger, newState: Float) {
        if(trigger in triggers) {
            triggerValues[trigger] = newState
            setState(Vector2(
                xTriggers
                    .mapNotNull { triggerValues[it] }
                    .maxByOrNull { it.absoluteValue } ?: 0f,
                yTriggers
                    .mapNotNull { triggerValues[it] }
                    .maxByOrNull { it.absoluteValue } ?: 0f,
            ))
        }
    }
}