/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.actions

import net.derfruhling.kemono.input.AxisInputTrigger
import net.derfruhling.kemono.input.InputAcceptor
import kotlin.math.absoluteValue

/**
 * An input action that accepts a [Float].
 */
public abstract class AxisInputAction :
    InputAction<Float>(0.0f), InputAcceptor<Float> {
    /**
     * The triggers that will trigger this action.
     */
    public abstract val triggers: List<AxisInputTrigger>

    private val triggerValues = mutableMapOf<AxisInputTrigger, Float>()

    override fun accept(trigger: AxisInputTrigger, newState: Float) {
        if(trigger in triggers) {
            triggerValues[trigger] = newState
            setState(triggerValues
                .filterKeys { triggers.contains(it) }
                .maxByOrNull { it.value.absoluteValue }?.value ?: 0f)
        }
    }
}