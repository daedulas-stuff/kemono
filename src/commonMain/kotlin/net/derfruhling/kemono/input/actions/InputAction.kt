/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.input.actions

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

/**
 * An input action that can be set.
 *
 * @see SimpleInputAction
 * @see AxisInputAction
 * @see VectorInputAction
 */
public open class InputAction<T>(initialValue: T) {
    private val _mutableState = MutableStateFlow(initialValue)

    /**
     * The current state of this action.
     */
    public val state: StateFlow<T> = _mutableState.asStateFlow()

    /**
     * Sets the current state of the action.
     *
     * @param value The value to set the state to.
     */
    public fun setState(value: T) {
        if(state.value == value) return
        _mutableState.value = value
    }
}