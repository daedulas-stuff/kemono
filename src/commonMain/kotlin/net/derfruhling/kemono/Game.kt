/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.onEach
import kotlinx.datetime.Clock
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.graphics.*
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.graphics.structures.UniformValue
import net.derfruhling.kemono.graphics.ui.Overlay
import net.derfruhling.kemono.graphics.ui.Screen
import net.derfruhling.kemono.input.InputService
import net.derfruhling.kemono.map.MapChunk
import net.derfruhling.kemono.map.MapService
import net.derfruhling.kemono.map.entities.EntityUniformToken
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.platform.PlatformService
import net.derfruhling.kemono.platform.Window
import net.derfruhling.kemono.resources.ResourceService
import net.derfruhling.kemono.resources.husks.ShaderResource.UniformAcceptValue.*
import net.derfruhling.kemono.savedata.SaveDataService
import net.derfruhling.kemono.services.ServiceContainer
import net.derfruhling.kemono.services.getServiceDelegate
import net.derfruhling.kemono.services.setServiceLoader
import net.derfruhling.kemono.text.LocaleService
import net.derfruhling.kemono.text.TextService
import me.derfruhling.quantum.core.async.ThreadId
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.interfaces.use
import me.derfruhling.quantum.core.log.Logger
import me.derfruhling.quantum.core.log.logger

/**
 * Specifies a game and it's top-level logic.
 */
public abstract class Game : Disposable {
    private val log by logger

    /**
     * The name to use when something on-disk needs to be referenced by this
     * game, and must be located, like save data.
     * Changing this changes the path of save data and should probably be
     * considered a breaking change.
     * You should also probably consider automatic migration from the old
     * directory.
     *
     * This value should differ by platform:
     * - On Windows, this should be in PascalCase (eg. ThisIsAnExample)
     * - On Linux, this should be kebab-case (eg. this-is-an-example)
     * - On macOS, this should be Upper Case With Spaces (eg. This Is An Example)
     *
     * This can be achieved with a simple when {} block.
     *
     * ```kotlin
     * override val referenceName: String = when {
     *     OperatingSystem.isWindows -> "ThisIsAnExample"
     *     OperatingSystem.isLinux -> "this-is-an-example"
     *     OperatingSystem.isMacOS -> "This Is An Example"
     *     else -> "this-is-an-example" // default to something acceptable
     * }
     * ```
     */
    public abstract val referenceName: String

    init {
        log.info("Starting game")
    }

    public val services: ServiceContainer = ServiceContainer.build {
        @OptIn(InternalAPI::class)
        setServiceLoader(this.asLoader)

        addService { this@Game }
        addService(::AsyncService)
        addService(::ResourceService)
        addService(::PlatformService)
        addService(::InputService)
        addService(::RenderService)
        addService(::MapService)
        addService(::SaveDataService)
        addService(::LocaleService)
        addService(::TextService)
    }

    public val async: AsyncService by services.getServiceDelegate()
    public val resource: ResourceService by services.getServiceDelegate()
    public val platform: PlatformService by services.getServiceDelegate()
    public val input: InputService by services.getServiceDelegate()
    public val render: RenderService by services.getServiceDelegate()
    public val map: MapService by services.getServiceDelegate()
    public val saveData: SaveDataService by services.getServiceDelegate()
    public val locale: LocaleService by services.getServiceDelegate()
    public val text: TextService by services.getServiceDelegate()

    override fun dispose() {
        services.dispose()
    }

    public val graphics: Graphics
        get() = render.graphics

    /**
     * This is the desired frame rate, in frames per second.
     */
    public open val desiredFrameRate: Int get() = 60

    private val frameTimeStream: FrameTimeStream = object : FrameTimeStream() {
        override val desiredFrameRate: Int
            get() = this@Game.desiredFrameRate
    }

    /**
     * This the amount of time compared to [desiredFrameRate] that the last
     * frame took.
     * This allows the game to adjust its actions based on the current frame
     * rate by multiplying additive values with this.
     */
    public var lastFrameTimeAvg: Float = frameTimeStream.next()
        private set

    public var currentScreen: Screen? = null
    public var currentOverlay: Overlay? = null

    /**
     * Renders the game.
     * Calls all render related functions in this game in the correct order.
     */
    public fun render() {
        if (ThreadId.current() != async.mainThread)
            throw IllegalStateException("render() must be called on main thread")

        globalPreRender()

        @OptIn(InternalAPI::class)
        async.endRenderThreadFrame()

        @OptIn(InternalAPI::class)
        async
            .acceptRenderThreadRunnables()
            .forEach { it.run() }

        execRender()
        globalPostRender()
    }

    /**
     * Updates the game state.
     * Supports asynchronous coroutines!
     *
     * @see execPreUpdate
     * @see execUpdate
     * @see execPostUpdate
     */
    public suspend fun update(): Unit = withContext(async.threadPool) {
        coroutineScope { execPreUpdate() }
        coroutineScope { execUpdate() }
        coroutineScope { execPostUpdate() }
    }

    /**
     * Executes the pre-update stage of the game.
     * This can be overriden, but it's probably more helpful to override
     * [globalPreUpdate].
     */
    protected open fun CoroutineScope.execPreUpdate() {
        launch {
            render.updatePools()

            // isn't it beautiful?
            launch { globalPreUpdate() }
            launch { map.preUpdate() }
        }
    }

    /**
     * Executes the update stage of the game.
     * This can be overriden, but it's probably more helpful to override
     * [globalUpdate].
     */
    protected open fun CoroutineScope.execUpdate() {
        launch { globalUpdate() }
        currentScreen?.let { launch { it.update() } }
            ?: run {
                launch { map.update() }
                currentOverlay?.let { launch { it.update() } }
            }
    }

    /**
     * Executes the post-update stage of the game.
     * This can be overriden, but it's probably more helpful to override
     * [globalPostUpdate].
     */
    protected open fun CoroutineScope.execPostUpdate() {
        launch { globalPostUpdate() }
        launch { map.postUpdate() }
    }

    /**
     * The global side of the pre-update stage in the game.
     * The default implementation tells [Window] to process gamepad events,
     * if a gamepad is set.
     */
    protected open suspend fun globalPreUpdate() {
        platform.primaryWindow.processGamepad()
    }

    /**
     * The global side of the update stage in the game.
     */
    protected open suspend fun globalUpdate() {}

    /**
     * The global side of the post-update stage in the game.
     */
    protected open suspend fun globalPostUpdate() {}

    /**
     * The global side of the pre-render stage in the game.
     * The default implementation clears the screen.
     */
    protected open fun globalPreRender() {
        graphics.clear()
    }

    /**
     * Executes the render stage in the game.
     *
     * @see globalPreRenderGame
     * @see globalPostRenderGame
     */
    protected open fun execRender() {
        globalPreRenderGame()
        if(currentScreen == null) {
            renderMap()

            if(currentOverlay != null) {
                renderOverlay()
            }
        } else {
            renderScreen()
        }

        globalPostRenderGame()
    }

    /**
     * Run in [execRender]'s default implementation before rendering the game.
     * Perform any additional preparation before each frame in here.
     */
    protected open fun globalPreRenderGame() {}

    /**
     * Run in [execRender]'s default implementation after rendering the game.
     * Perform any additional UI rendering or cleanup in here.
     */
    protected open fun globalPostRenderGame() {}

    /**
     * Renders the map if [map] is not null.
     */
    public open fun renderMap() {
        map.render()
    }

    public open fun renderScreen() {
        currentScreen!!.render()
    }

    public open fun renderOverlay() {
        currentOverlay!!.render()
    }

    /**
     * The global side of the post-render stage in the game.
     */
    protected open fun globalPostRender() {}

    /**
     * Run by [run] before everything else.
     */
    protected open suspend fun initialize() {
        services.initialize()
    }

    /**
     * Run by [run] after the game is closed.
     */
    protected open suspend fun tearDown() {}

    /**
     * Runs the game.
     */
    public fun run(): Unit = runBlocking {
        initialize()

        var now = Clock.System.now()
        platform.primaryWindow.isVisible = true

        while (!platform.primaryWindow.shouldClose) {
            platform.primaryWindow.pollEvents()

            update()
            render()

            platform.primaryWindow.swapBuffers()

            val end = Clock.System.now()

            frameTimeStream.put(end - now)
            lastFrameTimeAvg = frameTimeStream.next()

            now = end
        }

        platform.primaryWindow.isVisible = false
        tearDown()
    }
}

/* TODO eligible for moving to quantum-core */
public inline operator fun Regex.contains(string: String): Boolean = string.matches(this)

/**
 * Runs a game, from start to finish.
 */
public inline fun <T : Game> runGame(args: Array<String> = emptyArray(), crossinline fn: (List<String>) -> T) {
    val unrecognisedArgs = mutableListOf<String>()

    kmProgram({
        if (args.isNotEmpty()) {
            Logger.of("runGame").run {
                info("Running with arguments: {}", args.joinToString())
                warn("Using console arguments may not be supported by this game! Use caution!")
            }

            args.forEach {
                when (it) {
                    "--dbg-debug-shader-contents" -> dumpShaderContents = true
                    "--dbg-debug-buffer-contents" -> dumpBufferContents = true
                    "--dbg-debug-mesh-builder" -> debugMeshBuilder = true
                    "--dbg-debug-input" -> debugInput = true
                    "--dbg-debug-all" -> setAll()
                    else -> unrecognisedArgs.add(it)
                }
            }
        }
    }) {
        if (unrecognisedArgs.isNotEmpty()) {
            Logger.of("runGame").info("Passing unrecognised args to application: {}", unrecognisedArgs.joinToString())
        }

        fn(unrecognisedArgs).let { game ->
            gameScoped(game) {
                game.use { it.run() }
            }
        }
    }
}

/**
 * Configures a render object to include various uniform values selected from
 * the provided [game].
 * Some values returned are special and require the game to be in a certain
 * state, and some are also ignored here.
 *
 * See the implementation for more details.
 *
 * @receiver The [RenderObjectConfigurator].
 * @param game The relevant game
 */
public fun RenderObjectConfigurator.uniforms(game: Game) {
    (shader as? ShaderResource)?.boundUniforms?.forEach { (k, v) ->
        shader uniform k from when (v) {
            CAMERA_POSITION -> UniformValue.Special.CAMERA_POSITION
            ENTITY_POSITION -> EntityUniformToken.ENTITY_POSITION
            ENTITY_DIRECTION -> EntityUniformToken.ENTITY_DIRECTION
            CHUNK_POSITION -> return@forEach
            CHUNK_SIZE -> UniformValue.Vector2I(Vector2(MapChunk.SIZE, MapChunk.SIZE))

            UI_BOUNDS -> return@forEach

            RENDER_TARGET_BOUNDS -> UniformValue.Vector2I(game.platform.primaryWindow.framebufferSize)
            RENDER_TARGET_ASPECT_RATIO_W_OVER_H -> game.platform.primaryWindow.let { window ->
                UniformValue.Float(
                    window.framebufferSize.x.toFloat() /
                            window.framebufferSize.y.toFloat()
                )
            }

            RENDER_TARGET_ASPECT_RATIO_H_OVER_W -> game.platform.primaryWindow.let { window ->
                UniformValue.Float(
                    window.framebufferSize.y.toFloat() /
                            window.framebufferSize.x.toFloat()
                )
            }

            TILES_CENTER_TO_TOP -> UniformValue.Int(game.map.tilesToTop)
            TEXTURE_0 -> UniformValue.Int(0)
            TEXTURE_1 -> UniformValue.Int(1)
            TEXTURE_2 -> UniformValue.Int(2)
            TEXTURE_3 -> UniformValue.Int(3)
            TEXTURE_4 -> UniformValue.Int(4)
            TEXTURE_5 -> UniformValue.Int(5)
            TEXTURE_6 -> UniformValue.Int(6)
            TEXTURE_7 -> UniformValue.Int(7)
        }
    }
}
