/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.takeWhile
import net.derfruhling.kemono.annotations.InternalAPI
import me.derfruhling.quantum.core.async.ThreadId
import kotlin.coroutines.CoroutineContext

/**
 * Dispatches [Runnable]s to the render thread. Eventually.
 */
public class RenderThreadDispatcher {
    private val mainThreadId = ThreadId.current()
    private val channel = Channel<Runnable?>(Channel.UNLIMITED)

    public fun dispatch(runnable: Runnable) {
        channel.trySend(runnable)
    }

    /**
     * Sends an end-frame message over this dispatcher, causing the runner to
     * immediately stop executing and continue to the next stage.
     *
     * This should never be called by anything outside of [Game] itself.
     */
    @InternalAPI
    public fun endFrame() {
        channel.trySend(null)
    }

    public fun accept(): Runnable? {
        val result = channel.tryReceive()
        return result.getOrNull()
    }
}
