/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.savedata

import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import me.derfruhling.quantum.core.interfaces.Closeable
import me.derfruhling.quantum.core.log.Logger
import me.derfruhling.quantum.core.log.logger
import okio.FileSystem
import okio.buffer
import kotlin.reflect.KClass

public open class SaveDataService(
    override val serviceLoader: ServiceLoader
) : Service, Closeable {
    @PublishedApi
    internal val log: Logger by logger

    private val codecs = mutableMapOf<KClass<*>, SaveDataCodec<*>>()
    private val saves = mutableMapOf<KClass<*>, LoadedSave>()

    public var shouldSaveWhenDisposed: Boolean = true

    private data class LoadedSave(
        val codec: SaveDataCodec<*>,
        val reference: SaveDataFileReference,
        val value: Any
    )

    public fun <T : Any> registerSaveDataCodec(
        kClass: KClass<T>,
        codec: SaveDataCodec<T>
    ) {
        require(kClass !in codecs) { "Save data codec $kClass already registered" }

        codecs[kClass] = codec
    }

    public fun <T : Any> getSaveDataCodec(kClass: KClass<T>): SaveDataCodec<T> {
        require(kClass in codecs) { "Save data code $kClass not registered" }

        @Suppress("UNCHECKED_CAST")
        return codecs[kClass]!! as SaveDataCodec<T>
    }

    public inline fun <reified T : Any> registerSaveDataCodec(
        codec: SaveDataCodec<T>
    ) {
        registerSaveDataCodec(T::class, codec)
    }

    public inline fun <reified T : Any> getSaveDataCodec(): SaveDataCodec<T> {
        return getSaveDataCodec(T::class)
    }

    public fun <T : Any> getExistingSave(kClass: KClass<T>): T? {
        if(kClass in saves) {
            @Suppress("UNCHECKED_CAST")
            return saves[kClass]!!.value as T
        } else return null
    }

    public fun <T : Any> getExistingReference(kClass: KClass<T>): SaveDataFileReference? {
        return if(kClass in saves) {
            saves[kClass]!!.reference
        } else null
    }

    @InternalAPI
    public fun <T : Any> putExistingSave(kClass: KClass<T>, reference: SaveDataFileReference, value: T) {
        require(kClass !in saves) { "Save $kClass already loaded" }

        @Suppress("UNCHECKED_CAST")
        saves[kClass] = LoadedSave(getSaveDataCodec(kClass), reference, value)
    }

    public inline fun <reified T : Any> getSave(reference: SaveDataFileReference): T {
        getExistingSave(T::class)?.let { return it }

        val codec = getSaveDataCodec<T>()
        val path = reference.resolve(saveDataDir)

        if(!FileSystem.SYSTEM.exists(path)) {
            val value = codec.default()

            @OptIn(InternalAPI::class)
            putExistingSave(T::class, reference, value)

            return value
        } else {
            val bufferedSource = FileSystem.SYSTEM.source(path).buffer()

            val value = try {
                codec.decode(bufferedSource)
            } catch (e: SaveDecodingException) {
                log.error("Failed to load save file", e.cause!! as Exception)
                codec.default()
            }

            @OptIn(InternalAPI::class)
            putExistingSave(T::class, reference, value)

            return value
        }
    }

    private fun updateSave(kClass: KClass<Any>) {
        val value = getExistingSave(kClass)
            ?: throw IllegalStateException("No $kClass save loaded")
        val reference = getExistingReference(kClass)!!
        val codec = getSaveDataCodec(kClass)

        val path = reference.resolve(saveDataDir)
        if(!FileSystem.SYSTEM.exists(saveDataDir)) {
            FileSystem.SYSTEM.createDirectories(saveDataDir)
        }

        FileSystem.SYSTEM.write(path) {
            codec.encode(this, value)
        }
    }

    public inline fun <reified T : Any> updateSave() {
        val value = getExistingSave(T::class)
            ?: throw IllegalStateException("No ${T::class} save loaded")
        val reference = getExistingReference(T::class)!!
        val codec = getSaveDataCodec<T>()

        val path = reference.resolve(saveDataDir)
        if(!FileSystem.SYSTEM.exists(saveDataDir)) {
            FileSystem.SYSTEM.createDirectories(saveDataDir)
        }

        FileSystem.SYSTEM.write(path) {
            codec.encode(this, value)
        }
    }

    public fun <T : Any> unloadSave(kClass: KClass<T>) {
        saves.remove(kClass)
    }

    public inline fun <reified T : Any> unloadSave() {
        unloadSave(T::class)
    }

    public inline fun <reified T : Any> updateAndUnloadSave() {
        updateSave<T>()
        unloadSave<T>()
    }

    override fun close() {
        for ((key, _) in saves) {
            @Suppress("UNCHECKED_CAST")
            updateSave(key as KClass<Any>)
        }
    }
}
