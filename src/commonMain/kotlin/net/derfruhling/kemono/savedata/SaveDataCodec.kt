/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.savedata

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.Serializer
import okio.BufferedSink
import okio.BufferedSource

public interface SaveDataCodec<T> {
    public fun encode(
        sink: BufferedSink,
        value: T
    )

    public fun decode(
        source: BufferedSource
    ): T

    public fun default(): T

    public companion object {
        public inline fun <T> fromSerializer(
            serializationStrategy: SerializationStrategy<T>,
            deserializationStrategy: DeserializationStrategy<T>,
            crossinline default: () -> T
        ): SaveDataCodec<T> {
            return object : SerializableSaveDataCodec<T>(
                serializationStrategy,
                deserializationStrategy
            ) {
                override fun default(): T {
                    return default()
                }
            }
        }

        public inline fun <T> fromSerializer(
            serializer: KSerializer<T>,
            crossinline default: () -> T
        ): SaveDataCodec<T> {
            return fromSerializer(serializer, serializer, default)
        }
    }
}
