/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.services

import me.derfruhling.quantum.core.properties.Property
import kotlin.reflect.KClass

/**
 * Represents a container of services that can be queried.
 */
public interface ServiceLoader {
    public fun <T : Any> getService(kClass: KClass<T>): T
    public fun <T : Any> isServicePresent(kClass: KClass<T>): Boolean
}

public inline fun <reified T : Any> ServiceLoader.getService(): T {
    return getService(T::class)
}

public inline fun <reified T : Any> ServiceLoader.getOptionalService(): T? {
    if(!isServicePresent(T::class)) return null
    return getService(T::class)
}

public inline fun <reified T : Any> ServiceLoader.getServiceDelegate(): Property<T> {
    return Property.const {
        getService(T::class)
    }
}

public inline fun <reified T : Any> ServiceLoader.getOptionalServiceDelegate(): Property<T?> {
    return Property.const {
        if(!isServicePresent(T::class)) return@const null
        getService(T::class)
    }
}

public inline fun <reified T : Any> Component.service(): Property<T> {
    return Property.const {
        serviceLoader.getService(T::class)
    }
}

public inline fun <reified T : Any> Component.optionalService(): Property<T?> {
    return Property.const {
        if(!serviceLoader.isServicePresent(T::class)) return@const null
        serviceLoader.getService(T::class)
    }
}
