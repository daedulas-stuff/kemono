/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.services

import net.derfruhling.kemono.annotations.InternalAPI
import me.derfruhling.quantum.core.interfaces.Closeable
import me.derfruhling.quantum.core.interfaces.Disposable
import kotlin.reflect.KClass

public class ServiceContainer(
    private var map: Map<KClass<*>, Any>,
    private var orderedList: List<Any>
): ServiceLoader, Disposable {
    override fun <T : Any> getService(kClass: KClass<T>): T {
        if(kClass !in map) {
            throw IllegalStateException("No service of $kClass")
        }

        @Suppress("UNCHECKED_CAST")
        val service = map[kClass]!! as? T
            ?: throw IllegalStateException("Service $kClass has mis-matching real type")

        return service
    }

    override fun <T : Any> isServicePresent(kClass: KClass<T>): Boolean {
        return kClass in map
    }

    @InternalAPI
    public fun sanitize() {
        map = buildMap {
            for ((key, value) in map) put(key, value)
        }
    }

    public companion object {
        public inline fun build(fn: MutableServiceContainer.() -> Unit): ServiceContainer {
            return MutableServiceContainer().apply(fn).build()
        }
    }

    public fun initialize() {
        orderedList.forEach {
            if(it is Service) it.initialize()
        }
    }

    override fun dispose() {
        orderedList.reversed().forEach {
            if(it !is Service) return@forEach
            if(it is Disposable) it.dispose()
            if(it is Closeable) it.close()
        }
    }
}
