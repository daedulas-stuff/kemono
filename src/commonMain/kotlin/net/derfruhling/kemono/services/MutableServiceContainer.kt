/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.services

import net.derfruhling.kemono.annotations.InternalAPI
import kotlin.reflect.KClass

public class MutableServiceContainer {
    private val services = mutableMapOf<KClass<*>, Any>()
    private val servicesList = mutableListOf<Any>()
    private val container = ServiceContainer(services, servicesList)

    @InternalAPI
    public val asLoader: ServiceLoader get() = container

    @InternalAPI
    public fun createServiceEntry(
        kClass: KClass<*>,
        service: Any
    ) {
        require(kClass !in services) { "$kClass already registered as a service" }
        services[kClass] = service
        servicesList.add(service)
    }

    public fun build(): ServiceContainer {
        @OptIn(InternalAPI::class)
        return container.apply { sanitize() }
    }

    public inline fun <reified T : Any> addService(
        constructor: (ServiceLoader) -> T
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader))
    }

    public inline fun <
            reified T : Any,
            A1
    > addService(
        constructor: (ServiceLoader, A1) -> T,
        arg1: A1
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader, arg1))
    }

    public inline fun <
            reified T : Any,
            A1, A2
    > addService(
        constructor: (ServiceLoader, A1, A2) -> T,
        arg1: A1,
        arg2: A2
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader, arg1, arg2))
    }

    public inline fun <
            reified T : Any,
            A1, A2, A3
    > addService(
        constructor: (ServiceLoader, A1, A2, A3) -> T,
        arg1: A1,
        arg2: A2,
        arg3: A3
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader, arg1, arg2, arg3))
    }

    public inline fun <
            reified T : Any,
            A1, A2, A3, A4
    > addService(
        constructor: (ServiceLoader, A1, A2, A3, A4) -> T,
        arg1: A1,
        arg2: A2,
        arg3: A3,
        arg4: A4
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader, arg1, arg2, arg3, arg4))
    }

    public inline fun <
            reified T : Any,
            A1, A2, A3, A4, A5
    > addService(
        constructor: (ServiceLoader, A1, A2, A3, A4, A5) -> T,
        arg1: A1,
        arg2: A2,
        arg3: A3,
        arg4: A4,
        arg5: A5
    ) {
        @OptIn(InternalAPI::class)
        createServiceEntry(T::class, constructor(asLoader, arg1, arg2, arg3, arg4, arg5))
    }
}