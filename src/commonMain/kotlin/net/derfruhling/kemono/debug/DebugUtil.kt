/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.debug

import net.derfruhling.kemono.ThereIsNoSpoonException
import me.derfruhling.quantum.core.async.ThreadId
import me.derfruhling.quantum.core.log.logger

/**
 * Utilities for debugging without a real debugger.
 * Created because Idea's native debugging is not functional for me.
 */
public object DebugUtil {
    private val log by logger

    /**
     * Outputs the current thread to the log, along with a spoon for good
     * measure.
     *
     * @throws Nothing This function never throws.
     */
    public fun logCurrentThread() {
        log.debug("Current thread: {}", ThereIsNoSpoonException(), ThreadId.current())
    }
}
