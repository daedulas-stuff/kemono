/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.text

import net.derfruhling.kemono.resources.Language
import net.derfruhling.kemono.resources.ResourceService
import net.derfruhling.kemono.resources.husks.LocaleResource
import net.derfruhling.kemono.resources.read
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import net.derfruhling.kemono.services.service

public open class LocaleService(
    override val serviceLoader: ServiceLoader
) : Service {
    private val resourceService: ResourceService by service()

    private lateinit var languageResolver: (Language) -> Iterable<String>
    private val arbitraryLocales = mutableMapOf<String, Locale>()
    private var languageLocales = emptyList<Locale>()

    public fun loadArbitraryLocale(assetName: String) {
        if(assetName in arbitraryLocales) return
        val resource = resourceService.resourceStorage[assetName].read<LocaleResource>()
        arbitraryLocales[assetName] = Locale(resource)
    }

    public fun unloadArbitraryLocale(assetName: String) {
        arbitraryLocales.remove(assetName)
    }

    public fun setLanguageAssetResolver(fn: (Language) -> Iterable<String>) {
        languageResolver = fn
        updateLanguageLocales()
    }

    public var language: Language = Language.en_US
        set(value) {
            field = value
            updateLanguageLocales()
        }

    private fun updateLanguageLocales() {
        val assets = languageResolver(language)
        languageLocales = assets.map {
            Locale(resourceService.resourceStorage[it].read<LocaleResource>())
        }
    }

    public operator fun get(key: String): String? {
        for (locale in languageLocales + arbitraryLocales.values) {
            if(key in locale.texts) return locale.texts[key]!!
        }

        return null
    }
}