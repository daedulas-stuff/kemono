/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.text

import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.resources.ResourceService
import net.derfruhling.kemono.resources.husks.FontDefResource
import net.derfruhling.kemono.resources.read
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.dynamo.Dynamo
import me.derfruhling.quantum.core.dynamo.DynamoObject
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger

public open class TextService(
    override val serviceLoader: ServiceLoader
) : Service, Disposable, Dynamo {
    private val log by logger
    private val renderService: RenderService by service()
    private val resourceService: ResourceService by service()

    override val dyn: DynamoObject = DynamoObject()

    public lateinit var monospaceFont: FontMap private set

    public fun loadFont(assetName: String): FontMap {
        val fontDef = resourceService.resourceStorage[assetName].read<FontDefResource>()
        val texture = renderService.texturePool[fontDef.textureAssetName]
        return FontMap.Spritemap(texture, fontDef)
    }

    override fun initialize() {
        super.initialize()

        monospaceFont = loadFont("kemono:fonts/km_mono.fontdef")
    }

    override fun dispose() {
        runCatching { monospaceFont.dispose() }.onFailure { log.error("Failed to dispose monospace font", it as Exception) }
    }
}