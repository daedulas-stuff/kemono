/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.text

import net.derfruhling.kemono.graphics.NamedPool
import net.derfruhling.kemono.graphics.Spritesheet
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.interfaces.Texture2D
import net.derfruhling.kemono.math.Corners
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.resources.husks.FontDefResource
import me.derfruhling.quantum.core.interfaces.Disposable

public sealed class FontMap : Disposable {
    public abstract fun mapCharacter(char: Char): Corners<Vector2<Float>>?

    public class Spritemap internal constructor(
        private val texture: NamedPool.Ref<Texture>,
        fontDefResource: FontDefResource
    ) : FontMap() {
        public val spritesheet: Spritesheet = Spritesheet(
            texture.value as Texture2D,
            Vector2(
                fontDefResource.spritemapWidth,
                fontDefResource.spritemapHeight
            ),
            Vector2(
                fontDefResource.spritemapXOffset,
                fontDefResource.spritemapYOffset
            ),
            Vector2(
                fontDefResource.spritemapXIgnored,
                fontDefResource.spritemapYIgnored
            )
        )

        private val characters = fontDefResource.characters.associateBy { it.target }

        override fun mapCharacter(char: Char): Corners<Vector2<Float>>? {
            return characters[char]?.let { spritesheet[Vector2(it.x, it.y)] }
        }

        override fun dispose() {
            texture.dispose()
        }
    }
}