/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

@file:Suppress("KDocMissingDocumentation")

package net.derfruhling.kemono.graphics

import okio.Path

/**
 * Represents an OpenGL error.
 * Thrown by many graphics functions if it detects that an error occurred.
 */
public open class OpenGLException(message: String) : RuntimeException(message)

public class InvalidEnumGLException : OpenGLException("Invalid enum")
public class InvalidValueGLException : OpenGLException("Invalid value")
public class InvalidOperationGLException : OpenGLException("Invalid operation")
public class StackOverflowGLException : OpenGLException("Stack overflow")
public class StackUnderflowGLException : OpenGLException("Stack underflow")
public class GPUOutOfMemoryError : OutOfMemoryError("OpenGL: GPU out of memory; cannot continue")
public class InvalidFramebufferGLException : OpenGLException("Invalid framebuffer")
public class ContextLostGLException : OpenGLException("Context lost")

/**
 * Thrown by [Graphics.createShaderPartFromRawSource] (and by extension,
 * [Graphics.createShaderPart]) when the shader part fails to compile.
 *
 * @property log The compiler log that was produced. Maybe empty.
 */
public class ShaderCompileException(
    shaderPart: UInt,
    dumpedSourcePath: Path,
    public val log: String
) : RuntimeException("shader part $shaderPart failed to compile; see log above " +
        "(dumped source to $dumpedSourcePath)")

/**
 * Thrown by [Graphics.createShader] when the shader fails to link.
 *
 * @property log The link log that was produced. Maybe empty.
 */
public class ShaderLinkException(
    shader: UInt,
    public val log: String
) : RuntimeException("shader $shader failed to link; see log above")
