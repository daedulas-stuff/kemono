/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.structures

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.interfaces.Shader
import net.derfruhling.kemono.graphics.interfaces.ShaderUniform
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4
import me.derfruhling.quantum.core.properties.Property
import kotlin.Float as KFloat
import kotlin.Int as KInt

/**
 * Represents an arbitrary uniform value.
 *
 * @see Float
 * @see Vector2F
 * @see Vector3F
 * @see Vector4F
 * @see Int
 * @see Vector2I
 * @see Vector3I
 * @see Vector4I
 * @see FromProperty
 * @see Special
 * @see Token
 * @see Custom
 */
public sealed interface UniformValue {
    @Suppress("KDocMissingDocumentation")
    public value class Float(public val value: KFloat) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector2F(public val value: Vector2<KFloat>) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector3F(public val value: Vector3<KFloat>) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector4F(public val value: Vector4<KFloat>) : UniformValue


    @Suppress("KDocMissingDocumentation")
    public value class Int(public val value: KInt) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector2I(public val value: Vector2<KInt>) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector3I(public val value: Vector3<KInt>) : UniformValue

    @Suppress("KDocMissingDocumentation")
    public value class Vector4I(public val value: Vector4<KInt>) : UniformValue


    /**
     * Sets the uniform to the value read from [property] when a render object
     * is drawn.
     *
     * @property property The property to read.
     * It is unsafe to throw an exception in its getter.
     */
    public value class FromProperty(public val property: Property<UniformValue>) : UniformValue

    /**
     * Represents a special uniform value that will automagically resolve to
     * something when a render object is being drawn.
     */
    public enum class Special : UniformValue {
        /**
         * The current camera position.
         * Uniform expected to be `vec2`.
         */
        CAMERA_POSITION
    }

    /**
     * Represents a token that is resolved by a render object context block on
     * a draw call.
     *
     * Implementors are usually enum classes.
     *
     * @see Custom
     */
    public interface Token : UniformValue

    /**
     * Represents a uniform value that is resolved on its own when a render
     * object is drawn.
     *
     * @see Token
     */
    public fun interface Custom : UniformValue {
        /**
         * Not required. Used to make logging nicer.
         */
        public val isNull: Boolean get() = false

        /**
         * Sets this uniform's value.
         *
         * @param shader The shader of the uniform to set.
         * @param uniform The uniform to set.
         *
         * @throws Nothing It is unsafe to throw here.
         */
        @RenderFun
        public fun set(shader: Shader, uniform: ShaderUniform)
    }
}

/**
 * Sets [uniform] to [value], selecting the correct real function based on the
 * subtype of [UniformValue].
 *
 * @throws IllegalStateException [value] is [UniformValue.Special] or
 * [UniformValue.Token].
 * These values cannot be resolved by this function, and must be resolved
 * elsewhere.
 */
@RenderFun
public fun Shader.setUniform(uniform: ShaderUniform, value: UniformValue) {
    when (value) {
        is UniformValue.Float -> setUniform(uniform, value.value)
        is UniformValue.Int -> setUniform(uniform, value.value)
        is UniformValue.Vector2F -> setUniform(uniform, value.value)
        is UniformValue.Vector2I -> setUniform(uniform, value.value)
        is UniformValue.Vector3F -> setUniform(uniform, value.value)
        is UniformValue.Vector3I -> setUniform(uniform, value.value)
        is UniformValue.Vector4F -> setUniform(uniform, value.value)
        is UniformValue.Vector4I -> setUniform(uniform, value.value)
        is UniformValue.FromProperty -> setUniform(uniform, value.property.value)
        is UniformValue.Special -> throw IllegalArgumentException("Cannot set special uniform")
        is UniformValue.Token -> throw IllegalArgumentException("Cannot set token uniform")
        is UniformValue.Custom -> value.set(this, uniform)
    }
}
