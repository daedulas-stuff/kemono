/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.enums.EdgeMode
import net.derfruhling.kemono.graphics.enums.MagnifyFilter
import net.derfruhling.kemono.graphics.enums.MinifyFilter
import net.derfruhling.kemono.math.Vector4

/**
 * Allows configuring a [Texture].
 */
public interface TextureConfigurator {
    /** @see Texture.minFilter */
    public var minFilter: MinifyFilter

    /** @see Texture.magFilter */
    public var magFilter: MagnifyFilter

    /** @see Texture.sEdgeMode */
    public var sEdgeMode: EdgeMode

    /** @see Texture.tEdgeMode */
    public var tEdgeMode: EdgeMode

    /** @see Texture.rEdgeMode */
    public var rEdgeMode: EdgeMode

    /** @see Texture.borderColor */
    public var borderColor: Vector4<Float>?
}
