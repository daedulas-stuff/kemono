/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

/**
 * Inside the [fn] block, the [resources] are guaranteed to be bound.
 * The block also represent a single stack of definitely bound status for
 * [resources].
 *
 * @see DefinitelyBindable
 *
 * @param resources The resources to definitely bind.
 * It is not an error if they are already definitely bound.
 * @param fn The function block to execute while the [resources] are definitely
 * bound.
 * Returns [R], which could be [Unit].
 *
 * @return [fn()][fn]
 *
 * @throws Nothing This function itself does not throw.
 * **However,** the [fn] block may throw an exception, and [withDefinitelyBound]
 * will propagate that exception.
 */
public inline fun <R> withDefinitelyBound(
    vararg resources: DefinitelyBindable,
    fn: () -> R
): R {
    return try {
        resources.forEach { r ->
            if (!r.isDefinitelyBound) r.ensureBound()
            r.stackDefinitelyBoundStatus()
        }

        fn()
    } finally {
        resources.forEach { r ->
            r.removeDefinitelyBoundStatus()
        }
    }
}