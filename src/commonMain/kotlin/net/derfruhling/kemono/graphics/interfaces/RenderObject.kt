/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.structures.UniformValue

/**
 * Specifies a render object that contains all necessary information to render
 * using a [Shader], vertex [Buffer], index [Buffer] (optionally), and a set
 * of uniform values.
 *
 * `Graphics.draw` also allows specifying custom [UniformValue.Token] values.
 */
public interface RenderObject : GraphicsResource, DefinitelyBindable {
    /**
     * The shader used for rendering.
     */
    public val shader: Shader

    /**
     * The [Buffer] containing vertices.
     */
    public val vertexBuffer: Buffer

    /**
     * The number of vertices in [vertexBuffer].
     * Unused if [indexBuffer] and [indexCount] are not `null`
     */
    public val vertexCount: Int

    /**
     * The [Buffer] containing indices into [vertexBuffer].
     *
     * If this and [indexCount] are not `null`, `Graphics.draw` will use
     * index-buffer rendering.
     */
    public val indexBuffer: Buffer?

    /**
     * The number of indices in [indexBuffer].
     */
    public val indexCount: Int?

    /**
     * A set of [UniformValue]s to be set when this [RenderObject] is drawn.
     */
    public val uniformValues: Map<ShaderUniform, UniformValue>


    /**
     * Modifies this [RenderObject] with [fn].
     * The object's properties are specified to be the same unless they are
     * specifically changed by [fn].
     *
     * @param fn The configurator block.
     */
    public fun edit(fn: RenderObjectConfigurator.() -> Unit)
}

