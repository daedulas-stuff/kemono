/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.structures.UniformValue
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4
import me.derfruhling.quantum.core.dsl.DslIndexSet
import me.derfruhling.quantum.core.properties.Property

/**
 * Allows configuring a [RenderObject]
 */
public interface RenderObjectConfigurator {
    /** @see RenderObject.shader */
    public var shader: Shader

    /** @see RenderObject.vertexBuffer */
    public var vertexBuffer: Buffer

    /** @see RenderObject.vertexCount */
    public var vertexCount: Int

    /** @see RenderObject.indexBuffer */
    public var indexBuffer: Buffer?

    /** @see RenderObject.indexCount */
    public var indexCount: Int?

    /** @see RenderObject.uniformValues */
    public val uniformValues: DslIndexSet<ShaderUniform, UniformValue>

    /**
     * Sets the receiver uniform to [value].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: UniformValue) {
        uniformValues[this] = value
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Int) {
        uniformValues[this] = UniformValue.Int(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector2<Int>) {
        uniformValues[this] = UniformValue.Vector2I(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector3<Int>) {
        uniformValues[this] = UniformValue.Vector3I(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector4<Int>) {
        uniformValues[this] = UniformValue.Vector4I(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Float) {
        uniformValues[this] = UniformValue.Float(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector2<Float>) {
        uniformValues[this] = UniformValue.Vector2F(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector3<Float>) {
        uniformValues[this] = UniformValue.Vector3F(value)
    }

    /**
     * Sets the receiver uniform to [value], wrapping in as [UniformValue].
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(value: Vector4<Float>) {
        uniformValues[this] = UniformValue.Vector4F(value)
    }

    /**
     * Sets the receiver uniform to [property], which is read at every time
     * the render object is drawn.
     *
     * @see uniformValues
     */
    public infix fun ShaderUniform.from(property: Property<UniformValue>) {
        uniformValues[this] = UniformValue.FromProperty(property)
    }
}