/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.resources.husks.ShaderResource
import net.derfruhling.kemono.mesh.Vertex

/**
 * A complete shader resource that includes extra metadata that was loaded
 * from a real shader resource file or provided from an arbitrary husk.
 *
 * The shader resource is more capable than a regular shader as all the
 * information to construct a valid vertex buffer, outputs, and uniforms
 * are included in it.
 *
 * Many things may only accept shader resources rather than just shaders.
 */
public interface ShaderResource : Shader {
    /**
     * The shader that this resource is based on.
     */
    public val shader: Shader

    /**
     * Bound inputs.
     * The values of this map represent values in [Vertex].
     */
    public val boundInputs: Map<ShaderInput, Array<ShaderResource.InputValueSource>>

    /**
     * Bound outputs.
     * The values of this map represent the color location of the output.
     */
    public val boundOutputs: Map<ShaderOutput, Int>

    /**
     * Bound uniforms.
     * These uniforms are sourced from [ShaderResource.UniformAcceptValue]
     * and set when a [RenderObject] using this resource is drawn.
     */
    public val boundUniforms: Map<String, ShaderResource.UniformAcceptValue>

    /**
     * Gets all uniforms that accept [acceptValue].
     * This function may return multiple uniforms because it is perfectly valid
     * to have many uniforms that are set to the same value, although that is
     * inefficient.
     *
     * @param acceptValue The acceptValue
     * @return All uniforms accepting [acceptValue].
     * This may be empty if there are no uniforms accepting [acceptValue].
     */
    public fun uniforms(acceptValue: ShaderResource.UniformAcceptValue): List<ShaderUniform> =
        boundUniforms.toList()
            .filter { it.second == acceptValue }
            .mapNotNull { uniforms[it.first] }
}
