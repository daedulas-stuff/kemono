/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.OpenGLException
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * A base interface that all bindable graphics resources implement.
 * For resources that are not to be bound, directly implement [Disposable]
 * instead.
 */
public interface GraphicsResource : Disposable {
    /**
     * Ensures that this resource is currently bound.
     *
     * This function is not guaranteed to actually bind the resource, but the
     * resulting state must be that the resource is bound to an acceptable and
     * consistent target.
     *
     * @throws OpenGLException If an OpenGL error occurs.
     */
    @RenderFun
    @Throws(OpenGLException::class)
    public fun ensureBound()

    override fun dispose()
}
