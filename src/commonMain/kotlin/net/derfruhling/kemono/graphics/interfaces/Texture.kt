/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.enums.*
import net.derfruhling.kemono.math.Vector4

/**
 * Represents a texture.
 */
public interface Texture : GraphicsResource, DefinitelyBindable {
    // Formats

    /**
     * The internal texture format.
     * Not exposed to the program itself, see [textureFormat] for that.
     */
    public val internalTextureFormat: InternalTextureFormat

    /**
     * The format exposed to the texture.
     */
    public val textureFormat: TextureFormat

    /**
     * The value type of each component in the pixels.
     */
    public val valueType: ValueType

    // Filters

    /**
     * Minify filter.
     * When the texture is rendered smaller than it really is, this filter is
     * applied to determine how the texture is rendered.
     * The minify filter also supports mipmaps.
     */
    public val minFilter: MinifyFilter

    /**
     * Minify filter.
     * When the texture is rendered larger than it really is, this filter is
     * applied to determine how the texture is rendered.
     */
    public val magFilter: MagnifyFilter

    // Edge modes

    /**
     * The edge mode for the S dimension, which is equivalent to X in other
     * contexts.
     */
    public val sEdgeMode: EdgeMode

    /**
     * The edge mode for the T dimension, which is equivalent to Y in other
     * contexts.
     */
    public val tEdgeMode: EdgeMode

    /**
     * The edge mode for the R dimension, which is equivalent to Z in other
     * contexts.
     */
    public val rEdgeMode: EdgeMode

    /**
     * The border color used by the [EdgeMode.CLAMP_BORDER_COLOR] if any
     * edge mode requires it.
     */
    public val borderColor: Vector4<Float>?

    /**
     * Configures the texture.
     * Allows setting the various parameters of the texture.
     */
    public fun configure(fn: TextureConfigurator.() -> Unit)
}