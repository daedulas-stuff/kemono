/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4

/**
 * A complete shader.
 * Can be used to render and can be introspected to find out information
 * about the shader.
 */
public interface Shader : GraphicsResource, DefinitelyBindable {
    /**
     * The parts this shader consists of.
     */
    public val parts: Map<ShaderPartStage, ShaderPart>

    /**
     * The inputs this shader accepts.
     * Most or all of these should be set in a vertex buffer.
     */
    public val inputs: Map<String, ShaderInput>

    /**
     * The uniforms that the shader supports.
     * The name should not be used to identify what value a uniform should
     * be assigned.
     * See [ShaderResource] for that.
     */
    public val uniforms: Map<String, ShaderUniform>

    /**
     * The ordering of the inputs.
     */
    public val inputOrder: Iterable<ShaderInput>?

    /**
     * Gets an output from this shader by name.
     * Outputs cannot be introspected at scale.
     *
     * @param name The name of the output.
     * @return The [ShaderOutput]
     *
     * @throws Nothing This function always succeeds; though that does _not_
     * mean that the output is valid.
     */
    public fun getOutput(name: String): ShaderOutput

    /**
     * Gets an input from this shader by name.
     *
     * Names must be full.
     * Names like:
     *
     * ```
     * vPosition // valid -> in <type> vPosition;
     * vInputs.offset // valid -> in struct { <type> offset; } vInputs;
     * vTextureNums[7] // valid -> in int vTextures[16]; // @ index [7]
     * ```
     *
     * @param name The full name of the input.
     * This must include any parent structures and array indices.
     * @return [inputs][[name]]
     *
     * @throws IllegalArgumentException The input is not present in [inputs].
     */
    public infix fun input(name: String): ShaderInput = inputs[name]
        ?: throw IllegalArgumentException("shader input with name '$name' not found")

    /**
     * Alias of [getOutput].
     */
    public infix fun output(name: String): ShaderOutput = getOutput(name)

    /**
     * Gets a uniform from this shader by name.
     *
     * Names must be full.
     * Names like:
     *
     * ```
     * vChunkPosition // valid -> uniform <type> vChunkPosition;
     * vUniforms.position // valid -> uniform struct { <type> position; } vUniforms;
     * vTextures[7] // valid -> uniform sampler2D vTextures[16]; // @ index [7]
     * ```
     *
     * @param name The full name of the uniform.
     * This must include any parent structures and array indices.
     * @return [uniforms][[name]]
     *
     * @throws IllegalArgumentException The uniform is not present in
     * [uniforms].
     */
    public infix fun uniform(name: String): ShaderUniform = uniforms[name]
        ?: throw IllegalArgumentException("shader uniform with name '$name' not found (it may have been optimised out)")

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Int)

    /** Sets [uniform] to `ivec2`([x], [y]) */
    public fun setUniform(uniform: ShaderUniform, x: Int, y: Int)

    /** Sets [uniform] to `ivec3`([x], [y], [z]) */
    public fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int)

    /** Sets [uniform] to `ivec4`([x], [y], [z], [w]) */
    public fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int, w: Int)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Float)

    /** Sets [uniform] to `vec2`([x], [y]) */
    public fun setUniform(uniform: ShaderUniform, x: Float, y: Float)

    /** Sets [uniform] to `vec3`([x], [y], [z]) */
    public fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float)

    /** Sets [uniform] to `vec4`([x], [y], [z], [w]) */
    public fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float, w: Float)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector2<Int>)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector3<Int>)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector4<Int>)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector2<Float>)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector3<Float>)

    /** Sets [uniform] to [x] */
    public fun setUniform(uniform: ShaderUniform, x: Vector4<Float>)
}