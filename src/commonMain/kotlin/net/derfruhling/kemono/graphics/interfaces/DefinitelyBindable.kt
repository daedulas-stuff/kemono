/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

/**
 * Represents a resource that is bindable for a definite and known period of
 * time.
 * If the object is already known to be bound, the checks and actual binding
 * of the object are guaranteed to be skipped for almost all cases.
 * However, some functions will do their own thing for various reasons.
 */
public interface DefinitelyBindable : GraphicsResource {
    /**
     * If this property returns `true`, this resource is "definitely bound."
     * Being "definitely bound" is not the same as being bound, and
     * [GraphicsResource.ensureBound] does not update this value.
     */
    public val isDefinitelyBound: Boolean

    /**
     * The number of "stacks" this definitely bindable resource has been
     * definitely bound for.
     * This exists to ensure that if an object that is already definitely bound
     * is definitely bound inside a definitely bound block, the object does not
     * lose its definitely bound status early.
     */
    public val definitelyBoundStacks: Int

    /**
     * Adds a new definitely bound stack.
     * Please do not call this directly.
     *
     * @see definitelyBoundStacks
     *
     * @throws Nothing This function does not throw.
     */
    public fun stackDefinitelyBoundStatus()

    /**
     * Removes a definitely bound stack.
     *
     * @see definitelyBoundStacks
     *
     * @throws Nothing This function does not throw.
     */
    public fun removeDefinitelyBoundStatus()
}

