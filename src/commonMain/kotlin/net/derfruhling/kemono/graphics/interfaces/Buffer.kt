/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.Game
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.enums.BufferUpdatePurpose
import net.derfruhling.kemono.graphics.enums.BufferUpdateRate
import okio.Buffer as OkioBuffer

/**
 * Represents a buffer stored on the GPU.
 * The buffer has a certain [type] and [size], and can be mutated at any time.
 * This class's functions should only be accessed from the render thread.
 * Use [Game.runOnRenderThread] if you need to.
 */
public interface Buffer : GraphicsResource, DefinitelyBindable {
    /**
     * The size of this buffer, in bytes.
     * May be `0` if the buffer has not yet been written.
     */
    public val size: Int

    /**
     * The type of the buffer.
     * Certain operations require certain types of buffer, and the different
     * types should not be mixed and matched.
     */
    public val type: BufferType

    /**
     * Writes new contents to this buffer.
     *
     * Note the [@RenderFun][RenderFun] attribute!
     *
     * @param buffer An [OkioBuffer] containing the bytes to write to the
     * GPU buffer.
     * @param updateRate The predicted rate at which this buffer is to be
     * updated in the future.
     * Passed to the GPU for potential optimization.
     * @param updatePurpose The purpose of the buffer; how it will be used.
     * This should almost always be [BufferUpdatePurpose.DRAW].
     * Passed to the GPU for potential optimization.
     */
    @RenderFun
    public fun write(
        buffer: OkioBuffer,
        updateRate: BufferUpdateRate = BufferUpdateRate.STATIC,
        updatePurpose: BufferUpdatePurpose = BufferUpdatePurpose.DRAW
    )

    /**
     * Transitions this buffer to a new type.
     * The buffer will be unbound from its old type before the type is set,
     * but the buffer will still retain its data.
     *
     * Note the [@RenderFun][RenderFun] attribute!
     *
     * @param type The new [BufferType] for this buffer.
     */
    @RenderFun
    public fun transitionBuffer(
        type: BufferType
    )
}