/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.enums.ValueType

/**
 * Represents an input accepted by a shader.
 */
public interface ShaderInput {
    /**
     * The name of the input.
     */
    public val name: String

    /**
     * The type of the input.
     * This is a singular type.
     * Vectors are represented with [size]`s` greater than `1`.
     */
    public val type: ValueType

    /**
     * The number of values in this object.
     * If this is greater than one, this is a vector type.
     */
    public val size: Int

    /**
     * The internal location of the input.
     */
    public val location: Int
}
