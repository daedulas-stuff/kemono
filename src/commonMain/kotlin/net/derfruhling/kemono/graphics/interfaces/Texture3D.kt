/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.interfaces

import net.derfruhling.kemono.graphics.enums.InternalTextureFormat
import net.derfruhling.kemono.graphics.enums.TextureFormat
import net.derfruhling.kemono.graphics.enums.ValueType
import okio.Buffer

/**
 * Represents a 3D texture.
 */
public interface Texture3D : Texture {
    /**
     * Width of the texture, in pixels.
     */
    public val width: Int

    /**
     * Height of the texture, in pixels.
     */
    public val height: Int

    /**
     * Depth of the texture, in pixels.
     */
    public val depth: Int

    /**
     * Completely overwrites this texture.
     *
     * @param width The width of the texture.
     * @param height The height of the texture.
     * @param depth The depth of the texture.
     * @param internalTextureFormat The format to store the texture in
     * internally.
     * @param textureFormat The format of [buffer].
     * @param valueType The type of each component in a pixel in [buffer].
     * @param buffer The data.
     * Expected to be [width] *
     * [height] *
     * [depth] *
     * [textureFormat]'s length *
     * [valueType]'s byte size.
     * The data is arranged as a sequence of 2D texture layers, which
     * themselves are sequences of rows.
     */
    public fun write(
        width: Int,
        height: Int,
        depth: Int,
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    )

    /**
     * Writes data to a part of this texture.
     *
     * @param x The `x` offset to write into.
     * @param y The `y` offset to write into.
     * @param z The `z` offset to write into.
     * @param width The width of the partial texture to write.
     * @param height The height of the partial texture to write.
     * @param depth The depth of the partial texture to write.
     * @param textureFormat The format of [buffer].
     * @param valueType The type of each component in a pixel in [buffer].
     * @param buffer The data.
     * Expected to be [width] *
     * [height] *
     * [depth] *
     * [textureFormat]'s length *
     * [valueType]'s byte size.
     * The data is arranged as a sequence of 2D texture layers, which
     * themselves are sequences of rows.
     */
    public fun writePartial(
        x: Int,
        y: Int,
        z: Int,
        width: Int,
        height: Int,
        depth: Int,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    )
}