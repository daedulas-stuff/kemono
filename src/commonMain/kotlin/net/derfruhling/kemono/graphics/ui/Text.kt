/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.ui

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.NamedPool
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.graphics.interfaces.RenderObject
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.math.div
import net.derfruhling.kemono.mesh.*
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import net.derfruhling.kemono.text.FontMap
import me.derfruhling.quantum.core.interfaces.Disposable

public class Text(
    string: String,
    public val shaderRef: NamedPool.Ref<ShaderResource>,
    public val mesh: Mesh,
    public val renderObject: RenderObject,
    public val fontMap: FontMap
) : Disposable, Component {
    @set:RenderFun
    public var string: String = string
        set(value) {
            update(value)
            field = value
        }

    private val renderService: RenderService by service()

    override fun dispose() {
        renderObject.dispose()
        shaderRef.dispose()
        mesh.dispose()
    }

    @RenderFun
    public fun render() {
        renderService.graphics.draw(renderObject)
    }

    @RenderFun
    private fun update(string: String) {
        when(fontMap) {
            is FontMap.Spritemap -> {
                mesh.rebuild {
                    val (width, height) =
                        fontMap.spritesheet.virtualSize /
                        fontMap.spritesheet.spriteCount

                    buildText(
                        width,
                        height,
                        string,
                        fontMap
                    )
                }
            }
        }
    }
}
