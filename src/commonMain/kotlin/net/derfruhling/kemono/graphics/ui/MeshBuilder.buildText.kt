/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.ui

import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4
import net.derfruhling.kemono.mesh.MeshBuilder
import net.derfruhling.kemono.mesh.Quad
import net.derfruhling.kemono.mesh.Vertex
import net.derfruhling.kemono.mesh.addQuad
import net.derfruhling.kemono.text.FontMap

internal fun MeshBuilder.buildText(
    width: Int,
    height: Int,
    string: String,
    fontMap: FontMap
) {
    var i = 0
    var j = 0

    for (char in string) {
        when(char) {
            '\r' -> {}
            '\n' -> {
                i = 0
                j += height
            }

            ' ' -> {
                i += width
            }

            else -> {
                val corners = fontMap.mapCharacter(char)
                    ?: fontMap.mapCharacter('?')
                    ?: fontMap.mapCharacter('.')
                    ?: throw IllegalArgumentException("Character $char cannot be rendered by $fontMap")
                addQuad(
                    Quad(
                        Vertex(
                            position = Vector3(i.toFloat(), j.toFloat(), 0f),
                            color = Vector4(1f, 1f, 1f, 1f),
                            textureCoords = corners.bottomLeft
                        ),
                        Vertex(
                            position = Vector3(i.toFloat(), j.toFloat() + height, 0f),
                            color = Vector4(1f, 1f, 1f, 1f),
                            textureCoords = corners.topLeft
                        ),
                        Vertex(
                            position = Vector3(i.toFloat() + width, j.toFloat(), 0f),
                            color = Vector4(1f, 1f, 1f, 1f),
                            textureCoords = corners.bottomRight
                        ),
                        Vertex(
                            position = Vector3(i.toFloat() + width, j.toFloat() + height, 0f),
                            color = Vector4(1f, 1f, 1f, 1f),
                            textureCoords = corners.topRight
                        ),
                    )
                )
                i += width
            }
        }
    }
}