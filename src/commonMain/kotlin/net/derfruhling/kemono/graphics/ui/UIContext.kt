/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.ui

import net.derfruhling.kemono.Game
import net.derfruhling.kemono.graphics.RenderService
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.div
import net.derfruhling.kemono.math.toFloatVector
import net.derfruhling.kemono.mesh.Mesh
import net.derfruhling.kemono.mesh.bindMesh
import net.derfruhling.kemono.platform.PlatformService
import net.derfruhling.kemono.resources.husks.ShaderResource
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import net.derfruhling.kemono.text.FontMap
import net.derfruhling.kemono.text.TextService
import net.derfruhling.kemono.uniforms
import me.derfruhling.quantum.core.log.Logger

public abstract class UIContext : Component {
    private val game: Game by service()
    private val platformService: PlatformService by service()
    private val renderService: RenderService by service()
    private val textService: TextService by service()

    private val aspectRatio = platformService.primaryWindow.size
        .toFloatVector()
        .run { x / y }

    protected fun text(
        string: String,
        fontMap: FontMap = textService.monospaceFont
    ): Text {
        return when(fontMap) {
            is FontMap.Spritemap -> {
                val shader = renderService.shaderPool["kemono:shaders/ui/text.shdr"]
                val (width, height) =
                    fontMap.spritesheet.virtualSize /
                    fontMap.spritesheet.spriteCount

                Logger.of<Screen>().let { log ->
                    log.debug("Font map size {}, {}", width, height)
                    log.debug("Sprite count {}, {}",
                        fontMap.spritesheet.spriteCount.x,
                        fontMap.spritesheet.spriteCount.y)
                    log.debug("Virtual size {}, {}",
                        fontMap.spritesheet.virtualSize.x,
                        fontMap.spritesheet.virtualSize.y)
                }

                val mesh = Mesh.build(renderService, shader.value) {
                    buildText(width, height, string, fontMap)
                }

                val renderObject = renderService.graphics.createRenderObject {
                    this.shader = shader.value

                    uniforms(game)
                    bindMesh(mesh)

                    shader.value.boundUniforms.filter { it.value == ShaderResource.UniformAcceptValue.UI_BOUNDS }
                        .forEach { shader.value.uniform(it.key) from Vector2(
                            SCREEN_SIZE_DYN.toFloat() * aspectRatio,
                            SCREEN_SIZE_DYN.toFloat()
                        )
                        }
                }

                Text(string, shader, mesh, renderObject, fontMap)
            }
        }
    }
}