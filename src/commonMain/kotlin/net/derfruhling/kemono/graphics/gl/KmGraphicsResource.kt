/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.ThereIsNoSpoonException
import net.derfruhling.kemono.graphics.interfaces.DefinitelyBindable
import net.derfruhling.kemono.graphics.interfaces.GraphicsResource
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger

internal abstract class KmGraphicsResource : GraphicsResource, DefinitelyBindable, Disposable {
    private val log by logger

    abstract fun checkBound(): Boolean
    abstract fun bind()

    override val isDefinitelyBound: Boolean
        get() = definitelyBoundStacks > 0

    override var definitelyBoundStacks: Int = 0
        protected set

    override fun stackDefinitelyBoundStatus() {
        definitelyBoundStacks++
    }

    override fun removeDefinitelyBoundStatus() {
        if (--definitelyBoundStacks < 0) {
            log.error("graphics resource has mismatching definitely bound blocks", ThereIsNoSpoonException())
        }
    }

    override fun ensureBound() {
        if (isDefinitelyBound) return
        if (!checkBound()) bind()
    }

    abstract override fun dispose()
}