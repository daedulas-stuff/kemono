/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.enums.InternalTextureFormat
import net.derfruhling.kemono.graphics.enums.TextureFormat
import net.derfruhling.kemono.graphics.enums.ValueType
import net.derfruhling.kemono.graphics.interfaces.Texture3D
import okio.Buffer

internal expect class KmTexture3D : KmTexture, Texture3D {
    override val width: Int
    override val height: Int
    override val depth: Int

    override fun write(
        width: Int,
        height: Int,
        depth: Int,
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    )

    override fun writePartial(
        x: Int,
        y: Int,
        z: Int,
        width: Int,
        height: Int,
        depth: Int,
        textureFormat: TextureFormat,
        valueType: ValueType,
        buffer: Buffer
    )
}
