/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.enums.BufferUpdatePurpose
import net.derfruhling.kemono.graphics.enums.BufferUpdateRate
import net.derfruhling.kemono.graphics.interfaces.Buffer
import okio.Buffer as OkioBuffer

internal expect class KmBuffer : KmGraphicsResource, Buffer {
    override val size: Int
    override var type: BufferType
        private set

    override fun checkBound(): Boolean
    override fun bind()
    override fun write(buffer: OkioBuffer, updateRate: BufferUpdateRate, updatePurpose: BufferUpdatePurpose)
    override fun transitionBuffer(type: BufferType)
    override fun dispose()
}