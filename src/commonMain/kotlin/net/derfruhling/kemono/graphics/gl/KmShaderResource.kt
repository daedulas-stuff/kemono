/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.interfaces.Shader
import net.derfruhling.kemono.graphics.interfaces.ShaderInput
import net.derfruhling.kemono.graphics.interfaces.ShaderOutput
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import me.derfruhling.quantum.core.log.LogPrettyPrint
import net.derfruhling.kemono.resources.husks.ShaderResource as ShaderResourceHusk

internal class KmShaderResource(
    override val shader: Shader,
    override val boundInputs: Map<ShaderInput, Array<ShaderResourceHusk.InputValueSource>>,
    override val boundOutputs: Map<ShaderOutput, Int>,
    override val boundUniforms: Map<String, ShaderResourceHusk.UniformAcceptValue>,
) : ShaderResource, Shader by shader, LogPrettyPrint {
    override fun toPrettyString(): String {
        return "[#shader-resource $shader " +
                "boundInputs: ${boundInputs.toList().joinToString(",") { (k, v) -> "$k=$v" }} " +
                "boundOutputs: ${boundOutputs.toList().joinToString(",") { (k, v) -> "$k=$v" }} " +
                "boundUniforms: ${boundUniforms.toList().joinToString(",") { (k, v) -> "$k=$v" }}" +
                "]"
    }
}
