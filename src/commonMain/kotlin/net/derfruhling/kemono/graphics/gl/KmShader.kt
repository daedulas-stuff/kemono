/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.Vector3
import net.derfruhling.kemono.math.Vector4

internal expect class KmShader : KmGraphicsResource, Shader {
    override val parts: Map<ShaderPartStage, ShaderPart>
    override val inputs: Map<String, ShaderInput>
    override val uniforms: Map<String, ShaderUniform>
    override val inputOrder: Iterable<ShaderInput>?

    override fun getOutput(name: String): ShaderOutput

    override fun setUniform(uniform: ShaderUniform, x: Int)
    override fun setUniform(uniform: ShaderUniform, x: Int, y: Int)
    override fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int)
    override fun setUniform(uniform: ShaderUniform, x: Int, y: Int, z: Int, w: Int)
    override fun setUniform(uniform: ShaderUniform, x: Float)
    override fun setUniform(uniform: ShaderUniform, x: Float, y: Float)
    override fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float)
    override fun setUniform(uniform: ShaderUniform, x: Float, y: Float, z: Float, w: Float)
    override fun setUniform(uniform: ShaderUniform, x: Vector2<Int>)
    override fun setUniform(uniform: ShaderUniform, x: Vector3<Int>)
    override fun setUniform(uniform: ShaderUniform, x: Vector4<Int>)
    override fun setUniform(uniform: ShaderUniform, x: Vector2<Float>)
    override fun setUniform(uniform: ShaderUniform, x: Vector3<Float>)
    override fun setUniform(uniform: ShaderUniform, x: Vector4<Float>)

    override fun bind()
    override fun checkBound(): Boolean
    override fun dispose()
}