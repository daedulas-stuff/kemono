/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.Graphics
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.graphics.structures.UniformValue
import me.derfruhling.quantum.core.dsl.DslIndexSet

internal expect class KmRenderObject : KmGraphicsResource, RenderObject {
    class Creator : RenderObjectConfigurator {
        override var shader: Shader
        override var vertexBuffer: Buffer
        override var vertexCount: Int
        override var indexBuffer: Buffer?
        override var indexCount: Int?
        override val uniformValues: DslIndexSet<ShaderUniform, UniformValue>

        fun build(graphics: Graphics, vao: Int): KmRenderObject
    }

    class Editor : RenderObjectConfigurator {
        override var shader: Shader
        override var vertexBuffer: Buffer
        override var vertexCount: Int
        override var indexBuffer: Buffer?
        override var indexCount: Int?
        override val uniformValues: DslIndexSet<ShaderUniform, UniformValue>

        fun apply()
    }

    override val shader: Shader
    override val vertexBuffer: Buffer
    override val vertexCount: Int
    override val indexBuffer: Buffer?
    override val indexCount: Int?
    override val uniformValues: Map<ShaderUniform, UniformValue>

    override fun checkBound(): Boolean
    override fun bind()
    override fun dispose()
    override fun edit(fn: RenderObjectConfigurator.() -> Unit)
}