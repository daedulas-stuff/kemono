/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.gl

import net.derfruhling.kemono.graphics.enums.*
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.interfaces.TextureConfigurator
import net.derfruhling.kemono.math.Vector4
import me.derfruhling.quantum.core.log.LogPrettyPrint

internal expect sealed class KmTexture : KmGraphicsResource, Texture, LogPrettyPrint {
    val gl: UInt

    override val internalTextureFormat: InternalTextureFormat
    override val textureFormat: TextureFormat
    override val valueType: ValueType
    override val minFilter: MinifyFilter
    override val magFilter: MagnifyFilter
    override val sEdgeMode: EdgeMode
    override val tEdgeMode: EdgeMode
    override val rEdgeMode: EdgeMode
    override val borderColor: Vector4<Float>?

    protected fun setPostWriteVariables(
        internalTextureFormat: InternalTextureFormat,
        textureFormat: TextureFormat,
        valueType: ValueType
    )

    override fun checkBound(): Boolean
    override fun bind()
    override fun configure(fn: TextureConfigurator.() -> Unit)
    override fun dispose()
}
