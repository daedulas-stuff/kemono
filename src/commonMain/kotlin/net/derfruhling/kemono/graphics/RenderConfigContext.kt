/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.graphics.interfaces.RenderObject
import net.derfruhling.kemono.graphics.interfaces.Shader
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.graphics.interfaces.ShaderUniform
import net.derfruhling.kemono.graphics.structures.UniformValue
import kotlin.reflect.KClass

/**
 * Allows configuring additional properties of a singular draw command.
 */
public interface RenderConfigContext {
    /**
     * The render object being used in the render context.
     */
    public val renderObject: RenderObject

    /**
     * Specifies a uniform value token type to handle.
     *
     * @param T The type implementing [UniformValue.Token] to handle.
     *
     * @param kClass The class of [T]
     * @param handler Called every time [T] appears as a [UniformValue] to
     * resolve its actual value.
     */
    public fun <T : UniformValue.Token> onUniformToken(
        kClass: KClass<T>,
        handler: RenderConfigContext.(ShaderUniform, UniformValue.Token) -> UniformValue
    )
}

/**
 * The shader being used for the draw command.
 */
public inline val RenderConfigContext.shader: Shader
    get() = renderObject.shader

/**
 * The shader resource being used for the draw command.
 *
 * @throws IllegalStateException The current render object does not have a
 * [ShaderResource] as its [RenderObject.shader]
 */
public inline val RenderConfigContext.shaderResource: ShaderResource
    get() = renderObject.shader as? ShaderResource
        ?: throw IllegalStateException("renderObject.shader !is ShaderResource")

/**
 * Specifies a uniform value token type to handle.
 *
 * Alias of `RenderConfigContext.onUniformToken<T>(T::class) { t -> /* ... */ }`
 *
 * @param T The type implementing [UniformValue.Token] to handle.
 *
 * @param handler Called every time [T] appears as a [UniformValue] to
 * resolve its actual value.
 */
public inline fun <reified T : UniformValue.Token> RenderConfigContext.onUniformToken(
    crossinline handler: RenderConfigContext.(ShaderUniform, T) -> UniformValue
) {
    onUniformToken(T::class) { uniform, value ->
        handler(uniform, value as T)
    }
}
