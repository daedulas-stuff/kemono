/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.interfaces.*
import net.derfruhling.kemono.math.Vector4

/**
 * A graphics context that can be used to interact with OpenGL.
 * This instance should only be interacted with in the context of the render
 * thread, which can be retrieved the game instance if it is available.
 *
 * On macOS, the render thread **must** be the same as the main thread, else
 * macOS gets all pissy and throws an exception that may or may not be caught
 * by a try-catch block.
 */
public expect open class Graphics() {
    /**
     * The screen clear color.
     *
     * Safe to get asynchronously.
     * However, setting this calls an OpenGL function, and must be done on the
     * render thread.
     *
     * @see clear
     */
    @set:RenderFun
    public var clearColor: Vector4<Float>

    /**
     * Throws if there is an OpenGL error present.
     *
     * @see catchGL
     *
     * @throws OpenGLException There is an error.
     * A subclass of [OpenGLException] is thrown rather than the class itself.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun throwIfError()

    /**
     * Clears the current framebuffer, or just the window contents if there
     * is no framebuffer bound.
     *
     * @see clearColor
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun clear()

    /**
     * Creates a new buffer.
     *
     * @param type The type of the buffer to create.
     * Cannot be changed later.
     *
     * @return A brand new [Buffer].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun createBuffer(type: BufferType): Buffer

    /**
     * Creates a new 1-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture1D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun createTexture1D(): Texture1D

    /**
     * Creates a new 2-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture2D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun createTexture2D(): Texture2D

    /**
     * Creates a new 3-dimension texture.
     * The texture must be configured with [Texture.configure].
     *
     * @return A brand new [Texture3D].
     *
     * @throws OpenGLException An OpenGL error occurred and this graphics instance
     * is in debug mode.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun createTexture3D(): Texture3D

    /**
     * Sets a shader preprocessor define for _all_ shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun setShaderCommonPreprocessorBindingCommon(name: String, value: String? = null)

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.VERTEX] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun setShaderVertexStagePreprocessorBindingCommon(name: String, value: String? = null)

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.FRAGMENT] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun setShaderFragmentStagePreprocessorBindingCommon(name: String, value: String? = null)

    /**
     * Sets a shader preprocessor define for [ShaderPartStage.GEOMETRY] shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun setShaderGeometryStagePreprocessorBindingCommon(name: String, value: String? = null)

    /**
     * Removes a shader preprocessor define for _all_ shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun unsetShaderCommonPreprocessorBindingCommon(name: String)

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.VERTEX]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun unsetShaderVertexStagePreprocessorBindingCommon(name: String)

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.FRAGMENT]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun unsetShaderFragmentStagePreprocessorBindingCommon(name: String)

    /**
     * Removes a shader preprocessor define for [ShaderPartStage.GEOMETRY]
     * shaders.
     * This function has no direct effect on the OpenGL context, but should
     * still only be called on the render thread due to potentially
     * non-thread-safe map access.
     *
     * @throws Nothing This function does not throw. Intentionally, at least.
     */
    @Throws(OpenGLException::class)
    public open fun unsetShaderGeometryStagePreprocessorBindingCommon(name: String)

    /**
     * Creates a new shader part from the provided GLSL source.
     *
     * @param stage The stage of the part to create.
     * @param source The source code of the shader, in GLSL.
     * @param definitions An optional map of preprocessor definitions to pass
     * to this shader specifically.
     *
     * @return A brand new [ShaderPart]
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderCompileException The shader part failed to compile.
     * [ShaderCompileException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderCompileException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderCompileException::class)
    @RenderFun
    public open fun createShaderPart(
        stage: ShaderPartStage,
        source: String,
        definitions: Map<String, String> = emptyMap()
    ): ShaderPart

    /**
     * Creates a new shader part from the raw source code.
     * The source code will be passed directly to the compiler without being
     * modified in any way, which means that preprocessor definitions added to
     * this graphics instance will not be defined for these shaders.
     *
     * @param stage The stage of the part to create.
     * @param source The source code of the shader, in GLSL.
     *
     * @return A brand new [ShaderPart]
     *
     * You probably want [createShaderPart].
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderCompileException The shader part failed to compile.
     * [ShaderCompileException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderCompileException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderCompileException::class)
    @RenderFun
    public open fun createShaderPartFromRawSource(stage: ShaderPartStage, source: String): ShaderPart

    /**
     * Creates a new shader by linking multiple shader parts together.
     *
     * @param shaderParts The parts to link.
     * @param inputOrder An optional sequence of shader input names that will be
     * the order that inputs are placed into buffers when the mesh builder
     * checks for inputs.
     * If not provided, the input order is not guaranteed to be the same across
     * runs of the game.
     *
     * May be useful for debugging!
     *
     * @param disposeParts If `true`, the resulting [Shader] will dispose its
     * parts as well as itself when it is [dispose][Shader.dispose]d.
     * Otherwise, the parts are left dangling.
     * `false` may be useful if you are using the shader part in multiple
     * shaders, and/or plan to reuse shader parts.
     *
     * Kemono does not make use of this feature itself.
     *
     * @return A brand new [Shader]
     *
     * @throws OpenGLException An OpenGL error occurred.
     * @throws ShaderLinkException The shader failed to link.
     * [ShaderLinkException.log] is set to the compile log that the
     * compiler output.
     * This differs from [OpenGLException] in that a [ShaderLinkException]
     * does not represent an OpenGL error in the same way that another exception
     * might, so it does not extend [OpenGLException].
     */
    @Throws(OpenGLException::class, ShaderLinkException::class)
    @RenderFun
    public open fun createShader(
        vararg shaderParts: ShaderPart,
        inputOrder: Iterable<String>? = null,
        disposeParts: Boolean = false
    ): Shader

    /**
     * Creates a new [RenderObject] from a configuration provided by the block.
     *
     * @param fn The configuration block. Should produce a valid [RenderObject].
     *
     * @return A brand new [RenderObject]
     *
     * @see RenderObjectConfigurator
     *
     * @throws OpenGLException An OpenGL error occurred.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun createRenderObject(fn: RenderObjectConfigurator.() -> Unit): RenderObject

    /**
     * Copies the contents of one buffer to another.
     * Both buffers must have been initialized by a [Buffer.write] before this
     * function is called, otherwise an error may be thrown.
     *
     * @param sourceOffset The offset (in bytes) into [source] from which to
     * copy bytes.
     * @param source The buffer to copy bytes from.
     * @param targetOffset The offset (in bytes) into [target] into which to
     * copy bytes.
     * @param target The buffer to copy bytes into.
     * @param size The number of bytes to copy from [source] to [target].
     *
     * @throws OpenGLException An OpenGL error occurred.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public open fun copyBuffer(
        sourceOffset: Int,
        source: Buffer,
        targetOffset: Int,
        target: Buffer,
        size: Int
    )

    /**
     * Draws the [renderObject] to the current framebuffer.
     *
     * @param renderObject The [RenderObject] to draw.
     *
     * @throws OpenGLException An OpenGL error occurred.
     */
    @Throws(OpenGLException::class)
    @RenderFun
    public inline fun draw(renderObject: RenderObject, crossinline fn: RenderConfigContext.() -> Unit = {})

    public companion object {
        /**
         * The common shader prefix prepended to all shaders passed to
         * [createShaderPart].
         */
        public val COMMON_SHADER_PREFIX: String

        /**
         * The [ShaderPartStage.VERTEX] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        public val VERTEX_SHADER_PREFIX: String

        /**
         * The [ShaderPartStage.FRAGMENT] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        public val FRAGMENT_SHADER_PREFIX: String

        /**
         * The [ShaderPartStage.GEOMETRY] exclusive shader prefix prepended
         * to all shaders passed to [createShaderPart].
         */
        public val GEOMETRY_SHADER_PREFIX: String
    }
}

