/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import net.derfruhling.kemono.AsyncService
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.error.IllegalResourceTypeException
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.interfaces.Buffer
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.interfaces.Texture2D
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.resources.ResourceService
import net.derfruhling.kemono.resources.husks.TextureResource
import net.derfruhling.kemono.resources.read
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.interfaces.Disposable
import me.derfruhling.quantum.core.log.logger

public open class RenderService(
    override val serviceLoader: ServiceLoader
) : Service, Disposable {
    private val log by logger
    private val resourceService: ResourceService by service()
    private val asyncService: AsyncService by service()

    /**
     * Creates the [Graphics] instance for this [RenderService].
     * This exists to allow a game to override [Graphics] and implement
     * their own functionality overtop it, while still using the existing
     * graphics code.
     */
    protected open fun createGraphics(): Graphics = Graphics()

    public lateinit var graphics: Graphics
        private set

    /**
     * A [NamedPool] that automatically loads and destroys shader resources.
     */
    public val shaderPool: NamedPool<ShaderResource> = object : NamedPool<ShaderResource>() {
        override fun load(name: String): ShaderResource {
            @OptIn(InternalAPI::class)
            return createShaderResource(name)
        }
    }

    /**
     * A [NamedPool] that automatically loads and destroys shader resources.
     */
    public val texturePool: NamedPool<Texture> = object : NamedPool<Texture>() {
        override fun load(name: String): Texture {
            @OptIn(InternalAPI::class)
            return createTextureResource(name)
        }
    }

    public val bufferPool: BufferPool = BufferPool()

    override fun initialize() {
        graphics = createGraphics()
    }

    override fun dispose() {
        shaderPool.dispose()
        texturePool.dispose()
    }

    /**
     * _Updates_ the [NamedPool] objects contained in this game.
     * This function does not call [NamedPool.cleanup].
     *
     * @see cleanupPools
     */
    public open suspend fun updatePools(): Unit = coroutineScope {
        launch { shaderPool.update() }
        launch { texturePool.update() }
    }

    /**
     * Cleans up [NamedPool] objects. Called after [updatePools].
     */
    public open suspend fun cleanupPools(): Unit = coroutineScope {
        launch { shaderPool.cleanup(asyncService) }
        launch { texturePool.cleanup(asyncService) }
    }

    @InternalAPI
    public fun createShaderResource(name: String): ShaderResource {
        try {
            val shader = resourceService[name].read<net.derfruhling.kemono.resources.husks.ShaderResource>()
            return graphics.createShaderFromResource(shader)
        } catch (e: RuntimeException) {
            throw RuntimeException("Exception encountered loading resource $name", e)
        }
    }

    @InternalAPI
    public fun createTextureResource(name: String): Texture {
        val texture = resourceService[name].read<TextureResource>()
        return graphics.createTextureFromResource(texture)
    }

    @Throws(IllegalResourceTypeException::class)
    public fun createSpritesheet(name: String, spriteCount: Vector2<Int>): Spritesheet {
        val texture = @OptIn(InternalAPI::class) createTextureResource(name)

        if(texture !is Texture2D) {
            try {
                texture.dispose()
            } catch (e: Exception) {
                log.error("Error disposing texture due to illegal resource type", e)
            }

            throw IllegalResourceTypeException("Texture $name is not a Texture2D")
        }

        return Spritesheet(
            texture,
            spriteCount
        )
    }

    @RenderFun
    public fun allocateVertexBuffer(): Buffer {
        return bufferPool.takeVertexBuffer()
            ?: graphics.createBuffer(BufferType.VERTEX_ARRAY)
    }

    @RenderFun
    public fun allocateIndexBuffer(): Buffer {
        return bufferPool.takeIndexBuffer()
            ?: graphics.createBuffer(BufferType.INDEX_ARRAY)
    }
}
