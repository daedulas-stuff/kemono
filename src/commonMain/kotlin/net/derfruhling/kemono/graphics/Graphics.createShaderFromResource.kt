/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.enums.ShaderPartStage
import net.derfruhling.kemono.graphics.gl.KmShaderResource
import net.derfruhling.kemono.graphics.interfaces.ShaderResource
import net.derfruhling.kemono.resources.husks.ShaderResource as ShaderResourceHusk

/**
 * Creates a shader resource from a file stored in resource storage, or just
 * from arbitrary data.
 *
 * @receiver [Graphics]
 * @param shaderResource The shader resource's husk.
 *
 * @return A brand new [ShaderResource]
 *
 * @throws OpenGLException When an OpenGL error occurs.
 */
@Throws(OpenGLException::class)
@RenderFun
public fun Graphics.createShaderFromResource(shaderResource: ShaderResourceHusk): ShaderResource {
    val parts = listOfNotNull(
        createShaderPart(
            ShaderPartStage.VERTEX,
            shaderResource.vertexShaderSource,
            definitions = shaderResource.definitions
        ),
        createShaderPart(
            ShaderPartStage.FRAGMENT,
            shaderResource.fragmentShaderSource,
            definitions = shaderResource.definitions
        ),
        shaderResource.geometryShaderSource?.let { source ->
            createShaderPart(ShaderPartStage.GEOMETRY, source, definitions = shaderResource.definitions)
        }
    ).toTypedArray()

    val shader = createShader(*parts, disposeParts = true, inputOrder = shaderResource.inputs.map { it.name })
    return KmShaderResource(
        shader,
        shaderResource.inputs.associate { shader.inputs[it.name]!! to it.source },
        shaderResource.outputs.associate { shader.getOutput(it.name) to it.into },
        shaderResource.uniformAccepts,
    )
}
