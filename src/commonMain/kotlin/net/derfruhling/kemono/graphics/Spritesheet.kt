/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.graphics.interfaces.Texture2D
import net.derfruhling.kemono.math.*

/**
 * A spritesheet referring to sprites in a 2D texture.
 *
 * @property texture The texture to read from.
 * @property spriteCount The number of sprites in each dimension.
 * @property offset The offset from the beginning edges to interpret the
 * spritesheet from.
 * X is the left side, Y is the top.
 * @property ignoredMargin THe offset from the ending edges to ignore pixels
 * that do not contribute to any sprites.
 * X is the right side, Y is the bottom.
 */
public data class Spritesheet(
    val texture: Texture2D,
    val spriteCount: Vector2<Int>,
    val offset: Vector2<Int> = Vector2(0, 0),
    val ignoredMargin: Vector2<Int> = Vector2(0, 0)
) : Texture2D by texture {
    private val constantOffset = offset.toFloatVector() / texture.size.toFloatVector()
    private val constantMargin = ignoredMargin.toFloatVector() / texture.size.toFloatVector()

    public val virtualSize: Vector2<Int> = texture.size - offset - ignoredMargin

    private fun getPosOf(position: Vector2<Int>): Vector2<Float> =
        (position.toFloatVector() / spriteCount.toFloatVector()).let { pos ->
            constantOffset + (pos - (constantOffset + constantMargin) * pos)
        }

    /**
     * Gets the corners referring into this spritesheet for the sprite at
     * [position].
     */
    public operator fun get(position: Vector2<Int>): Corners<Vector2<Float>> =
        Corners(
            getPosOf(position),
            getPosOf(Vector2(position.x + 1, position.y)),
            getPosOf(Vector2(position.x, position.y + 1)),
            getPosOf(Vector2(position.x + 1, position.y + 1)),
        )
}
