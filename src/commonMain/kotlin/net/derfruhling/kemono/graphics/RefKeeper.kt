/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import me.derfruhling.quantum.core.dsl.DslDelegateGetter
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * Keeps refs.
 * A class can extend this value to keep refs.
 * Ex, `Entity` extends this to help keep track of inheritor-defined refs.
 */
public abstract class RefKeeper : Disposable {
    private val refs = mutableListOf<NamedPool.Ref<*>>()

    /**
     * Adds a [NamedPool.Ref] to this keeper.
     * It will be disposed of when this keeper is disposed.
     */
    protected fun <T : Disposable> ref(ref: NamedPool.Ref<T>): DslDelegateGetter<Any, T> {
        return DslDelegateGetter { _, _ -> ref.value }
    }

    /**
     * Adds a [NamedPool.Ref] to this keeper.
     * It will be disposed of when this keeper is disposed.
     */
    protected fun <T : Disposable> NamedPool.Ref<T>.ref(): DslDelegateGetter<Any, T> = ref(this)

    override fun dispose() {
        refs.forEach { it.dispose() }
    }
}
