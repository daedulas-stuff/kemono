/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

/**
 * A stage of a shader part.
 */
public enum class ShaderPartStage {
    /**
     * The shader is run on every vertex.
     * The outputs of the shader are interpolated and passed to the
     * [FRAGMENT] part of the complete shader.
     */
    VERTEX,

    /**
     * The shader is run every fragment.
     * This may be run multiple times for a single pixel.
     * The outputs represent the output color.
     */
    FRAGMENT,

    /**
     * The shader is run for every primitive passed to the shader.
     * The shader then outputs more primitives to modify the input.
     * This is run before the [VERTEX] shader.
     */
    GEOMETRY
}