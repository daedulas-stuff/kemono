/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics.enums

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.derfruhling.kemono.graphics.interfaces.Texture

/**
 * Represents a chosen edge filtering mode for a [Texture].
 * If a texture sampling tries to sample something outside the bounds of the
 * texture, this operation is applied to determine what is returned.
 */
@Serializable
public expect enum class EdgeMode {
    /**
     * The texture will repeat itself verbatim outside the bounds of the
     * texture.
     *
     * This makes the following operations equivalent:
     * - `texture(tex, vec2(0.75, 0.5))`
     * - `texture(tex, vec2(-0.25, 0.5))` -> `texture(tex, vec2(0.75, 0.5))`
     */
    @SerialName("r")
    REPEAT,

    /**
     * The texture will repeat itself mirrored outside the bounds of the
     * texture.
     *
     * This makes the following operations equivalent:
     * - `texture(tex, vec2(0.75, 0.5))`
     * - `texture(tex, vec2(-0.75, 0.5))` -> `texture(tex, vec2(-0.75, 0.5))`
     */
    @SerialName("rm")
    REPEAT_MIRRORED,

    /**
     * The texture will clamp the passed position to the edges of its bounds.
     *
     * This makes the following operations equivalent:
     * - `texture(tex, vec2(1, 0))`
     * - `texture(tex, vec2(1.5, 0))` -> `texture(tex, vec2(1, 0))`
     * - `texture(tex, vec2(2.6187236, 0))` -> `texture(tex, vec2(1, 0))`
     */
    @SerialName("c")
    CLAMP,

    /**
     * The texture sampler will return a specified color if it tries to sample
     * out of bounds.
     * The color must be configured in the texture configurator.
     *
     * Example operations (they are not equivalent) (mostly):
     * - `texture(tex, vec2(1, 0))` -> texture
     * - `texture(tex, vec2(1.5, 0))` -> border color
     * - `texture(tex, vec2(2.6187236, 0))` -> border color
     */
    @SerialName("b")
    CLAMP_BORDER_COLOR
}
