/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.interfaces.Texture
import net.derfruhling.kemono.graphics.interfaces.Texture1D
import net.derfruhling.kemono.graphics.interfaces.Texture2D
import net.derfruhling.kemono.graphics.interfaces.Texture3D
import net.derfruhling.kemono.resources.husks.TextureResource
import me.derfruhling.quantum.core.log.Logger
import okio.Buffer

private fun okioBufferFromTextureResource(textureResource: TextureResource): Buffer {
    val originalBuffer = Buffer().write(textureResource.byteArray)
    val buffer = if (textureResource.options.valueType.requiresConversionToLittleEndian) {
        when (textureResource.options.valueType) {
            TextureResource.ValueType.FLOAT, TextureResource.ValueType.U_INT -> originalBuffer.reverseElementsOfIntBuffer()
            TextureResource.ValueType.U_SHORT -> originalBuffer.reverseElementsOfShortBuffer()
            TextureResource.ValueType.U_BYTE -> throw NotImplementedError()
        }
    } else originalBuffer

    return buffer
}

/**
 * Creates a new 1-dimensional texture from a resource husk.
 *
 * @receiver [Graphics]
 * @param textureResource The resource husk to create from.
 *
 * @return A brand new [Texture1D]
 *
 * @throws OpenGLException When an OpenGL error occurs.
 */
@Throws(OpenGLException::class)
@RenderFun
public fun Graphics.createTexture1DFromResource(textureResource: TextureResource): Texture1D {
    if (textureResource.options.type != TextureResource.Type.TEXTURE_1D)
        throw IllegalArgumentException("texture resource is not 1d")

    val texture = createTexture1D()

    if (textureResource.size.size != 1)
        throw IllegalStateException("texture resource ${textureResource.metadata.name} has mismatched dimension count")

    texture.write(
        textureResource.size[0].toInt(),
        textureResource.options.arrangement.internal,
        textureResource.options.arrangement.real,
        textureResource.options.valueType.real,
        okioBufferFromTextureResource(textureResource)
    )

    texture.configure {
        minFilter = textureResource.options.minifyFilter
        magFilter = textureResource.options.magnifyFilter
        if (textureResource.options.edgeModes.size != 1)
            throw IllegalStateException("texture resource ${textureResource.metadata.name} has illegal edge mode count")
        sEdgeMode = textureResource.options.edgeModes[0]
        textureResource.options.borderColor?.let { borderColor = it }
    }

    Logger.of(Graphics::class)
        .debug("Loaded texture[1D] {} as a graphics resource", textureResource.metadata.name)
    return texture
}

/**
 * Creates a new 2-dimensional texture from a resource husk.
 *
 * @receiver [Graphics]
 * @param textureResource The resource husk to create from.
 *
 * @return A brand new [Texture2D]
 *
 * @throws OpenGLException When an OpenGL error occurs.
 */
@Throws(OpenGLException::class)
@RenderFun
public fun Graphics.createTexture2DFromResource(textureResource: TextureResource): Texture2D {
    if (textureResource.options.type != TextureResource.Type.TEXTURE_2D)
        throw IllegalArgumentException("texture resource is not 2d")

    val texture = createTexture2D()

    if (textureResource.size.size != 2)
        throw IllegalStateException("texture resource ${textureResource.metadata.name} has mismatched dimension count")

    texture.write(
        textureResource.size[0].toInt(),
        textureResource.size[1].toInt(),
        textureResource.options.arrangement.internal,
        textureResource.options.arrangement.real,
        textureResource.options.valueType.real,
        okioBufferFromTextureResource(textureResource)
    )

    texture.configure {
        minFilter = textureResource.options.minifyFilter
        magFilter = textureResource.options.magnifyFilter
        if (textureResource.options.edgeModes.size != 2)
            throw IllegalStateException("texture resource ${textureResource.metadata.name} has illegal edge mode count")
        sEdgeMode = textureResource.options.edgeModes[0]
        tEdgeMode = textureResource.options.edgeModes[1]
        textureResource.options.borderColor?.let { borderColor = it }
    }

    Logger.of(Graphics::class)
        .debug("Loaded texture[2D] {} as a graphics resource", textureResource.metadata.name)
    return texture
}

/**
 * Creates a new 3-dimensional texture from a resource husk.
 *
 * @receiver [Graphics]
 * @param textureResource The resource husk to create from.
 *
 * @return A brand new [Texture3D]
 *
 * @throws OpenGLException When an OpenGL error occurs.
 */
@Throws(OpenGLException::class)
@RenderFun
public fun Graphics.createTexture3DFromResource(textureResource: TextureResource): Texture3D {
    if (textureResource.options.type != TextureResource.Type.TEXTURE_3D)
        throw IllegalArgumentException("texture resource is not 3d")

    val texture = createTexture3D()

    if (textureResource.size.size != 3)
        throw IllegalStateException("texture resource ${textureResource.metadata.name} has mismatched dimension count")

    texture.write(
        textureResource.size[0].toInt(),
        textureResource.size[1].toInt(),
        textureResource.size[2].toInt(),
        textureResource.options.arrangement.internal,
        textureResource.options.arrangement.real,
        textureResource.options.valueType.real,
        okioBufferFromTextureResource(textureResource)
    )

    texture.configure {
        minFilter = textureResource.options.minifyFilter
        magFilter = textureResource.options.magnifyFilter
        if (textureResource.options.edgeModes.size != 3)
            throw IllegalStateException("texture resource ${textureResource.metadata.name} has illegal edge mode count")
        sEdgeMode = textureResource.options.edgeModes[0]
        tEdgeMode = textureResource.options.edgeModes[1]
        tEdgeMode = textureResource.options.edgeModes[2]
        textureResource.options.borderColor?.let { borderColor = it }
    }

    Logger.of(Graphics::class)
        .debug("Loaded texture[3D] {} as a graphics resource", textureResource.metadata.name)
    return texture
}

/**
 * Creates a new texture from a resource husk.
 *
 * @receiver [Graphics]
 * @param textureResource The resource husk to create from.
 *
 * @return A brand new [Texture]
 *
 * @throws OpenGLException When an OpenGL error occurs.
 */
@Throws(OpenGLException::class)
@RenderFun
public fun Graphics.createTextureFromResource(textureResource: TextureResource): Texture {
    return when (textureResource.options.type) {
        TextureResource.Type.TEXTURE_1D -> createTexture1DFromResource(textureResource)
        TextureResource.Type.TEXTURE_2D -> createTexture2DFromResource(textureResource)
        TextureResource.Type.TEXTURE_3D -> createTexture3DFromResource(textureResource)
    }
}
