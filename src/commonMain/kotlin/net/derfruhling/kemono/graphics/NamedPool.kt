/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import net.derfruhling.kemono.AsyncService
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * A container for resources that allows loading them on demand.
 */
public abstract class NamedPool<T : Disposable> : Disposable {
    internal sealed interface ChannelOp {
        value class CreateRef(val name: String) : ChannelOp
        value class DestroyRef(val name: String) : ChannelOp
    }

    private val channel = Channel<ChannelOp>(Channel.UNLIMITED)
    private val referencesSemaphore = Semaphore(1)
    private val references = mutableMapOf<String, ReferenceCounter<T>>()

    internal fun send(op: ChannelOp) { channel.trySend(op) }
    internal fun sendCreateRef(name: String) = send(ChannelOp.CreateRef(name))
    internal fun sendDestroyRef(name: String) = send(ChannelOp.DestroyRef(name))

    internal class ReferenceCounter<T : Disposable>(val value: T) : Disposable {
        var count = 0
        val semaphore = Semaphore(1)

        var isDisposed: Boolean = false
            private set

        override fun dispose() {
            isDisposed = true
            value.dispose()
        }
    }

    /**
     * Refers to a reference of value [T] stored in a [NamedPool].
     * [dispose] this [Ref] when you no longer need its resource.
     *
     * @property pool The source pool of the resource.
     * @property name The name of the resource.
     */
    public class Ref<T : Disposable> internal constructor(
        public val pool: NamedPool<T>,
        public val name: String,
        private val refCounter: ReferenceCounter<T>
    ) : Disposable {
        /**
         * If this is `true`, this [Ref] should not be used.
         */
        public var isDisposed: Boolean = false
            get() = field || refCounter.isDisposed
            private set

        /**
         * Gets the value of this reference.
         */
        public val value: T
            get() =
                if(!isDisposed) refCounter.value
                else throw IllegalStateException("Disposed ref")

        override fun dispose() {
            if(isDisposed) return
            isDisposed = true
            pool.sendDestroyRef(name)
        }
    }

    /**
     * Loads a new value that is not already present is this [NamedPool].
     *
     * @param name The name of the resource.
     * @return [T]
     */
    protected abstract fun load(name: String): T

    /**
     * Gets (and possibly creates) a resource.
     *
     * This function is thread-safe.
     * It can be called anywhere.
     *
     * @param name The name of the resource.
     * @return A reference to [T]. [Ref.dispose] it if you're done with it.
     */
    public operator fun get(name: String): Ref<T> {
        val refCounter = if(!references.containsKey(name)) {
            ReferenceCounter(load(name)).also { references[name] = it }
        } else {
            references[name]!!
        }

        sendCreateRef(name)
        return Ref(this, name, refCounter)
    }

    /**
     * Processes reference count updates.
     */
    public suspend fun update(): Unit = coroutineScope {
        while(true) {
            val result = channel.tryReceive()
            if(result.isFailure || result.isClosed) break

            launch {
                when (val op = result.getOrThrow()) {
                    is ChannelOp.CreateRef -> references[op.name]!!.semaphore.withPermit {
                        references[op.name]!!.count++
                    }

                    is ChannelOp.DestroyRef -> references[op.name]!!.semaphore.withPermit {
                        references[op.name]!!.count--
                    }
                }
            }
        }
    }

    /**
     * Performs routine cleanup on this [NamedPool], destroying unneeded objects.
     */
    public suspend fun cleanup(asyncService: AsyncService): Unit = coroutineScope {
        references.forEach { (name, ref) ->
            if(ref.count <= 0) launch {
                asyncService.runOnRenderThread { ref.dispose() }
                referencesSemaphore.withPermit {
                    references.remove(name)
                }
            }
        }
    }

    override fun dispose() {
        references.forEach { (_, value) -> value.dispose() }
        references.clear()
    }
}