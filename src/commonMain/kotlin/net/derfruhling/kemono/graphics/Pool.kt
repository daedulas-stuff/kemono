/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.graphics

import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.graphics.enums.BufferType
import net.derfruhling.kemono.graphics.interfaces.Buffer
import me.derfruhling.quantum.core.interfaces.Disposable

/**
 * A container for resources that allows loading them on demand.
 */
public open class Pool<T : Disposable> : Disposable {
    private val available = mutableListOf<T>()

    /**
     * Adds [value] to this pool.
     * It will be available when [take] is called.
     */
    @RenderFun
    public open fun enpool(value: T) {
        available.add(value)
    }

    /**
     * Takes a value from this pool. Returns `null` if the pool is empty.
     */
    @RenderFun
    public open fun take(): T? {
        return available.removeFirstOrNull()
    }

    /**
     * Pre-allocates [count] values with [fn] and adds all of them to the pool
     * for later use.
     */
    @RenderFun
    public inline fun preAllocate(count: Int, fn: (Int) -> T) {
        repeat(count) { index ->
            enpool(fn(index))
        }
    }

    override fun dispose() {
        available.forEach { it.dispose() }
    }
}

/**
 * A buffer-specific form of [Pool]
 */
public open class BufferPool : Pool<Buffer>() {
    @Suppress("DeprecatedCallableAddReplaceWith")
    @Deprecated("Use more specific buffer take functions")
    override fun take(): Buffer? {
        return super.take()
    }

    /**
     * Takes a buffer from this [BufferPool] and transitions it to a new
     * [BufferType] if necessary.
     */
    public fun take(type: BufferType): Buffer? {
        val buffer = super.take() ?: return null
        buffer.transitionBuffer(type)
        return buffer
    }

    public fun takeVertexBuffer(): Buffer? = take(BufferType.VERTEX_ARRAY)
    public fun takeIndexBuffer(): Buffer? = take(BufferType.INDEX_ARRAY)
}
