/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import kotlinx.coroutines.*
import net.derfruhling.kemono.annotations.InternalAPI
import net.derfruhling.kemono.annotations.RenderFun
import net.derfruhling.kemono.services.Service
import net.derfruhling.kemono.services.ServiceLoader
import me.derfruhling.quantum.core.async.ThreadId
import me.derfruhling.quantum.core.async.maxConcurrency
import me.derfruhling.quantum.core.interfaces.Disposable

public open class AsyncService(
    override val serviceLoader: ServiceLoader
) : Service, Disposable {
    /**
     * The thread that this [AsyncService] was constructed on.
     * This is assumed to be the main thread.
     * Whether it actually is... is not relevant (or known).
     */
    public val mainThread: ThreadId = ThreadId.current()

    private val renderThreadDispatcher = RenderThreadDispatcher()
    private val _asyncThreadPool = newFixedThreadPoolContext(maxConcurrency, "km_async")

    /**
     * An asynchronous thread pool for running things asynchronously.
     * Disposed of by this [AsyncService] when it dies.
     */
    public val threadPool: CoroutineDispatcher get() = _asyncThreadPool

    /**
     * A [CoroutineScope] wrapping [threadPool], allowing tasks to be
     * started arbitrarily.
     */
    public val coroutineScope: CoroutineScope = CoroutineScope(threadPool)


    /**
     * Launches a coroutine in [coroutineScope].
     *
     * @param fn A block to execute.
     */
    public inline fun launch(noinline fn: suspend CoroutineScope.() -> Unit): Job {
        return coroutineScope.launch(block = fn)
    }

    /**
     * Launches a coroutine in [coroutineScope] and returns a [Deferred]
     * that will provide the returned value when the coroutine completes.
     *
     * @param fn A block to execute.
     *
     * @see launch
     */
    public inline fun <T> async(noinline fn: suspend CoroutineScope.() -> T): Deferred<T> {
        return coroutineScope.async(block = fn)
    }

    @PublishedApi
    internal fun publishToRenderThreadDispatcher(fn: () -> Unit) {
        renderThreadDispatcher.dispatch(Runnable(fn))
    }

    @InternalAPI
    public fun endRenderThreadFrame() {
        renderThreadDispatcher.endFrame()
    }

    @InternalAPI
    public fun acceptRenderThreadRunnables(): Iterator<Runnable> {
        return iterator {
            while(true) {
                yield(renderThreadDispatcher.accept() ?: break)
            }
        }
    }

    /**
     * Runs a block on the render thread the next time [render] is called.
     *
     * @param fn The function to run.
     * If [ThreadId.current()][ThreadId.current] is the same as [mainThread],
     * `fn()` is instead immediately inlined into this function and executed.
     */
    public inline fun runOnRenderThread(crossinline fn: @RenderFun () -> Unit) {
        if(ThreadId.current() == mainThread) {
            fn()
        } else {
            publishToRenderThreadDispatcher { fn() }
        }
    }

    override fun dispose() {
        // closing this throws an exception :/
        // FIXME maybe someday...
        // _asyncThreadPool.close()
    }
}