/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono

import net.derfruhling.kemono.graphics.Graphics

/**
 * An exception that is thrown by [scopedGame] when there is no game in scope.
 *
 * @see runGame
 * @see gameScoped
 */
public class GameScopeException : IllegalStateException("No game in scope")

private var gameScope: Game? = null

/**
 * The game that is currently in scope.
 *
 * @see runGame
 * @see gameScoped
 *
 * @throws GameScopeException Thrown if there is no game in scope.
 */
public val scopedGame: Game
    get() = gameScope ?: throw GameScopeException()

/**
 * The [Game.graphics] of [scopedGame].
 *
 * @see runGame
 * @see gameScoped
 *
 * @throws GameScopeException Thrown if there is no game in scope.
 */
public inline val scopedGraphics: Graphics
    get() = scopedGame.graphics

@PublishedApi
internal fun setGameScope(game: Game?) {
    gameScope = game
}

/**
 * Sets the current game scope.
 *
 * @param game The game to place in scope.
 * @param fn The function block to execute with the game in scope.
 */
public inline fun gameScoped(game: Game, fn: () -> Unit) {
    try {
        setGameScope(game)
        fn()
    } finally {
        setGameScope(null)
    }
}
