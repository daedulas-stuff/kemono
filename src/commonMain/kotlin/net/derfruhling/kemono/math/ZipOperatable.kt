/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.math

/**
 * Allows `VectorMath.kt` to be much simpler by specifying a way to execute
 * math operations on vector types without knowing how many of [T] are contained
 * in the vector type.
 */
public interface ZipOperatable<T, Self : ZipOperatable<T, Self>> {
    /**
     * Zip-operates on `this` and [other].
     *
     * For every [T] in [Self], [operation] is called with the first parameter
     * being a value in `this` and the second parameter being the value in the
     * equivalent position in [other].
     *
     * @return Zip-operated [Self].
     */
    public fun zipOperate(other: Self, operation: (T, T) -> T): Self

    /**
     * Zip-operates on `this` and nothing.
     * Functionally equal to [map] on an iterable type, but for strict vector
     * types.
     */
    public fun zipOperate(operation: (T) -> T): Self

    /**
     * Zip-combines on `this`.
     * Functionally equal to [reduce] on an iterable type, but for strict vector
     * types.
     */
    public fun zipCombine(operation: (T, T) -> T): T
}
