/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.math

import kotlinx.serialization.Serializable

/**
 * Represents a [ZipOperatable] vector of three [T]'s.
 *
 * @property x [T]
 * @property y [T]
 * @property z [T]
 */
@Serializable
public data class Vector3<T>(val x: T, val y: T, val z: T) : ZipOperatable<T, Vector3<T>> {
    override fun zipOperate(other: Vector3<T>, operation: (T, T) -> T): Vector3<T> {
        return Vector3(
            operation(x, other.x),
            operation(y, other.y),
            operation(z, other.z)
        )
    }

    override fun zipOperate(operation: (T) -> T): Vector3<T> {
        return Vector3(
            operation(x),
            operation(y),
            operation(z)
        )
    }

    override fun zipCombine(operation: (T, T) -> T): T {
        return listOf(x, y, z).reduce(operation)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vector3<*>) return false

        if (x != other.x) return false
        if (y != other.y) return false
        if (z != other.z) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x?.hashCode() ?: 0
        result = 31 * result + (y?.hashCode() ?: 0)
        result = 31 * result + (z?.hashCode() ?: 0)
        return result
    }
}