/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.math

/**
 * Represents four corners of a quadrilateral.
 *
 * @param T The type of this object. Can be anything. Even nullable types.
 *
 * @property topLeft The top left corner.
 * @property topRight The top right corner.
 * @property bottomLeft The bottom left corner.
 * @property bottomRight The bottom right corner.
 */
public data class Corners<T>(
    public val topLeft: T,
    public val topRight: T,
    public val bottomLeft: T,
    public val bottomRight: T,
) {
    public companion object {
        public inline val unitBox: Corners<Vector2<Float>>
            get() = Corners(
                Vector2(0f, 0f),
                Vector2(1f, 0f),
                Vector2(0f, 1f),
                Vector2(1f, 1f)
            )
    }
}
