/*
 * Kemono is under the BSD license, partially included below:
 *
 * Copyright [c] 2023 der_frühling
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/orother materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * Please see LICENSE in the root directory of this repository for the complete licensing
 * information.
 */

package net.derfruhling.kemono.math

import kotlin.math.*

/*
 * Vector swizzling is left without documentation as it is assumed that
 * their function is obvious from the name.
 *
 * @Suppress is applied individually to avoid applying it to the whole file.
 */

// Vector2 swizzling ===========================================================

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector2<T>.xy: Vector2<T> get() = this

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector2<T>.yx: Vector2<T> get() = Vector2(y, x)

// Vector3 swizzling ===========================================================

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.xy: Vector2<T> get() = Vector2(x, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.yx: Vector2<T> get() = Vector2(y, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.xyz: Vector3<T> get() = this

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.xzy: Vector3<T> get() = Vector3(x, z, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.zxy: Vector3<T> get() = Vector3(z, x, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.zyx: Vector3<T> get() = Vector3(z, y, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.yzx: Vector3<T> get() = Vector3(y, z, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector3<T>.yxz: Vector3<T> get() = Vector3(y, x, z)


// Vector4 swizzling ===========================================================

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xyz: Vector3<T> get() = Vector3(x, y, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xzy: Vector3<T> get() = Vector3(x, z, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zxy: Vector3<T> get() = Vector3(z, x, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zyx: Vector3<T> get() = Vector3(z, y, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.yzx: Vector3<T> get() = Vector3(y, z, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.yxz: Vector3<T> get() = Vector3(y, x, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xyzw: Vector4<T> get() = Vector4(x, y, z, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xzyw: Vector4<T> get() = Vector4(x, z, y, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zxyw: Vector4<T> get() = Vector4(z, x, y, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zyxw: Vector4<T> get() = Vector4(z, y, x, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.yzxw: Vector4<T> get() = Vector4(y, z, x, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.yxzw: Vector4<T> get() = Vector4(y, x, z, w)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xywz: Vector4<T> get() = Vector4(x, y, w, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xzwy: Vector4<T> get() = Vector4(x, z, w, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zxwy: Vector4<T> get() = Vector4(z, x, w, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zywx: Vector4<T> get() = Vector4(z, y, w, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.yzwx: Vector4<T> get() = Vector4(y, z, w, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.ywxz: Vector4<T> get() = Vector4(y, w, x, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xwyz: Vector4<T> get() = Vector4(x, w, y, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.xwzy: Vector4<T> get() = Vector4(x, w, z, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zwxy: Vector4<T> get() = Vector4(z, w, x, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.zwyx: Vector4<T> get() = Vector4(z, w, y, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.ywzx: Vector4<T> get() = Vector4(y, w, z, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wyxz: Vector4<T> get() = Vector4(w, y, x, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wxyz: Vector4<T> get() = Vector4(w, x, y, z)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wxzy: Vector4<T> get() = Vector4(w, x, z, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wzxy: Vector4<T> get() = Vector4(w, z, x, y)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wzyx: Vector4<T> get() = Vector4(w, z, y, x)

@Suppress("KDocMissingDocumentation")
public inline val <T> Vector4<T>.wyzx: Vector4<T> get() = Vector4(w, y, z, x)

// ZipOperatable math operations <Int> =========================================

/**
 * @return [Self] with all elements added to [other]'s elements in the
 * same position.
 */
public inline operator fun <Self : ZipOperatable<Int, Self>> Self.plus(other: Self): Self =
    zipOperate(other) { a, b -> a + b }

/**
 * @return [Self] with all elements subtracted from [other]'s elements in the
 * same position.
 */
public inline operator fun <Self : ZipOperatable<Int, Self>> Self.minus(other: Self): Self =
    zipOperate(other) { a, b -> a - b }

/**
 * @return [Self] with all elements multiplied by [other]'s elements in the same
 * position.
 */
public inline operator fun <Self : ZipOperatable<Int, Self>> Self.times(other: Self): Self =
    zipOperate(other) { a, b -> a * b }

/**
 * @return [Self] with all elements divided by [other]'s elements in the same
 * position.
 */
public inline operator fun <Self : ZipOperatable<Int, Self>> Self.div(other: Self): Self =
    zipOperate(other) { a, b -> a / b }

/**
 * @return [Self] with all elements multiplied by [other]
 */
public inline operator fun <Self : ZipOperatable<Int, Self>> Self.times(other: Int): Self =
    zipOperate { a -> a * other }

/**
 * @return The sum of all the elements in this [ZipOperatable].
 */
public inline fun <Self : ZipOperatable<Int, Self>> Self.sum(): Int =
    zipCombine { a, b -> a + b }

// ZipOperatable math operations <Float> =======================================

/**
 * @return [Self] with all elements added to [other]'s elements in the
 * same position.
 */
public inline operator fun <Self : ZipOperatable<Float, Self>> Self.plus(other: Self): Self =
    zipOperate(other) { a, b -> a + b }

/**
 * @return [Self] with all elements subtracted from [other]'s elements in the
 * same position.
 */
public inline operator fun <Self : ZipOperatable<Float, Self>> Self.minus(other: Self): Self =
    zipOperate(other) { a, b -> a - b }

/**
 * @return [Self] with all elements multiplied by [other]'s elements in the same
 * position.
 */
public inline operator fun <Self : ZipOperatable<Float, Self>> Self.times(other: Self): Self =
    zipOperate(other) { a, b -> a * b }

/**
 * @return [Self] with all elements divided by [other]'s elements in the same
 * position.
 */
public inline operator fun <Self : ZipOperatable<Float, Self>> Self.div(other: Self): Self =
    zipOperate(other) { a, b -> a / b }

/**
 * @return [Self] with all elements multiplied by [other]
 */
public inline operator fun <Self : ZipOperatable<Float, Self>> Self.times(other: Float): Self =
    zipOperate { a -> a * other }

/**
 * @return The sum of all the elements in this [ZipOperatable].
 */
public inline fun <Self : ZipOperatable<Float, Self>> Self.sum(): Float =
    zipCombine { a, b -> a + b }

/**
 * @return The magnitude of this [ZipOperatable] of [Float].
 */
public inline fun <Self: ZipOperatable<Float, Self>> Self.magnitude(): Float =
    sqrt(zipOperate { a -> a.pow(2) }.sum())

/**
 * @return A normalized vector in this [ZipOperatable].
 */
public inline fun <Self: ZipOperatable<Float, Self>> Self.normalized(): Self =
    magnitude().let { magnitude -> zipOperate { it / magnitude } }

/**
 * Gets the angle of a vector as if it's a point on a circle.
 *
 * The vector must be [normalized]. A non-normalized vector may produce
 * strange results.
 *
 * @return Radians.
 */
public val Vector2<Float>.asAngle: Angle
    get() = atan(y / x).let { v ->
        if(y < 0) v + PI.toFloat()
        else v
    }.radians


@Suppress("KDocMissingDocumentation")
public inline fun Vector2<Int>.toFloatVector(): Vector2<Float> =
    Vector2(x.toFloat(), y.toFloat())

@Suppress("KDocMissingDocumentation")
public inline fun Vector2<Float>.roundDownToIntVector(): Vector2<Int> =
    Vector2(floor(x).toInt(), floor(y).toInt())

@Suppress("KDocMissingDocumentation")
public inline fun Vector2<Float>.roundUpToIntVector(): Vector2<Int> =
    Vector2(ceil(x).toInt(), ceil(y).toInt())

/**
 * @return The [magnitude] of [other] as if it was relative to `this` instead
 * of the world origin.
 * In simpler terms, the distance from `this` to [other].
 */
public inline infix fun <Self : ZipOperatable<Float, Self>> Self.distanceTo(other: Self): Float =
    (other - this).magnitude()

/**
 * @return The [Angle] of [other] compared to `this`.
 * If `this` is considered to be north, this function returns the angle relative
 * to that north.
 */
public inline infix fun Vector2<Float>.angleOfLineTo(other: Vector2<Float>): Angle =
    (other - this).asAngle
