package net.derfruhling.kemono.physics

import net.derfruhling.kemono.map.MapChunk
import net.derfruhling.kemono.map.Tile
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.times
import net.derfruhling.kemono.math.toFloatVector
import me.derfruhling.quantum.core.log.Logger

private fun findBoxes(
    chunkPosition: Vector2<Int>,
    chunk: Iterable<Pair<Vector2<Int>, Tile>>
): List<BoundingBox> {
    val remaining = chunk.toMap().toMutableMap()
    
    fun nextPair() = remaining.keys.toList().minBy { (x, y) -> (y * MapChunk.SIZE) + x }
    
    val boxes = mutableListOf<BoundingBox>()
    val chunkPosF = chunkPosition.toFloatVector() * 16.0f
    
    while(remaining.isNotEmpty()) {
        // what this basically tries to do is:
        // - pick a position of a tile
        // - try to push as far into +X as possible
        // - upon hitting a roadblock or the edge, try to expand +Y
        // - upon hitting a roadblock or the edge, add the box
        val tilePos = nextPair()
        val tile = remaining.remove(tilePos) ?: continue
        if(!tile.canBeCollidedWith) continue
        
        val (x, y) = tilePos
        
        val usedX = mutableListOf(x)
        var startX = chunkPosF.x + x.toFloat()
        var startY = chunkPosF.y + y.toFloat()
        var endX = startX + 1.0f
        var endY = startY + 1.0f

        outer@ for(nx in (x+1)..<(MapChunk.SIZE - x)) {
            val newPos = Vector2(nx, y)
            if(remaining[newPos]?.canBeCollidedWith != true) break@outer
            
            endX += 1.0f
            usedX.add(nx)
            remaining.remove(newPos)
        }
        
        outer@ for(ny in (y+1)..<(MapChunk.SIZE - y)) {
            for (nx in usedX) {
                if(remaining[Vector2(nx, ny)]?.canBeCollidedWith != true) break@outer
            }

            endY += 1.0f
            for(nx in usedX) remaining.remove(Vector2(nx, ny))
        }
        
        boxes.add(BoundingBox(startX, endX, startY, endY).also {
            Logger.of("findBoxes").debug("Found box {}", it)
        })
    }
    
    return boxes
}

public data class PhysicsChunk(public val boxes: List<BoundingBox>) : Iterable<BoundingBox> {
    public constructor(mapChunk: MapChunk) : this(findBoxes(mapChunk.position, mapChunk))
    
    override fun iterator(): Iterator<BoundingBox> {
        return boxes.iterator()
    }
}

public infix fun BoundingBox.intersectsWith(chunk: PhysicsChunk): Boolean {
    return chunk.any { this intersectsWith it }
}

public fun BoundingBox.introspect(chunk: PhysicsChunk): IntersectionIntrospection? {
    return chunk.firstNotNullOfOrNull { introspect(it) }
}
