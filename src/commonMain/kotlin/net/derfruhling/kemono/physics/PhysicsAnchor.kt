package net.derfruhling.kemono.physics

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2

public class PhysicsAnchor(
    public val engine: PhysicsEngine,
    public val entity: Entity
) {
    public var boundingBox: BoundingBox = entity.createBoundingBox(entity.position)
        private set
    
    public fun updateBoundingBox() {
        boundingBox = entity.createBoundingBox(entity.position)
    }
    
    public suspend fun runPhysicsSimulation(
        entityBoundingBox: BoundingBox = boundingBox
    ): Vector2<Float>? = coroutineScope {
        /*
         * ignoring boundingBox here is intentional.
         * getBoxesAroundEntity will also retrieve all anchors of the engine.
         *
         * if entityBoundingBox is different from boundingBox, this entity
         * will attempt to test collision between the two, which is probably
         * not what you're intending.
         */
        val tests = engine.getBoxesAroundEntity(entity.position, boundingBox).map { staticBox ->
            async { entityBoundingBox.introspect(staticBox) }
        }
        
        tests.awaitAll().firstNotNullOfOrNull { it }?.correction
    }
}