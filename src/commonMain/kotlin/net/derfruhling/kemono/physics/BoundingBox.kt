package net.derfruhling.kemono.physics

import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.linearInterp
import me.derfruhling.quantum.core.dsl.PairOf
import me.derfruhling.quantum.core.log.logger

public data class BoundingBox(
    val startX: Float,
    val endX: Float,
    val startY: Float,
    val endY: Float
) {
    public constructor(
        x: PairOf<Float>,
        y: PairOf<Float>
    ) : this(x.first, x.second, y.first, y.second)

    private val log by logger

    public val center: Vector2<Float> = Vector2(
        linearInterp(startX, endX, 0.5f),
        linearInterp(startY, endY, 0.5f)
    )

    /**
     * Normalizes this bounding box. The actual physics calculations require
     * that [startX] is less than [endX] and [startY] is less than [endY],
     * so this method will ensure that they are in the right order.
     */
    public inline fun normalize(): BoundingBox {
        var box = this
        if(startX > endX) box = box.copy(startX = endX, endX = startX)
        if(startY > endY) box = box.copy(startY = endY, endY = startY)
        return box
    }
    
    /**
     * Checks if `this` intersects with [other] on the X-axis.
     * This does not check the Y-axis.
     * 
     * @see intersectsWith
     */
    public infix fun xIntersectsWith(other: BoundingBox): Boolean {
        return (((startX > other.startX && startX < other.endX) ||
                 (endX > other.startX && endX < other.endX)) ||
                ((other.startX > startX && other.startX < endX) ||
                 (other.endX > startX && other.endX < endX)))
    }

    /**
     * Checks if `this` intersects with [other] on the Y-axis.
     * This does not check the X-axis.
     *
     * @see intersectsWith
     */
    public infix fun yIntersectsWith(other: BoundingBox): Boolean {
        return (((startY > other.startY && startY < other.endY) ||
                 (endY > other.startY && endY < other.endY)) ||
                ((other.startY > startY && other.startY < endY) ||
                 (other.endY > startY &&  other.endY < endY)))
    }
    
    /**
     * Determines if `this` intersects with `other`.
     */
    public infix fun intersectsWith(other: BoundingBox): Boolean {
        return this xIntersectsWith other && this yIntersectsWith other
    }

    public operator fun contains(other: Vector2<Float>): Boolean {
        return other.x > startX && other.x < endX &&
               other.y > startX && other.y < endY
    }
    
    /**
     * Allows comparing `this` with [other] in a way that can automatically
     * produce a correction value for a current intersection.
     *
     * @return The intersection introspection, or `null` if there is not
     * an intersection.
     */
    public fun introspect(other: BoundingBox): IntersectionIntrospection? {
        if(!(this intersectsWith other)) return null
        return IntersectionIntrospection(this, other)
    }
    
    /**
     * Adds [other] to the values of `this` and returns the new box.
     */
    public operator fun plus(other: Vector2<Float>): BoundingBox {
        return BoundingBox(
            startX = startX + other.x,
            startY = startY + other.y,
            endX = endX + other.x,
            endY = endY + other.y
        )
    }
    
    /**
     * Subtracts [other] from the values of `this` and returns the new box. 
     */
    public operator fun minus(other: Vector2<Float>): BoundingBox {
        return BoundingBox(
            startX = startX - other.x,
            startY = startY - other.y,
            endX = endX - other.x,
            endY = endY - other.y
        )
    }
}
