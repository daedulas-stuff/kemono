package net.derfruhling.kemono.physics

import kotlinx.coroutines.coroutineScope
import net.derfruhling.kemono.map.Map
import net.derfruhling.kemono.map.MapChunk
import net.derfruhling.kemono.map.entities.Entity
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.div
import net.derfruhling.kemono.math.roundDownToIntVector

public class PhysicsEngine {
    private val chunks = mutableMapOf<Vector2<Int>, PhysicsChunk>()
    private val entities = mutableListOf<PhysicsAnchor>()

    public fun createChunk(chunk: MapChunk): PhysicsChunk {
        require(chunk.position !in chunks) { "Physics chunk already created" }
        return PhysicsChunk(chunk).also { chunks[chunk.position] = it }
    }

    public fun removeChunk(chunk: MapChunk) {
        if(chunk.position !in chunks) return
        chunks.remove(chunk.position)
    }

    public suspend fun refresh(map: Map): Unit = coroutineScope {
        chunks.keys.toList().forEach { pos ->
            val chunk = map[pos] ?: run {
                chunks.remove(pos)
                return@forEach
            }

            chunks[pos] = PhysicsChunk(chunk)
        }
    }

    public fun anchor(entity: Entity): PhysicsAnchor =
        PhysicsAnchor(this, entity).also { entities.add(it) }
    
    /**
     * Gets the boxes in the 9 chunks that surround and include [position].
     *
     * @return All bounding boxes in those chunks.
     */
    public fun getBoxesAroundEntity(position: Vector2<Float>, ignoring: BoundingBox? = null): Iterable<BoundingBox> {
        val chunk = (position / Vector2(16.0f, 16.0f)).roundDownToIntVector()
        
        return Iterable {
            iterator {
                for(i in (chunk.x-1)..(chunk.x+1)) {
                    for(j in (chunk.y-1)..(chunk.y+1)) {
                        chunks[Vector2(i, j)]?.boxes?.let { yieldAll(it) }
                    }
                }

                for(entity in entities) {
                    if(entity.boundingBox == ignoring) continue
                    yield(entity.boundingBox)
                }
            }
        }
    }

    public fun getBoxesAroundBox(boundingBox: BoundingBox, ignoring: BoundingBox? = null): Iterable<BoundingBox> {
        return getBoxesAroundEntity(boundingBox.center, ignoring)
    }

    public fun testIntersection(boundingBox: BoundingBox, ignoring: BoundingBox? = null): IntersectionIntrospection? {
        return getBoxesAroundBox(boundingBox, ignoring).firstNotNullOfOrNull {
            boundingBox.introspect(it)
        }
    }
}