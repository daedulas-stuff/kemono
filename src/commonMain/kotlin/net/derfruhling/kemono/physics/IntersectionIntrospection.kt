package net.derfruhling.kemono.physics

import net.derfruhling.kemono.map.MapService
import net.derfruhling.kemono.math.Vector2
import net.derfruhling.kemono.math.magnitude
import net.derfruhling.kemono.services.Component
import net.derfruhling.kemono.services.service
import me.derfruhling.quantum.core.log.logger

public class IntersectionIntrospection(
    public val entity: BoundingBox,
    public val static: BoundingBox
) : Component {
    private val log by logger
    private val mapService: MapService by service()

    /**
     * Tries to calculate an acceptible correction for the intersection.
     * This will return `[0, 0]` if there was never an intersection in
     * the first place.
     */
    public val correction: Vector2<Float>
        get() {
            var xCorr = 0.0f
            var yCorr = 0.0f
            
            for(i in 1..16) {
                val entity = entity + Vector2(xCorr, yCorr)

                entity.introspect(static)
                    ?: mapService.currentMap?.physicsEngine?.testIntersection(entity, this.entity)
                    ?: break

                val corrections = mutableListOf<Vector2<Float>>()

                if (entity.endX > static.startX && entity.endX < static.endX) {
                    corrections.add(Vector2(xCorr - (entity.endX - static.startX), yCorr))
                }

                if (entity.startX > static.startX && entity.startX < static.endX) {
                    corrections.add(Vector2(xCorr + (static.endX - entity.startX), yCorr))
                }

                if (entity.endY > static.startY && entity.endY < static.endY) {
                    corrections.add(Vector2(xCorr, yCorr - (entity.endY - static.startY)))
                }

                if (entity.startY > static.startY && entity.startY < static.endY) {
                    corrections.add(Vector2(xCorr, yCorr + (static.endY - entity.startY)))
                }

                // select the least significant correction
                val applied = corrections.minByOrNull { it.magnitude() } ?: break

                // apply and loop again
                xCorr = applied.x
                yCorr = applied.y
            }

            return Vector2(xCorr, yCorr)
        }
}