Kemono shaders are organized in this order:

- `shaders` directory
- What is its main purpose? `color`? `textured`?
- What are its quirks? For `ui`? All `jitter`y? Or just `basic`?
- Choose one of the following targets as your target name:
    - `2d` for shaders that take `vec2 vPosition` and do not fall into any other category.
    - `3d` for shaders that take `vec3 vPosition` and do not fall into any other category.
    - `ui` for shaders that take `ivec2 vPositionUI`, `uniform ivec2 vBoundsUI`, and `uniform int uSideUI`. Most UI
      stuff just takes integers like this.
  <!-- Add targets as necessary here. `oth` must be last -->
    - `oth` for anything that does not fall into a target category. _Seriously discouraged! Try to create a new target
      instead!_
- Your shader name here. The above target name can also just be the shader name if you don't need any more
  categorization under it. (Just remember not to break backwards compatibility when updating the paths.)
    - For `color` shaders this is usually the components the shader takes, as in `rgb` or `monochr` (which just uses R,
      please do not name things `r`).
    - For `ui` shaders this is usually the name of a specialized UI element, or just `generic` for a generic rectangle
      element.
    - For others, anything is fair game. Your names must be descriptive and accurate, however.

Ultimately, your path should look something like `shaders/color/basic/2d/rgb.shdr.xml`. You would reference this in code
like:

<!-- 
IJ checks below code block for errors.
Because the project uses explicit-api mode, it thinks it's an error for it to not be `public`.
Send help.
-->

```kotlin
public val rgbColorBasicShader: ShaderResoruce by createShaderResource("kemono:shaders/color/basic/2d/rgb.shdr").autoDisposing()
```