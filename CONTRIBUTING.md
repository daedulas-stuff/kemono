# Contributing

Hey! If you're here because you wish to contribute to this little project, that's great! Make sure you've read this full document before doing so.

We would _really_ like you to follow the rules laid out in this document in your contribution. It lays out our requirements for what we will accept in our project, and additionally what we will not accept or request revisions for.

This document is intended to be specific about certain things. If you feel that something is ambiguous, you can open an issue about it to try and get it resolved. Issues exclusively involving this file may be exempt from many of it's restrictions.

Key words are **emboldened**. _Emphasis_ may be put on certain words for stylistic purposes.

## Issues

If you find a bug or potential new feature in the project, you can inform the project about it through issues.

1. Issues **must** be descriptive and on-topic.
2. Issues **must** address only one topic at a time.
   - Broad feature requests may be exempt from this.
3. **If** you intend to fix the issue or implement the feature yourself, you **must not** open an issue alongside your merge request.
   - Instead, consider opening a _draft_ merge request, described in a section below.
4. Issues **must** follow the provided template, if any is provided.
5. Ensure you have searched for your issue before you report it.
   - Duplicate issues will be closed and marks as a duplicate.

## Contributing Code

Ensure you have reviewed the [LICENSE](LICENSE) and agree that your code will be under it's terms before you decide to contribute.

6. Code contributions **must** be substantial.
   - No MRs just for fixing typos.
   - Small, but _significant_ problems may be considered substantial.
7. Merge Requests **must** be reviewed by a maintainer before the MR is merged, **except** for MRs that are created by a maintainer.
8. Merge Request changes **must** follow the general code styling of the existing code.
   - There are no exceptions. Format your code!
9. Changes **must** be purposeful and **must** show a clear effort to find a good solution to the issue at hand.
10. Because this project involves native code, consider testing on multiple operating systems, or asking others to help with testing. Windows and Linux should suffice.

## Drafting Changes

11. Draft Merge Requests may be considered the same as feature requests until they are completed, with the additional stipulation that there is existing work on the issue.
12. In the event where there are multiple MRs implementing the same feature, the following rules will be applied to resolve the conflict:
    1. A comparison of features and methods. The more "substantial" branch as determined by an assigned maintainer of either MR determines what is considered more "substantial" out of the two conflicting changes.
    2. A comparison of "documentation". The more "documented" branch wins. "Documented" is defined by more meaningful KDoc comments and using the provided annotations to mark function behaviors in a more meaningful way.
    3. A comparison of MR age. If one MR is older by more than a few days and shows that there has been more work done on it, it wins the conflict.
    4. A good ol' coin flip. If all else fails, someone will flip a coin and just merge something. There needs to be a resolution at some point. Sorry!
13. If there are already changes that you wish to contribute existing as a MR, you have several options in no particular order:
    - Help contribute to the existing MR.
    - Review the existing MR and decide if you would do it in a way that is substantially different. If so, open a new draft MR to show that you are working on the changes as well.
        - Ensure you know that you are creating a conflict here, which will be resolved as described in [12]!
    - Do nothing.
    - Fuck it! Submit a new MR anyways! Chaos reigns!
        - Same as above notice about conflicts and stuff.

---

Thank you for your interest in contributing to this project. We look forward to seeing what you build!
