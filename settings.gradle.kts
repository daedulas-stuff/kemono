pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        mavenLocal()
    }

    includeBuild("kebuild")
}

rootProject.name = "kemono"

//include("kebuild")
