import net.derfruhling.kemono.gradle.*
import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode
import java.nio.file.Files
import kotlin.io.path.name

plugins {
    kotlin("multiplatform") version "2.0.0"
    kotlin("plugin.serialization") version "2.0.0"
    id("net.derfruhling.kemono.kebuild") version "2023.1a3-SNAPSHOT"
    `maven-publish`
}

apply<KebuildPlugin>()

group = "net.derfruhling.kemono"
version = "2023.1a3-SNAPSHOT"

/** documentation */
val os = System.getProperty("os.name").lowercase()

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/47184198/packages/maven")
}

kotlin {
    mingwX64 {
        binaries {
            executable {
                entryPoint("net.derfruhling.kemono.main")
            }
        }

        compilations.all {
            val platform by cinterops.creating {
                defFile("src/nativeMain/platform-graphics.def")
            }
        }
    }

    linuxX64 {
        binaries {
            executable {
                entryPoint("net.derfruhling.kemono.main")
            }
        }

        compilations.all {
            val platform by cinterops.creating {
                defFile("src/nativeMain/platform-graphics.def")
            }
        }

        withLinuxX64Libraries()
    }

    macosX64 {
        binaries {
            executable {
                entryPoint("net.derfruhling.kemono.main")
            }
        }

        compilations.all {
            val platform by cinterops.creating {
                defFile("src/nativeMain/platform-graphics.def")
            }
        }

        withMacosLibraries()
    }

    macosArm64 {
        binaries {
            executable {
                entryPoint("net.derfruhling.kemono.main")
            }
        }

        compilations.all {
            val platform by cinterops.creating {
                defFile("src/nativeMain/platform-graphics.def")
            }
        }

        withMacosLibraries()
    }

    targets.all {
        compilations.all {
            compileTaskProvider.configure {
                compilerOptions {
                    progressiveMode = true
                    allWarningsAsErrors = true
                    freeCompilerArgs.add("-Xexpect-actual-classes")
                    freeCompilerArgs.add("-Xexplicit-api=strict")
                }
            }
        }
    }

    /*
     * Disable explicit-API mode for tests.
     */
    testableTargets.all {
        compilations.all compilations@{
            compileTaskProvider.configure {
                compilerOptions {
                    progressiveMode = true
                    freeCompilerArgs.add("-Xexplicit-api=none")
                }
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("me.derfruhling.quantum:quantum-core:1.0.0-SNAPSHOT")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.6.0")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-cbor:1.6.0")
                implementation("com.squareup.okio:okio:3.6.0")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        val nativeMain by creating { dependsOn(commonMain) }
        val nativeTest by creating { dependsOn(commonTest) }
        val mingwX64Main by getting { dependsOn(nativeMain) }
        val mingwX64Test by getting { dependsOn(nativeTest) }
        val macosMain by creating { dependsOn(nativeMain) }
        val macosTest by creating { dependsOn(nativeTest) }
        val macosX64Main by getting { dependsOn(macosMain) }
        val macosX64Test by getting { dependsOn(macosTest) }
        val macosArm64Main by getting { dependsOn(macosMain) }
        val macosArm64Test by getting { dependsOn(macosTest) }
        val linuxX64Main by getting { dependsOn(nativeMain) }
        val linuxX64Test by getting { dependsOn(nativeTest) }
    }
}

/** documentation */
val compileKmResources by tasks.creating(CompileResources::class) {
    dependsOn("metadataCommonMainProcessResources")
    sourceDir.set(
        buildDir
            .resolve("processedResources")
            .resolve("metadata")
            .resolve("commonMain")
    )
    outputDir.set(
        buildDir
            .resolve("compiledResources")
            .resolve("common")
    )
    moduleName.set("kemono")
}

/** documentation */
val packageKmResources by tasks.creating(PackageResources::class) {
    from(compileKmResources)
}

tasks {
    val bootstrapVcpkg by creating(Exec::class) {
        if (System.getProperty("os.name").contains("Windows")) {
            outputs.file(rootDir.resolve("vcpkg/vcpkg.exe"))
            workingDir(rootDir.resolve("vcpkg"))
            commandLine(rootDir.resolve("vcpkg/bootstrap-vcpkg.bat"))
        } else {
            outputs.file(rootDir.resolve("vcpkg/vcpkg"))
            workingDir(rootDir.resolve("vcpkg"))
            commandLine("/bin/sh", "-e", rootDir.resolve("vcpkg/bootstrap-vcpkg.sh"))
        }
    }

    val vcpkgMingwX64 by creating(Exec::class) {
        dependsOn(bootstrapVcpkg)
        workingDir(rootDir)
        commandLine(
            rootDir.resolve(
                when {
                    os.contains("windows") -> "vcpkg/vcpkg.exe"
                    else -> "vcpkg/vcpkg"
                }
            ), "install", "--triplet", "x64-mingw-static", "--x-install-root=vcpkg_installed/mingwX64"
        )
    }

    val vcpkgLinuxX64 by creating(Exec::class) {
        dependsOn(bootstrapVcpkg)
        workingDir(rootDir)
        commandLine(
            rootDir.resolve(
                when {
                    os.contains("windows") -> "vcpkg/vcpkg.exe"
                    else -> "vcpkg/vcpkg"
                }
            ), "install", "--triplet", "x64-linux", "--x-install-root=vcpkg_installed/linuxX64"
        )
    }

    val vcpkgMacosX64 by creating(Exec::class) {
        dependsOn(bootstrapVcpkg)
        workingDir(rootDir)
        commandLine(
            rootDir.resolve(
                when {
                    os.contains("windows") -> "vcpkg/vcpkg.exe"
                    else -> "vcpkg/vcpkg"
                }
            ), "install", "--triplet", "x64-osx", "--x-install-root=vcpkg_installed/macosX64"
        )
    }

    val vcpkgMacosArm64 by creating(Exec::class) {
        dependsOn(bootstrapVcpkg)
        workingDir(rootDir)
        commandLine(
            rootDir.resolve(
                when {
                    os.contains("windows") -> "vcpkg/vcpkg.exe"
                    else -> "vcpkg/vcpkg"
                }
            ), "install", "--triplet", "arm64-osx", "--x-install-root=vcpkg_installed/macosArm64"
        )
    }

    val resolveResourceDependencies by creating {
        doFirst {
            val deps = configurations.resources.get().dependencies
            deps.forEach {
                println(it)
            }

            println("Existing files: " + configurations.resources.get().files.joinToString())
            println("Resolved files: " + configurations.resources.get().resolve().joinToString())
            println("Artifacts: " + configurations.resources.get().allArtifacts.joinToString())
        }
    }

    when {
        os.contains("nix") || os.contains("nux") -> {
            val linkDebugExecutableLinuxX64 by getting
            val linkReleaseExecutableLinuxX64 by getting
            val linuxX64ProcessResources by getting

            val stageDebug by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/linuxX64/debug")) }

                dependsOn(
                    linkDebugExecutableLinuxX64,
                    compileKmResources
                )

                from(
                    linkDebugExecutableLinuxX64,
                    rootDir.resolve("vcpkg_installed/linuxX64/x64-linux/bin")
                        .listFiles { _, name -> name.endsWith(".so") }
                )

                into(buildDir.resolve("staged/linuxX64/debug"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedDebug by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/linuxX64/debug"))
                executable(buildDir.resolve("staged/linuxX64/debug/kemono.kexe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }

            val stageRelease by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/linuxX64/release")) }

                dependsOn(
                    linkReleaseExecutableLinuxX64,
                    compileKmResources
                )

                from(
                    linkReleaseExecutableLinuxX64,
                    rootDir.resolve("vcpkg_installed/linuxX64/x64-linux/bin")
                        .listFiles { _, name -> name.endsWith(".so") }
                )

                into(buildDir.resolve("staged/linuxX64/release"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedRelease by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/linuxX64/release"))
                executable(buildDir.resolve("staged/linuxX64/release/kemono.kexe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }
        }

        os.contains("windows") -> {
            val linkDebugExecutableMingwX64 by getting
            val linkReleaseExecutableMingwX64 by getting
            val mingwX64ProcessResources by getting

            val stageDebug by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/mingwX64/debug")) }

                dependsOn(
                    linkDebugExecutableMingwX64,
                    compileKmResources
                )

                from(
                    linkDebugExecutableMingwX64,
                    rootDir.resolve("vcpkg_installed/mingwX64/x64-mingw-static/bin")
                        .listFiles { _, name -> name.endsWith(".dll") || name.endsWith(".pdb") }
                )

                into(buildDir.resolve("staged/mingwX64/debug"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedDebug by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/mingwX64/debug"))
                executable(buildDir.resolve("staged/mingwX64/debug/kemono.exe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }

            val stageRelease by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/mingwX64/release")) }

                dependsOn(
                    linkReleaseExecutableMingwX64,
                    compileKmResources
                )

                from(
                    linkReleaseExecutableMingwX64,
                    rootDir.resolve("vcpkg_installed/mingwX64/x64-mingw-static/bin")
                        .listFiles { _, name -> name.endsWith(".dll") || name.endsWith(".pdb") }
                )

                into(buildDir.resolve("staged/mingwX64/release"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedRelease by creating(Exec::class) {
                dependsOn(stageRelease)
                workingDir(buildDir.resolve("staged/mingwX64/release"))
                executable(buildDir.resolve("staged/mingwX64/release/kemono.exe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }
        }

        os.contains("mac os") -> {
            val linkDebugExecutableMacosArm64 by getting
            val linkReleaseExecutableMacosArm64 by getting
            val macosArm64ProcessResources by getting
            val linkDebugExecutableMacosX64 by getting
            val linkReleaseExecutableMacosX64 by getting
            val macosX64ProcessResources by getting

            when {
                "aarch64" in System.getProperty("os.arch") -> {
                    val stageDebug by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosArm64/debug")) }

                        dependsOn(
                            linkDebugExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkDebugExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosArm64/arm64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosArm64/debug"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val stageDebugX64 by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosX64/debug")) }

                        dependsOn(
                            linkDebugExecutableMacosX64,
                            compileKmResources
                        )

                        from(
                            linkDebugExecutableMacosX64,
                            rootDir.resolve("vcpkg_installed/macosX64/x64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosX64/debug"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val runStagedDebug by creating(Exec::class) {
                        dependsOn(stageDebug)
                        workingDir(buildDir.resolve("staged/macosArm64/debug"))
                        executable(buildDir.resolve("staged/macosArm64/debug/kemono.kexe"))
                        args(
                            (findProperty("kemonoArgs") as String?)
                                ?.split(',')
                                ?.toList() ?: emptyList<String>()
                        )
                    }

                    val stageRelease by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosArm64/release")) }

                        dependsOn(
                            linkReleaseExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkReleaseExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosArm64/arm64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosArm64/release"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val stageReleaseX64 by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosX64/release")) }

                        dependsOn(
                            linkReleaseExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkReleaseExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosX64/x64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosX64/release"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val runStagedRelease by creating(Exec::class) {
                        dependsOn(stageDebug)
                        workingDir(buildDir.resolve("staged/macosArm64/release"))
                        executable(buildDir.resolve("staged/macosArm64/release/kemono.kexe"))
                        args(
                            (findProperty("kemonoArgs") as String?)
                                ?.split(',')
                                ?.toList() ?: emptyList<String>()
                        )
                    }
                }
                "amd64" in System.getProperty("os.arch") -> {
                    val stageDebug by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosX64/debug")) }

                        dependsOn(
                            linkDebugExecutableMacosX64,
                            compileKmResources
                        )

                        from(
                            linkDebugExecutableMacosX64,
                            rootDir.resolve("vcpkg_installed/macosX64/x64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosX64/debug"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val stageDebugArm64 by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosArm64/debug")) }

                        dependsOn(
                            linkDebugExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkDebugExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosArm64/arm64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosArm64/debug"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val runStagedDebug by creating(Exec::class) {
                        dependsOn(stageDebug)
                        workingDir(buildDir.resolve("staged/macosX64/debug"))
                        executable(buildDir.resolve("staged/macosX64/debug/kemono.kexe"))
                        args(
                            (findProperty("kemonoArgs") as String?)
                                ?.split(',')
                                ?.toList() ?: emptyList<String>()
                        )
                    }

                    val stageRelease by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosX64/release")) }

                        dependsOn(
                            linkReleaseExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkReleaseExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosX64/x64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosX64/release"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val stageReleaseArm64 by creating(Sync::class) {
                        doFirst { mkdir(buildDir.resolve("staged/macosArm64/release")) }

                        dependsOn(
                            linkReleaseExecutableMacosArm64,
                            compileKmResources
                        )

                        from(
                            linkReleaseExecutableMacosArm64,
                            rootDir.resolve("vcpkg_installed/macosArm64/arm64-osx/lib")
                                .listFiles { _, name -> name.endsWith(".dylib") }
                        )

                        into(buildDir.resolve("staged/macosArm64/release"))

                        from(compileKmResources) { into("resources") }
                        from(provider {
                            configurations.resources.get().resolvedConfiguration.files.map {
                                zipTree(it)
                            }
                        }) { into("resources") }
                    }

                    val runStagedRelease by creating(Exec::class) {
                        dependsOn(stageDebug)
                        workingDir(buildDir.resolve("staged/macosX64/release"))
                        executable(buildDir.resolve("staged/macosX64/release/kemono.kexe"))
                        args(
                            (findProperty("kemonoArgs") as String?)
                                ?.split(',')
                                ?.toList() ?: emptyList<String>()
                        )
                    }
                }
                else -> throw IllegalStateException("Unsupported macOS os.arch ${System.getProperty("os.arch")}")
            }
        }

        else -> {
            throw IllegalStateException("Unsupported OS $os")
        }
    }
}

publishing {
    repositories {
        maven("https://gitlab.com/api/v4/projects/50850114/packages/maven") {
            credentials(HttpHeaderCredentials::class) {
                when {
                    findProperty("gitLabPrivateToken") != null -> {
                        name = "Private-Token"
                        value = findProperty("gitLabPrivateToken") as String?
                    }
                    System.getenv("CI_JOB_TOKEN") != null -> {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")!!
                    }
                    else -> println(
                        """
                            Warning: No authentication credentials specified. Cannot publish!
                                     If that's OK, you can safely ignore this warning.
                        """.trimIndent()
                    )
                }
            }

            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }

    publications {
        val resources by creating(MavenPublication::class) {
            artifactId = project.name + "-resources"
            groupId = project.group.toString()
            version = project.version.toString()

            pom.withXml {
                val dependencies = asNode().appendNode("dependencies")
                configurations.resources.get().resolvedConfiguration.firstLevelModuleDependencies.forEach {
                    val dependency = dependencies.appendNode("dependency")
                    dependency.appendNode("groupId", it.moduleGroup)
                    dependency.appendNode("artifactId", it.moduleName)
                    dependency.appendNode("version", it.moduleVersion)
                    dependency.appendNode("classifier", "resources")
                    dependency.appendNode("extension", "kmrs")
                }
            }

            artifact(packageKmResources) {
                extension = "kmrs"
            }
        }
    }
}
